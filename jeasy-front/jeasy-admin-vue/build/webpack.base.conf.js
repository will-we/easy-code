// nodejs 中的path模块
var path = require('path')
var utils = require('./utils')
var config = require('../config')
var vueLoaderConfig = require('./vue-loader.conf')
// 在最上方的地方載入 webpack
var webpack = require('webpack')

function resolve (dir) {
  // __dirname是node里面的一个变量，指向的是当前文件夹目录
  return path.join(__dirname, '..', dir)
}

module.exports = {
  entry: {
    // 入口文件，path.resolve()方法，可以结合我们给定的两个参数最后生成绝对路径，最终指向的就是我们的index.js文件
    app: './src/main.js'
  },
  // 输出配置
  output: {
    // 输出路径
    path: config.build.assetsRoot,
    // 打包的js命名
    filename: '[name].js',
    // 指向异步加载的路径
    publicPath: process.env.NODE_ENV === 'production'
      ? config.build.assetsPublicPath
      : config.dev.assetsPublicPath
  },
  // 当webpack试图去加载模块的时候，它默认是查找以 .js 结尾的文件的，它并不知道 .vue 结尾的文件是什么鬼玩意儿，
  // 所以我们要在配置文件中告诉webpack，遇到 .vue 结尾的也要去加载，添加 resolve 配置项
  resolve: {
    // 当我们去加载 ‘./components/Favlist’ 这样的模块时，webpack首先会查找 ./components/Favlist.js
    // 如果没有发现Favlist.js文件就会继续查找 Favlist.vue 文件
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': resolve('src')
    }
  },
  plugins: [new webpack.ProvidePlugin({
    jQuery: 'jquery',
    $: 'jquery',
    jquery: 'jquery'
  })],
  module: {
    rules: [
      {
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        include: [resolve('src'), resolve('test')],
        options: {
          formatter: require('eslint-friendly-formatter')
        }
      },
      {
        // 使用vue-loader 加载 .vue 结尾的文件
        test: /\.vue$/,
        loader: 'vue-loader',
        options: vueLoaderConfig
      },
      {
        // 使用了ES6的语法 import 语句，所以我们要使用 babel-loader 去加载我们的js文件
        test: /\.js$/,
        loader: 'babel-loader',
        include: [resolve('src'), resolve('test')]
      },
      {
        // 加载图片
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          // 当我们的图片大小小于10000字节的时候，webpack会把图片转换成base64格式插入到代码中，从而减少http请求
          limit: 10000,
          name: utils.assetsPath('img/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsPath('media/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
        }
      }
    ]
  }
}
