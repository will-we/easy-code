import Vue from 'vue'
import Router from 'vue-router'
import UserList from '@/components/user/list'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/user/list',
      name: '/user/list',
      component: UserList
    }
  ],
  // 当hashbang的值为true时，所有的路径都会被格式化已#!开头
  hashbang: true,
  history: true,
  saveScrollPosition: true,
  suppressTransitionError: true
})
