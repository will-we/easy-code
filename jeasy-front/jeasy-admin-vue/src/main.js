// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// vue的应用当然要引，等下要用它来注册
import Vue from 'vue'
import VueResource from 'vue-resource'
// noinspection JSUnresolvedVariable
import App from './App'
import iView from 'iview'
// 这个是路由，spa应用必要哦
import router from './router'
import 'iview/dist/styles/iview.css'

Vue.use(VueResource)
Vue.http.options.emulateJSON = true
Vue.http.options.emulateHTTP = true

Vue.use(iView)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
