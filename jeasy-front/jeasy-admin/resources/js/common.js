$(function() {
	// Waves初始化
	Waves.displayEffect();
	// 数据表格动态高度
	$(window).resize(function () {
		$('#table').bootstrapTable('resetView', {
			height: getHeight()
		});
	});
	// 设置input特效
	$(document).on('focus', 'input[type="text"]', function() {
		$(this).parent().find('label').addClass('active');
	}).on('blur', 'input[type="text"]', function() {
		if ($(this).val() == '') {
			$(this).parent().find('label').removeClass('active');
		}
	});
	// select2初始化
	$('select').select2();
});
// 动态高度
function getHeight() {
	return $(window).height() - 20;
}
// 数据表格展开内容
function detailFormatter(index, row) {
	var html = [];
	$.each(row, function (key, value) {
		html.push('<p><b>' + key + ':</b> ' + value + '</p>');
	});
	return html.join('');
}
// 初始化input特效
function initMaterialInput() {
	$('form input[type="text"]').each(function () {
		if ($(this).val() != '') {
			$(this).parent().find('label').addClass('active');
		}
	});
}

$.ajaxSetup({
	timeout: 3000,
	crossDomain: true,
	// 解决Ajax访问,默认不带Cookie问题
	xhrFields: {withCredentials: true},
	dataType: 'html',
	//请求成功后触发
	success: function (data) { show.append('success invoke!' + data + '<br/>'); },
	//请求失败遇到异常触发
	error: function (xhr, status, e) { show.append('error invoke! status:' + status+'<br/>'); },
	//完成请求后触发。即在success或error触发后触发
	complete: function (xhr, status) { show.append('complete invoke! status:' + status+'<br/>'); },
	//发送请求前触发
	beforeSend: function (xhr) {
		//可以设置自定义标头
		//xhr.setRequestHeader('Content-Type', 'application/xml;charset=utf-8');
		//show.append('beforeSend invoke!' +'<br/>');
	}
});

$(document).ajaxSend(function(event, xhr, settings) {
});

$(document).ajaxError(function (event, xhr, settings) {
	try {
		parent.$.messager.progress('close');
		parent.$.messager.alert('错误', xhr.responseText);
	} catch (e) {
		alert(xhr.responseText);
	}
});

$(document).ajaxComplete(function (event, xhr, settings) {
	//ajax session超时处理:通过XMLHttpRequest取得响应头,oauthstatus
	var oauthstatus = xhr.getResponseHeader("oauthstatus");
	if (oauthstatus == '401') {
		window.parent.location.reload(true);
	}
});
