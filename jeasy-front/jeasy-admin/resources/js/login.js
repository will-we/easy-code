$(function () {
    // Waves初始化
    Waves.displayEffect();
    // 输入框获取焦点后出现下划线
    $('.form-control').focus(function () {
        $(this).parent().addClass('fg-toggled');
    }).blur(function () {
        $(this).parent().removeClass('fg-toggled');
    });
});
Checkbix.init();
$(function () {
    // 点击登录按钮
    $('#login-bt').click(function () {
        login();
    });
    // 回车事件
    $('#username, #password').keypress(function (event) {
        if (13 == event.keyCode) {
            login();
        }
    });
    // 点击刷新验证码
    $("#captchaImg").click(function () {
        var $this = $(this);
        var url = $this.data("src") + new Date().getTime();
        $this.attr("src", url);
    });
});
// 登录
function login() {
    $.ajax({
        url: 'http://localhost:8080/login',
        type: 'POST',
        data: {
            username: $('#username').val(),
            password: $('#password').val(),
            captcha: $('#captcha').val(),
            rememberMe: $('#rememberMe').is(':checked') ? '1' : '0'
        },
        success: function (json) {
            if (json.code == 200) {
                location.href = json.data;
            } else {
                $.alert({
                    title: 'Oh no',
                    type: 'red',
                    icon: 'zmdi zmdi-alert-triangle',
                    autoClose: 'cancel|3000',
                    content: json.data.message,
                    buttons: {
                        cancel: {
                            text: '取消'
                        }
                    },
                    onClose: function () {
                        // 刷新验证码
                        $("#captchaImg").trigger("click");
                    }
                });
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
};

$.ajaxSetup({
    timeout: 3000,
    crossDomain: true,
    // 解决Ajax访问,默认不带Cookie问题
    xhrFields: {withCredentials: true},
    //请求成功后触发
    success: function (data) {
        //show.append('success invoke!' + data + '<br/>');
    },
    //请求失败遇到异常触发
    error: function (xhr, status, e) {
        //show.append('error invoke! status:' + status + '<br/>');
    },
    //完成请求后触发。即在success或error触发后触发
    complete: function (xhr, status) {
        //show.append('complete invoke! status:' + status + '<br/>');
    },
    //发送请求前触发
    beforeSend: function (xhr) {
        //可以设置自定义标头
        //xhr.setRequestHeader('Content-Type', 'application/xml;charset=utf-8');
        //show.append('beforeSend invoke!' +'<br/>');
    }
});

$(document).ajaxSend(function (event, xhr, settings) {
});

$(document).ajaxError(function (event, xhr, settings) {
    try {
        parent.$.messager.progress('close');
        parent.$.messager.alert('错误', xhr.responseText);
    } catch (e) {
        alert(xhr.responseText);
    }
});

$(document).ajaxComplete(function (event, xhr, settings) {
    //ajax session超时处理:通过XMLHttpRequest取得响应头,oauthstatus
    var oauthstatus = xhr.getResponseHeader("oauthstatus");
    if (oauthstatus == '401') {
        window.parent.location.reload(true);
    }
});
