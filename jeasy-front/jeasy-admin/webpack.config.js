var path = require('path')

module.exports = {
    entry: [
    ],
    output: {
    },
    module: {
        loaders: [
            {
                test: /\.(es6|jsx)?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel', // 'babel-loader' is also a legal name to reference
                query: {
                    presets: ['es2015', 'react'],
                    plugins: [
                        "transform-es2015-block-scoping",
                        "transform-class-properties",
                        "transform-es2015-computed-properties"
                    ]
                }
            }
        ]
    }
};
