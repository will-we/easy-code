package com.jeasy.roleresource.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.jeasy.base.web.controller.BaseController;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.base.web.resolver.FromJson;
import com.jeasy.common.Func;
import com.jeasy.doc.annotation.InitField;
import com.jeasy.doc.annotation.MethodDoc;
import com.jeasy.doc.annotation.StatusEnum;
import com.jeasy.roleresource.dto.*;
import com.jeasy.roleresource.service.RoleResourceService;
import com.jeasy.validate.handler.ValidateNotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 角色资源 Controller
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Controller
public class RoleResourceController extends BaseController<RoleResourceService> {

    @MethodDoc(lists = RoleResourceListResDTO.class, desc = {"PC端", "角色资源-列表", "列表"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/list", method = {RequestMethod.GET})
    @ResponseBody
    public void list(final @FromJson RoleResourceListReqDTO roleresourceListReqDTO) {
        List<RoleResourceListResDTO> items = service.list(roleresourceListReqDTO);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(lists = RoleResourceListResDTO.class, desc = {"PC端", "角色资源-列表", "列表1.1.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/list", headers = {"version=1.1.0"}, method = {RequestMethod.GET})
    @ResponseBody
    public void list1_1_0(final @FromJson RoleResourceListReqDTO roleresourceListReqDTO) {
        List<RoleResourceListResDTO> items = service.list1_1_0(roleresourceListReqDTO);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(lists = RoleResourceListResDTO.class, desc = {"PC端", "角色资源-列表", "列表1.2.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/list", headers = {"version=1.2.0", "platform=APP"}, method = {RequestMethod.GET})
    @ResponseBody
    public void list1_2_0(final @FromJson RoleResourceListReqDTO roleresourceListReqDTO) {
        List<RoleResourceListResDTO> items = service.list1_2_0(roleresourceListReqDTO);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(lists = RoleResourceListResDTO.class, desc = {"PC端", "角色资源-列表", "列表1.3.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/list", headers = {"version=1.3.0", "platform=APP", "device=IOS"}, method = {RequestMethod.GET})
    @ResponseBody
    public void list1_3_0(final @FromJson RoleResourceListReqDTO roleresourceListReqDTO) {
        List<RoleResourceListResDTO> items = service.list1_3_0(roleresourceListReqDTO);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(entity = RoleResourceListResDTO.class, desc = {"PC端", "角色资源-列表", "First查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/listOne", method = {RequestMethod.GET})
    @ResponseBody
    public void listOne(final @FromJson RoleResourceListReqDTO roleresourceListReqDTO) {
        RoleResourceListResDTO entity = service.listOne(roleresourceListReqDTO);
        responseEntity(ModelResult.CODE_200, ModelResult.SUCCESS, entity);
    }

    @MethodDoc(pages = RoleResourcePageResDTO.class, desc = {"PC端", "角色资源-分页", "分页"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/page", method = {RequestMethod.GET})
    @ResponseBody
    public void page(final @FromJson RoleResourcePageReqDTO roleresourcePageReqDTO,
                     final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
                     final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int currentPage = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int pageLimit = Func.isNullOrZero(pageSize) ? 10 : pageSize;

        Page<RoleResourcePageResDTO> roleresourcePage = service.pagination(roleresourcePageReqDTO, currentPage, pageLimit);
        responsePage(ModelResult.CODE_200, ModelResult.SUCCESS, roleresourcePage.getTotal(), roleresourcePage.getRecords(), pageLimit, currentPage);
    }

    @MethodDoc(desc = {"PC端", "角色资源-新增", "新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/add", method = {RequestMethod.POST})
    @ResponseBody
    public void add(final @FromJson RoleResourceAddReqDTO roleresourceAddReqDTO) {
        Boolean isSuccess = service.add(roleresourceAddReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "角色资源-新增", "新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/addAllColumn", method = {RequestMethod.POST})
    @ResponseBody
    public void addAllColumn(final @FromJson RoleResourceAddReqDTO roleresourceAddReqDTO) {
        Boolean isSuccess = service.addAllColumn(roleresourceAddReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "角色资源-新增", "批量新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/addBatchAllColumn", method = {RequestMethod.POST})
    @ResponseBody
    public void addBatchAllColumn(final @FromJson List<RoleResourceAddReqDTO> roleresourceAddReqDTOList) {
        Boolean isSuccess = service.addBatchAllColumn(roleresourceAddReqDTOList);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(entity = RoleResourceShowResDTO.class, desc = {"PC端", "角色资源-详情", "详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/show", method = {RequestMethod.GET})
    @ResponseBody
    public void show(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") Long id) {
        RoleResourceShowResDTO entity = service.show(id);
        responseEntity(ModelResult.CODE_200, ModelResult.SUCCESS, entity);
    }

    @MethodDoc(entity = RoleResourceShowResDTO.class, desc = {"PC端", "角色资源-详情", "详情(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/showByIds", method = {RequestMethod.GET})
    @ResponseBody
    public void showByIds(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<Long> ids) {
        List<RoleResourceShowResDTO> items = service.showByIds(ids);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(desc = {"PC端", "角色资源-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/modify", method = {RequestMethod.POST})
    @ResponseBody
    public void modify(final @FromJson RoleResourceModifyReqDTO roleresourceModifyReqDTO) {
        Boolean isSuccess = service.modify(roleresourceModifyReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "角色资源-更新", "更新(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/modifySelective", method = {RequestMethod.POST})
    @ResponseBody
    public void modifyAllColumn(final @FromJson RoleResourceModifyReqDTO roleresourceModifyReqDTO) {
        Boolean isSuccess = service.modifyAllColumn(roleresourceModifyReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "角色资源-删除", "删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/remove", method = {RequestMethod.POST})
    @ResponseBody
    public void remove(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") Long id) {
        Boolean isSuccess = service.remove(id);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "角色资源-删除", "删除(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/removeBatch", method = {RequestMethod.POST})
    @ResponseBody
    public void removeBatch(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<Long> ids) {
        Boolean isSuccess = service.removeBatch(ids);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "角色资源-删除", "删除(参数)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "roleresource/removeByParams", method = {RequestMethod.POST})
    @ResponseBody
    public void removeByParams(final @FromJson RoleResourceRemoveReqDTO roleresourceRemoveReqDTO) {
        Boolean isSuccess = service.removeByParams(roleresourceRemoveReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }
}
