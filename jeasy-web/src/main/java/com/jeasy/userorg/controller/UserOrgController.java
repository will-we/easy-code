package com.jeasy.userorg.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.jeasy.base.web.controller.BaseController;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.base.web.resolver.FromJson;
import com.jeasy.common.Func;
import com.jeasy.doc.annotation.InitField;
import com.jeasy.doc.annotation.MethodDoc;
import com.jeasy.doc.annotation.StatusEnum;
import com.jeasy.userorg.dto.*;
import com.jeasy.userorg.service.UserOrgService;
import com.jeasy.validate.handler.ValidateNotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 用户机构 Controller
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Controller
public class UserOrgController extends BaseController<UserOrgService> {

    @MethodDoc(lists = UserOrgListResDTO.class, desc = {"PC端", "用户机构-列表", "列表"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/list", method = {RequestMethod.GET})
    @ResponseBody
    public void list(final @FromJson UserOrgListReqDTO userorgListReqDTO) {
        List<UserOrgListResDTO> items = service.list(userorgListReqDTO);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(lists = UserOrgListResDTO.class, desc = {"PC端", "用户机构-列表", "列表1.1.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/list", headers = {"version=1.1.0"}, method = {RequestMethod.GET})
    @ResponseBody
    public void list1_1_0(final @FromJson UserOrgListReqDTO userorgListReqDTO) {
        List<UserOrgListResDTO> items = service.list1_1_0(userorgListReqDTO);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(lists = UserOrgListResDTO.class, desc = {"PC端", "用户机构-列表", "列表1.2.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/list", headers = {"version=1.2.0", "platform=APP"}, method = {RequestMethod.GET})
    @ResponseBody
    public void list1_2_0(final @FromJson UserOrgListReqDTO userorgListReqDTO) {
        List<UserOrgListResDTO> items = service.list1_2_0(userorgListReqDTO);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(lists = UserOrgListResDTO.class, desc = {"PC端", "用户机构-列表", "列表1.3.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/list", headers = {"version=1.3.0", "platform=APP", "device=IOS"}, method = {RequestMethod.GET})
    @ResponseBody
    public void list1_3_0(final @FromJson UserOrgListReqDTO userorgListReqDTO) {
        List<UserOrgListResDTO> items = service.list1_3_0(userorgListReqDTO);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(entity = UserOrgListResDTO.class, desc = {"PC端", "用户机构-列表", "First查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/listOne", method = {RequestMethod.GET})
    @ResponseBody
    public void listOne(final @FromJson UserOrgListReqDTO userorgListReqDTO) {
        UserOrgListResDTO entity = service.listOne(userorgListReqDTO);
        responseEntity(ModelResult.CODE_200, ModelResult.SUCCESS, entity);
    }

    @MethodDoc(pages = UserOrgPageResDTO.class, desc = {"PC端", "用户机构-分页", "分页"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/page", method = {RequestMethod.GET})
    @ResponseBody
    public void page(final @FromJson UserOrgPageReqDTO userorgPageReqDTO,
                     final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
                     final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int currentPage = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int pageLimit = Func.isNullOrZero(pageSize) ? 10 : pageSize;

        Page<UserOrgPageResDTO> userorgPage = service.pagination(userorgPageReqDTO, currentPage, pageLimit);
        responsePage(ModelResult.CODE_200, ModelResult.SUCCESS, userorgPage.getTotal(), userorgPage.getRecords(), pageLimit, currentPage);
    }

    @MethodDoc(desc = {"PC端", "用户机构-新增", "新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/add", method = {RequestMethod.POST})
    @ResponseBody
    public void add(final @FromJson UserOrgAddReqDTO userorgAddReqDTO) {
        Boolean isSuccess = service.add(userorgAddReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "用户机构-新增", "新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/addAllColumn", method = {RequestMethod.POST})
    @ResponseBody
    public void addAllColumn(final @FromJson UserOrgAddReqDTO userorgAddReqDTO) {
        Boolean isSuccess = service.addAllColumn(userorgAddReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "用户机构-新增", "批量新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/addBatchAllColumn", method = {RequestMethod.POST})
    @ResponseBody
    public void addBatchAllColumn(final @FromJson List<UserOrgAddReqDTO> userorgAddReqDTOList) {
        Boolean isSuccess = service.addBatchAllColumn(userorgAddReqDTOList);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(entity = UserOrgShowResDTO.class, desc = {"PC端", "用户机构-详情", "详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/show", method = {RequestMethod.GET})
    @ResponseBody
    public void show(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") Long id) {
        UserOrgShowResDTO entity = service.show(id);
        responseEntity(ModelResult.CODE_200, ModelResult.SUCCESS, entity);
    }

    @MethodDoc(entity = UserOrgShowResDTO.class, desc = {"PC端", "用户机构-详情", "详情(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/showByIds", method = {RequestMethod.GET})
    @ResponseBody
    public void showByIds(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<Long> ids) {
        List<UserOrgShowResDTO> items = service.showByIds(ids);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(desc = {"PC端", "用户机构-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/modify", method = {RequestMethod.POST})
    @ResponseBody
    public void modify(final @FromJson UserOrgModifyReqDTO userorgModifyReqDTO) {
        Boolean isSuccess = service.modify(userorgModifyReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "用户机构-更新", "更新(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/modifySelective", method = {RequestMethod.POST})
    @ResponseBody
    public void modifyAllColumn(final @FromJson UserOrgModifyReqDTO userorgModifyReqDTO) {
        Boolean isSuccess = service.modifyAllColumn(userorgModifyReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "用户机构-删除", "删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/remove", method = {RequestMethod.POST})
    @ResponseBody
    public void remove(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") Long id) {
        Boolean isSuccess = service.remove(id);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "用户机构-删除", "删除(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/removeBatch", method = {RequestMethod.POST})
    @ResponseBody
    public void removeBatch(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<Long> ids) {
        Boolean isSuccess = service.removeBatch(ids);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "用户机构-删除", "删除(参数)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "userorg/removeByParams", method = {RequestMethod.POST})
    @ResponseBody
    public void removeByParams(final @FromJson UserOrgRemoveReqDTO userorgRemoveReqDTO) {
        Boolean isSuccess = service.removeByParams(userorgRemoveReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }
}
