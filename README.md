# JEasy 3.0 一款快速智能代码生成工具【EASY-CODE升级版】

简介
-----------------------------------
JEasy 3.0 是一款快速智能代码生成工具，面向使用Java开发的同仁们的。其关注各框架集成使用的基础代码构建过程。希望整合各种技术的规范和开发标准，能使您摆脱犹豫，摆脱选择的困难，规避没有经验带来的开发风险。不仅生成代码，同时还强调最佳实践，甚至包括名称规范。不仅仅只是提供技术，还会引导您应该如何使用好技术。

源码地址：http://git.oschina.net/taomk/easy-code

特性
-----------------------------------
JEasy 3.0 目前只支持Spring MVC框架，可以从数据库读取表结构，直接生成这WEB工程的代码，包括DAO层，Manager层, Service层, Controller层的代码，皆可以一键生成.

JEasy 3.0 框架生成的WEB工程代码, 自带以下特性:
> 1. 日志切面支持: 规范Controller层, Service层, Manager层, DAO层日志输出, 记录: 执行时间, 执行耗时, 执行参数, 执行结果等信息;
> 2. 参数校验支持: Controller层请求参数的校验, 自定义实现校验框架, 具体可参考DocController Demo演示;
> 3. 接口Doc支持: 接口Doc页面, 提供后端详细接口描述, 方便前端联调;

JEasy 版本升级
----------
- 3.0
 - 去掉JFinal/Rose等框架支持;
 - 引入shiro权限框架支持;
 - 引入Dubbox服务框架支持;
 - 引入rabbitmq消息框架支持;
 - 引入cxf框架支持;
 - 引入Spring scheduler定时任务支持;
 - 引入Redis、Memcached、J2Cache等缓存支持；
- 2.0 支持JFinal框架
 - 添加支持JFinal框架的代码生成模板;
- 1.1.3
 - 新增：接口Doc页面，提供后端详细接口描述；
- 1.1.2
 - 新增：请求参数校验框架；
 - 新增：请求支持Json数据格式参数转换；
- 1.1.1
 - Bug修复：将日志体系由logback改成log4j实现；
 - Bug修复：base-mvc-web list/page方法参数修改成具体实体类型；
- 1.1.0 支持Rose框架
 - 添加支持Rose框架的代码生成模板；
- 1.0.1
 - BaseController重构；
 - Controller/Service/DAO方法命名规范；
 - Controller/Service层添加日志切面，输出执行信息；
 - base-xxx-web工程模板添加logback日志支持；
 - Bug修复：Mapper.ftl insertBatch 不支持配置返回主键问题；
 - Bug修复：完善MyBatisType映射关系；

JEasy 3.0 演示
------------
1. **代码生成：jeasy-code-gen**
2. **运行WEB：jeasy-web，访问http://localhost:8080/doc**
 ![运行WEB，查看Doc接口文档](http://upload-images.jianshu.io/upload_images/2062729-225b3441ab3b3e8f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/620)

打赏作者
-----------

![打赏作者，请作者喝杯咖啡](http://upload-images.jianshu.io/upload_images/2062729-2146cd9df090bae9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/310)

![打赏作者，请作者喝杯咖啡](http://upload-images.jianshu.io/upload_images/2062729-8f491d1d7ff144a3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/310)

技术交流
-----------------------------------
* 	作者：陶邦仁
* 	邮箱：mingkai.tao@gmail.com
*	微信：扫码加微信

![image.png](http://upload-images.jianshu.io/upload_images/2062729-a1be84849dcc17f6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/310)
