package com.jeasy.userrole.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 用户角色
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Data
@TableName("su_user_role")
public class UserRoleEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    public static final String DB_COL_USER_ID = "userId";

    public static final String DB_COL_USER_NAME = "userName";

    public static final String DB_COL_USER_CODE = "userCode";

    public static final String DB_COL_ROLE_ID = "roleId";

    public static final String DB_COL_ROLE_NAME = "roleName";

    public static final String DB_COL_ROLE_CODE = "roleCode";


    /**
     * 用户ID
     */
    @TableField("userId")
    private Long userId;

    /**
     * 用户名称
     */
    @TableField("userName")
    private String userName;

    /**
     * 用户编码
     */
    @TableField("userCode")
    private String userCode;

    /**
     * 角色ID
     */
    @TableField("roleId")
    private Long roleId;

    /**
     * 角色名称
     */
    @TableField("roleName")
    private String roleName;

    /**
     * 角色编码
     */
    @TableField("roleCode")
    private String roleCode;

}
