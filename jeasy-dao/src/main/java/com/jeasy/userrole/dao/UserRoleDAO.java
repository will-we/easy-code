package com.jeasy.userrole.dao;

import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.userrole.entity.UserRoleEntity;

/**
 * 用户角色 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
public interface UserRoleDAO extends BaseDAO<UserRoleEntity> {
}
