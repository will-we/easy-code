package com.jeasy.user.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 用户
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Data
@TableName("su_user")
public class UserEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    public static final String DB_COL_NAME = "name";

    public static final String DB_COL_LOGIN_NAME = "loginName";

    public static final String DB_COL_CODE = "code";

    public static final String DB_COL_PWD = "pwd";

    public static final String DB_COL_SALT = "salt";

    public static final String DB_COL_MOBILE = "mobile";

    public static final String DB_COL_STATUS_VAL = "statusVal";

    public static final String DB_COL_STATUS_CODE = "statusCode";

    public static final String DB_COL_REMARK = "remark";


    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 登录名
     */
    @TableField("loginName")
    private String loginName;

    /**
     * 编码
     */
    @TableField("code")
    private String code;

    /**
     * 密码
     */
    @TableField("pwd")
    private String pwd;

    /**
     * 加密盐
     */
    @TableField("salt")
    private String salt;

    /**
     * 手机号
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 用户状态值:1000=启用,1001=停用
     */
    @TableField("statusVal")
    private Integer statusVal;

    /**
     * 用户状态编码:字典
     */
    @TableField("statusCode")
    private String statusCode;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

}
