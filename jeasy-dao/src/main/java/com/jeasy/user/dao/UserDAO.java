package com.jeasy.user.dao;

import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.user.entity.UserEntity;

/**
 * 用户 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
public interface UserDAO extends BaseDAO<UserEntity> {
}
