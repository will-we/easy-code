package com.jeasy.log.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 日志
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Data
@TableName("bd_log")
public class LogEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    public static final String DB_COL_TABLE_NAME = "tableName";

    public static final String DB_COL_RECORD_ID = "recordId";

    public static final String DB_COL_FIELD_NAME = "fieldName";

    public static final String DB_COL_LOG_TYPE_VAL = "logTypeVal";

    public static final String DB_COL_LOG_TYPE_CODE = "logTypeCode";

    public static final String DB_COL_OPT_TYPE_VAL = "optTypeVal";

    public static final String DB_COL_OPT_TYPE_CODE = "optTypeCode";

    public static final String DB_COL_OPT_DESC = "optDesc";

    public static final String DB_COL_BEFORE_VALUE = "beforeValue";

    public static final String DB_COL_AFTER_VALUE = "afterValue";

    public static final String DB_COL_REMARK = "remark";


    /**
     * 表名称
     */
    @TableField("tableName")
    private String tableName;

    /**
     * 记录ID
     */
    @TableField("recordId")
    private Long recordId;

    /**
     * 字段名称
     */
    @TableField("fieldName")
    private String fieldName;

    /**
     * 日志类型值
     */
    @TableField("logTypeVal")
    private Integer logTypeVal;

    /**
     * 日志类型编码:字典
     */
    @TableField("logTypeCode")
    private String logTypeCode;

    /**
     * 操作类型值
     */
    @TableField("optTypeVal")
    private Integer optTypeVal;

    /**
     * 操作类型编码:字典
     */
    @TableField("optTypeCode")
    private String optTypeCode;

    /**
     * 操作类型描述
     */
    @TableField("optDesc")
    private String optDesc;

    /**
     * 操作前值
     */
    @TableField("beforeValue")
    private String beforeValue;

    /**
     * 操作后值
     */
    @TableField("afterValue")
    private String afterValue;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

}
