package com.jeasy.log.dao;

import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.log.entity.LogEntity;

/**
 * 日志 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
public interface LogDAO extends BaseDAO<LogEntity> {
}
