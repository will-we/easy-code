package com.jeasy.roleresource.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 角色资源
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Data
@TableName("su_role_resource")
public class RoleResourceEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    public static final String DB_COL_ROLE_ID = "roleId";

    public static final String DB_COL_ROLE_NAME = "roleName";

    public static final String DB_COL_ROLE_CODE = "roleCode";

    public static final String DB_COL_RESOURCE_ID = "resourceId";

    public static final String DB_COL_RESOURCE_NAME = "resourceName";

    public static final String DB_COL_RESOURCE_CODE = "resourceCode";


    /**
     * 角色ID
     */
    @TableField("roleId")
    private Long roleId;

    /**
     * 角色名称
     */
    @TableField("roleName")
    private String roleName;

    /**
     * 角色编码
     */
    @TableField("roleCode")
    private String roleCode;

    /**
     * 资源ID
     */
    @TableField("resourceId")
    private Long resourceId;

    /**
     * 资源名称
     */
    @TableField("resourceName")
    private String resourceName;

    /**
     * 资源编码
     */
    @TableField("resourceCode")
    private String resourceCode;

}
