package com.jeasy.organization.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 机构
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Data
@TableName("su_organization")
public class OrganizationEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    public static final String DB_COL_NAME = "name";

    public static final String DB_COL_CODE = "code";

    public static final String DB_COL_ADDRESS = "address";

    public static final String DB_COL_TYPE_VAL = "typeVal";

    public static final String DB_COL_TYPE_CODE = "typeCode";

    public static final String DB_COL_SORT = "sort";

    public static final String DB_COL_IS_LEAF = "isLeaf";

    public static final String DB_COL_PID = "pid";

    public static final String DB_COL_ICON = "icon";

    public static final String DB_COL_REMARK = "remark";


    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 编码
     */
    @TableField("code")
    private String code;

    /**
     * 地址
     */
    @TableField("address")
    private String address;

    /**
     * 机构类型值
     */
    @TableField("typeVal")
    private Integer typeVal;

    /**
     * 机构类型编码:字典
     */
    @TableField("typeCode")
    private String typeCode;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 是否叶子节点:0=否,1=是
     */
    @TableField("isLeaf")
    private Integer isLeaf;

    /**
     * 父ID
     */
    @TableField("pid")
    private Long pid;

    /**
     * 图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 备注/描述
     */
    @TableField("remark")
    private String remark;

}
