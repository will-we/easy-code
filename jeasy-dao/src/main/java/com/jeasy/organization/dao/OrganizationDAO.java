package com.jeasy.organization.dao;

import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.organization.entity.OrganizationEntity;

/**
 * 机构 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
public interface OrganizationDAO extends BaseDAO<OrganizationEntity> {
}
