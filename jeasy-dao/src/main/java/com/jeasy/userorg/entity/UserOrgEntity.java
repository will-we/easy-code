package com.jeasy.userorg.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 用户机构
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Data
@TableName("su_user_org")
public class UserOrgEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    public static final String DB_COL_USER_ID = "userId";

    public static final String DB_COL_USER_NAME = "userName";

    public static final String DB_COL_USER_CODE = "userCode";

    public static final String DB_COL_ORG_ID = "orgId";

    public static final String DB_COL_ORG_NAME = "orgName";

    public static final String DB_COL_ORG_CODE = "orgCode";


    /**
     * 用户ID
     */
    @TableField("userId")
    private Long userId;

    /**
     * 用户名称
     */
    @TableField("userName")
    private String userName;

    /**
     * 用户标示
     */
    @TableField("userCode")
    private String userCode;

    /**
     * 机构ID
     */
    @TableField("orgId")
    private Long orgId;

    /**
     * 机构名称
     */
    @TableField("orgName")
    private String orgName;

    /**
     * 机构编码
     */
    @TableField("orgCode")
    private String orgCode;

}
