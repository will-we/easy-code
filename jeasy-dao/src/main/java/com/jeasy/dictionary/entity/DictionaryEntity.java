package com.jeasy.dictionary.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 字典
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Data
@TableName("bd_dictionary")
public class DictionaryEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    public static final String DB_COL_NAME = "name";

    public static final String DB_COL_CODE = "code";

    public static final String DB_COL_VALUE = "value";

    public static final String DB_COL_TYPE = "type";

    public static final String DB_COL_SORT = "sort";

    public static final String DB_COL_PID = "pid";

    public static final String DB_COL_PCODE = "pcode";


    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 编号
     */
    @TableField("code")
    private String code;

    /**
     * 值
     */
    @TableField("value")
    private Integer value;

    /**
     * 类型
     */
    @TableField("type")
    private String type;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 父ID
     */
    @TableField("pid")
    private Long pid;

    /**
     * 父编号
     */
    @TableField("pcode")
    private String pcode;

}
