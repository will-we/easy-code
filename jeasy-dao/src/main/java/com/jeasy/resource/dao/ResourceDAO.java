package com.jeasy.resource.dao;

import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.resource.entity.ResourceEntity;

/**
 * 菜单 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
public interface ResourceDAO extends BaseDAO<ResourceEntity> {
}
