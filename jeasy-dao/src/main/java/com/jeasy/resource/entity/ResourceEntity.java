package com.jeasy.resource.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 菜单
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Data
@TableName("su_resource")
public class ResourceEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    public static final String DB_COL_NAME = "name";

    public static final String DB_COL_CODE = "code";

    public static final String DB_COL_URL = "url";

    public static final String DB_COL_ICON = "icon";

    public static final String DB_COL_REMARK = "remark";

    public static final String DB_COL_PID = "pid";

    public static final String DB_COL_SORT = "sort";

    public static final String DB_COL_IS_MENU = "isMenu";

    public static final String DB_COL_IS_LEAF = "isLeaf";


    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 编码
     */
    @TableField("code")
    private String code;

    /**
     * URL
     */
    @TableField("url")
    private String url;

    /**
     * 图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 备注/描述
     */
    @TableField("remark")
    private String remark;

    /**
     * 父ID
     */
    @TableField("pid")
    private Long pid;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 是否菜单:0=否,1=是
     */
    @TableField("isMenu")
    private Integer isMenu;

    /**
     * 是否叶子节点:0=否,1=是
     */
    @TableField("isLeaf")
    private Integer isLeaf;

}
