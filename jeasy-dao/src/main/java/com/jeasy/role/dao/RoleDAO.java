package com.jeasy.role.dao;

import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.role.entity.RoleEntity;

/**
 * 角色 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
public interface RoleDAO extends BaseDAO<RoleEntity> {
}
