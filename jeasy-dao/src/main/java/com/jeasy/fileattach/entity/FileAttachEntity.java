package com.jeasy.fileattach.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 文件附件
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Data
@TableName("bd_file_attach")
public class FileAttachEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    public static final String DB_COL_TABLE_NAME = "tableName";

    public static final String DB_COL_RECORD_ID = "recordId";

    public static final String DB_COL_NAME = "name";

    public static final String DB_COL_URL = "url";

    public static final String DB_COL_ICON_URL = "iconUrl";

    public static final String DB_COL_PREVIEW_URL = "previewUrl";


    /**
     * 表名称
     */
    @TableField("tableName")
    private String tableName;

    /**
     * 记录ID
     */
    @TableField("recordId")
    private Long recordId;

    /**
     * 文件原名称
     */
    @TableField("name")
    private String name;

    /**
     * 文件URL
     */
    @TableField("url")
    private String url;

    /**
     * 文件图标URL
     */
    @TableField("iconUrl")
    private String iconUrl;

    /**
     * 文件预览URL
     */
    @TableField("previewUrl")
    private String previewUrl;

}
