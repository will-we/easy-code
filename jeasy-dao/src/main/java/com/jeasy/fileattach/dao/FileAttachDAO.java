package com.jeasy.fileattach.dao;

import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.fileattach.entity.FileAttachEntity;

/**
 * 文件附件 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
public interface FileAttachDAO extends BaseDAO<FileAttachEntity> {
}
