package com.jeasy.organization.manager;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import com.jeasy.base.manager.impl.BaseManagerImpl;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.object.Converter;
import com.jeasy.common.object.MapKit;
import com.jeasy.common.spring.SpringContextHolder;
import com.jeasy.exception.MessageException;
import com.jeasy.organization.dao.OrganizationDAO;
import com.jeasy.organization.dto.*;
import com.jeasy.organization.entity.OrganizationEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 机构 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Component
public class OrganizationManager extends BaseManagerImpl<OrganizationDAO, OrganizationEntity, OrganizationDTO> {

    /**
     * this is a converter demo only for BeanKit.copyProperties
     *
     * @see BeanKit#copyProperties(Object source, Object target, Converter... converters)
     */
    private static final Converter<String, String> DEMO_CONVERTER = new Converter<String, String>("filed1", "filed2") {
        @Override
        public String convert(final String val) {
            return val;
        }
    };

    public static OrganizationManager me() {
        return SpringContextHolder.getBean(OrganizationManager.class);
    }

    public List<OrganizationListResDTO> list(final OrganizationListReqDTO organizationListReqDTO) {
        OrganizationDTO organizationParamsDTO = new OrganizationDTO();
        if (!Func.isEmpty(organizationListReqDTO)) {
            BeanKit.copyProperties(organizationListReqDTO, organizationParamsDTO, DEMO_CONVERTER);
        }

        List<OrganizationDTO> organizationDtoList = super.findList(organizationParamsDTO);

        if (!Func.isEmpty(organizationDtoList)) {
            List<OrganizationListResDTO> items = Lists.newArrayList();
            for (OrganizationDTO organizationDto : organizationDtoList) {
                OrganizationListResDTO organizationListResDTO = new OrganizationListResDTO();
                BeanKit.copyProperties(organizationDto, organizationListResDTO, DEMO_CONVERTER);
                items.add(organizationListResDTO);
            }
            return items;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<OrganizationListResDTO> list1_1_0(final OrganizationListReqDTO organizationListReqDTO) {
        return list(organizationListReqDTO);
    }

    public List<OrganizationListResDTO> list1_2_0(final OrganizationListReqDTO organizationListReqDTO) {
        return list(organizationListReqDTO);
    }

    public List<OrganizationListResDTO> list1_3_0(final OrganizationListReqDTO organizationListReqDTO) {
        return list(organizationListReqDTO);
    }

    public OrganizationListResDTO listOne(final OrganizationListReqDTO organizationListReqDTO) {
        OrganizationDTO organizationParamsDTO = new OrganizationDTO();
        if (!Func.isEmpty(organizationListReqDTO)) {
            BeanKit.copyProperties(organizationListReqDTO, organizationParamsDTO, DEMO_CONVERTER);
        }

        OrganizationDTO organizationDto = super.findOne(organizationParamsDTO);
        if (!Func.isEmpty(organizationDto)) {
            OrganizationListResDTO organizationListResDTO = new OrganizationListResDTO();
            BeanKit.copyProperties(organizationDto, organizationListResDTO, DEMO_CONVERTER);
            return organizationListResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Page<OrganizationPageResDTO> pagination(final OrganizationPageReqDTO organizationPageReqDTO, final Integer currentPage, final Integer pageSize) {
        OrganizationDTO organizationParamsDTO = new OrganizationDTO();
        if (!Func.isEmpty(organizationPageReqDTO)) {
            BeanKit.copyProperties(organizationPageReqDTO, organizationParamsDTO, DEMO_CONVERTER);
        }

        Page<OrganizationDTO> organizationDTOPage = super.findPage(organizationParamsDTO, currentPage, pageSize);

        if (Func.isNotEmpty(organizationDTOPage) && Func.isNotEmpty(organizationDTOPage.getRecords())) {
            List<OrganizationPageResDTO> organizationPageResDTOs = Lists.newArrayList();
            for (OrganizationDTO organizationDto : organizationDTOPage.getRecords()) {
                OrganizationPageResDTO organizationPageResDTO = new OrganizationPageResDTO();
                BeanKit.copyProperties(organizationDto, organizationPageResDTO, DEMO_CONVERTER);
                organizationPageResDTOs.add(organizationPageResDTO);
            }

            Page<OrganizationPageResDTO> organizationPageResDTOPage = new Page<>();
            organizationPageResDTOPage.setRecords(organizationPageResDTOs);
            organizationPageResDTOPage.setTotal(organizationDTOPage.getTotal());
            return organizationPageResDTOPage;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean add(final OrganizationAddReqDTO organizationAddReqDTO) {
        if (Func.isEmpty(organizationAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        OrganizationDTO organizationDto = new OrganizationDTO();
        BeanKit.copyProperties(organizationAddReqDTO, organizationDto, DEMO_CONVERTER);
        return super.save(organizationDto);
    }

    public Boolean addAllColumn(final OrganizationAddReqDTO organizationAddReqDTO) {
        if (Func.isEmpty(organizationAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        OrganizationDTO organizationDto = new OrganizationDTO();
        BeanKit.copyProperties(organizationAddReqDTO, organizationDto, DEMO_CONVERTER);
        return super.saveAllColumn(organizationDto);
    }

    public Boolean addBatchAllColumn(final List<OrganizationAddReqDTO> organizationAddReqDTOList) {
        if (Func.isEmpty(organizationAddReqDTOList)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        List<OrganizationDTO> organizationDTOList = Lists.newArrayList();
        for (OrganizationAddReqDTO organizationAddReqDTO : organizationAddReqDTOList) {
            OrganizationDTO organizationDto = new OrganizationDTO();
            BeanKit.copyProperties(organizationAddReqDTO, organizationDto, DEMO_CONVERTER);
            organizationDTOList.add(organizationDto);
        }
        return super.saveBatchAllColumn(organizationDTOList);
    }

    public OrganizationShowResDTO show(final Long id) {
        OrganizationDTO organizationDto = super.findById(id);

        if (!Func.isEmpty(organizationDto)) {
            OrganizationShowResDTO organizationShowResDTO = new OrganizationShowResDTO();
            BeanKit.copyProperties(organizationDto, organizationShowResDTO, DEMO_CONVERTER);
            return organizationShowResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<OrganizationShowResDTO> showByIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(ModelResult.CODE_200, "集合不能为空且大小大于0");
        }

        List<OrganizationDTO> organizationDtoList = super.findBatchIds(ids);

        if (!Func.isEmpty(organizationDtoList)) {
            List<OrganizationShowResDTO> organizationShowResDTOList = Lists.newArrayList();
            for (OrganizationDTO organizationDto : organizationDtoList) {
                OrganizationShowResDTO organizationShowResDTO = new OrganizationShowResDTO();
                BeanKit.copyProperties(organizationDto, organizationShowResDTO, DEMO_CONVERTER);
                organizationShowResDTOList.add(organizationShowResDTO);
            }
            return organizationShowResDTOList;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean modify(final OrganizationModifyReqDTO organizationModifyReqDTO) {
        if (Func.isEmpty(organizationModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        OrganizationDTO organizationDto = new OrganizationDTO();
        BeanKit.copyProperties(organizationModifyReqDTO, organizationDto, DEMO_CONVERTER);
        return super.modifyById(organizationDto);
    }

    public Boolean modifyAllColumn(final OrganizationModifyReqDTO organizationModifyReqDTO) {
        if (Func.isEmpty(organizationModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        OrganizationDTO organizationDto = new OrganizationDTO();
        BeanKit.copyProperties(organizationModifyReqDTO, organizationDto, DEMO_CONVERTER);
        return super.modifyAllColumnById(organizationDto);
    }

    public Boolean removeByParams(final OrganizationRemoveReqDTO organizationRemoveReqDTO) {
        if (Func.isEmpty(organizationRemoveReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        OrganizationDTO organizationParamsDTO = new OrganizationDTO();
        BeanKit.copyProperties(organizationRemoveReqDTO, organizationParamsDTO, DEMO_CONVERTER);
        return super.remove(organizationParamsDTO);
    }


    @Override
    protected List<OrganizationDTO> entityToDTOList(final List<OrganizationEntity> organizationEntityList) {
        List<OrganizationDTO> organizationDtoList = null;
        if (!Func.isEmpty(organizationEntityList)) {
            organizationDtoList = Lists.newArrayList();
            for (OrganizationEntity organizationEntity : organizationEntityList) {
                organizationDtoList.add(entityToDTO(organizationEntity));
            }
        }
        return organizationDtoList;
    }

    @Override
    protected OrganizationDTO entityToDTO(final OrganizationEntity organizationEntity) {
        OrganizationDTO organizationDto = null;
        if (!Func.isEmpty(organizationEntity)) {
            organizationDto = new OrganizationDTO();
            BeanKit.copyProperties(organizationEntity, organizationDto);
        }
        return organizationDto;
    }

    @Override
    protected List<OrganizationEntity> dtoToEntityList(final List<OrganizationDTO> organizationDtoList) {
        List<OrganizationEntity> organizationEntityList = null;
        if (!Func.isEmpty(organizationDtoList)) {
            organizationEntityList = Lists.newArrayList();
            for (OrganizationDTO organizationDto : organizationDtoList) {
                organizationEntityList.add(dtoToEntity(organizationDto));
            }
        }
        return organizationEntityList;
    }

    @Override
    protected OrganizationEntity dtoToEntity(final OrganizationDTO organizationDto) {
        OrganizationEntity organizationEntity = null;
        if (!Func.isEmpty(organizationDto)) {
            organizationEntity = new OrganizationEntity();
            BeanKit.copyProperties(organizationDto, organizationEntity);
        }
        return organizationEntity;
    }

    @Override
    protected OrganizationEntity mapToEntity(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new OrganizationEntity();
        }
        return (OrganizationEntity) MapKit.toBean(map, OrganizationEntity.class);
    }

    @Override
    protected OrganizationDTO mapToDto(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new OrganizationDTO();
        }
        return (OrganizationDTO) MapKit.toBean(map, OrganizationDTO.class);
    }
}
