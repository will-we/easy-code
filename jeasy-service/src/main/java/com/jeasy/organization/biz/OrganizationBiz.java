package com.jeasy.organization.biz;

import com.jeasy.common.spring.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 机构 Biz
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Component
public class OrganizationBiz {

    public static OrganizationBiz me() {
        return SpringContextHolder.getBean(OrganizationBiz.class);
    }
}
