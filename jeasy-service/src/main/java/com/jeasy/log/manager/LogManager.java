package com.jeasy.log.manager;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import com.jeasy.base.manager.impl.BaseManagerImpl;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.object.Converter;
import com.jeasy.common.object.MapKit;
import com.jeasy.common.spring.SpringContextHolder;
import com.jeasy.exception.MessageException;
import com.jeasy.log.dao.LogDAO;
import com.jeasy.log.dto.*;
import com.jeasy.log.entity.LogEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 日志 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Component
public class LogManager extends BaseManagerImpl<LogDAO, LogEntity, LogDTO> {

    /**
     * this is a converter demo only for BeanKit.copyProperties
     *
     * @see BeanKit#copyProperties(Object source, Object target, Converter... converters)
     */
    private static final Converter<String, String> DEMO_CONVERTER = new Converter<String, String>("filed1", "filed2") {
        @Override
        public String convert(final String val) {
            return val;
        }
    };

    public static LogManager me() {
        return SpringContextHolder.getBean(LogManager.class);
    }

    public List<LogListResDTO> list(final LogListReqDTO logListReqDTO) {
        LogDTO logParamsDTO = new LogDTO();
        if (!Func.isEmpty(logListReqDTO)) {
            BeanKit.copyProperties(logListReqDTO, logParamsDTO, DEMO_CONVERTER);
        }

        List<LogDTO> logDtoList = super.findList(logParamsDTO);

        if (!Func.isEmpty(logDtoList)) {
            List<LogListResDTO> items = Lists.newArrayList();
            for (LogDTO logDto : logDtoList) {
                LogListResDTO logListResDTO = new LogListResDTO();
                BeanKit.copyProperties(logDto, logListResDTO, DEMO_CONVERTER);
                items.add(logListResDTO);
            }
            return items;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<LogListResDTO> list1_1_0(final LogListReqDTO logListReqDTO) {
        return list(logListReqDTO);
    }

    public List<LogListResDTO> list1_2_0(final LogListReqDTO logListReqDTO) {
        return list(logListReqDTO);
    }

    public List<LogListResDTO> list1_3_0(final LogListReqDTO logListReqDTO) {
        return list(logListReqDTO);
    }

    public LogListResDTO listOne(final LogListReqDTO logListReqDTO) {
        LogDTO logParamsDTO = new LogDTO();
        if (!Func.isEmpty(logListReqDTO)) {
            BeanKit.copyProperties(logListReqDTO, logParamsDTO, DEMO_CONVERTER);
        }

        LogDTO logDto = super.findOne(logParamsDTO);
        if (!Func.isEmpty(logDto)) {
            LogListResDTO logListResDTO = new LogListResDTO();
            BeanKit.copyProperties(logDto, logListResDTO, DEMO_CONVERTER);
            return logListResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Page<LogPageResDTO> pagination(final LogPageReqDTO logPageReqDTO, final Integer currentPage, final Integer pageSize) {
        LogDTO logParamsDTO = new LogDTO();
        if (!Func.isEmpty(logPageReqDTO)) {
            BeanKit.copyProperties(logPageReqDTO, logParamsDTO, DEMO_CONVERTER);
        }

        Page<LogDTO> logDTOPage = super.findPage(logParamsDTO, currentPage, pageSize);

        if (Func.isNotEmpty(logDTOPage) && Func.isNotEmpty(logDTOPage.getRecords())) {
            List<LogPageResDTO> logPageResDTOs = Lists.newArrayList();
            for (LogDTO logDto : logDTOPage.getRecords()) {
                LogPageResDTO logPageResDTO = new LogPageResDTO();
                BeanKit.copyProperties(logDto, logPageResDTO, DEMO_CONVERTER);
                logPageResDTOs.add(logPageResDTO);
            }

            Page<LogPageResDTO> logPageResDTOPage = new Page<>();
            logPageResDTOPage.setRecords(logPageResDTOs);
            logPageResDTOPage.setTotal(logDTOPage.getTotal());
            return logPageResDTOPage;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean add(final LogAddReqDTO logAddReqDTO) {
        if (Func.isEmpty(logAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        LogDTO logDto = new LogDTO();
        BeanKit.copyProperties(logAddReqDTO, logDto, DEMO_CONVERTER);
        return super.save(logDto);
    }

    public Boolean addAllColumn(final LogAddReqDTO logAddReqDTO) {
        if (Func.isEmpty(logAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        LogDTO logDto = new LogDTO();
        BeanKit.copyProperties(logAddReqDTO, logDto, DEMO_CONVERTER);
        return super.saveAllColumn(logDto);
    }

    public Boolean addBatchAllColumn(final List<LogAddReqDTO> logAddReqDTOList) {
        if (Func.isEmpty(logAddReqDTOList)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        List<LogDTO> logDTOList = Lists.newArrayList();
        for (LogAddReqDTO logAddReqDTO : logAddReqDTOList) {
            LogDTO logDto = new LogDTO();
            BeanKit.copyProperties(logAddReqDTO, logDto, DEMO_CONVERTER);
            logDTOList.add(logDto);
        }
        return super.saveBatchAllColumn(logDTOList);
    }

    public LogShowResDTO show(final Long id) {
        LogDTO logDto = super.findById(id);

        if (!Func.isEmpty(logDto)) {
            LogShowResDTO logShowResDTO = new LogShowResDTO();
            BeanKit.copyProperties(logDto, logShowResDTO, DEMO_CONVERTER);
            return logShowResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<LogShowResDTO> showByIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(ModelResult.CODE_200, "集合不能为空且大小大于0");
        }

        List<LogDTO> logDtoList = super.findBatchIds(ids);

        if (!Func.isEmpty(logDtoList)) {
            List<LogShowResDTO> logShowResDTOList = Lists.newArrayList();
            for (LogDTO logDto : logDtoList) {
                LogShowResDTO logShowResDTO = new LogShowResDTO();
                BeanKit.copyProperties(logDto, logShowResDTO, DEMO_CONVERTER);
                logShowResDTOList.add(logShowResDTO);
            }
            return logShowResDTOList;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean modify(final LogModifyReqDTO logModifyReqDTO) {
        if (Func.isEmpty(logModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        LogDTO logDto = new LogDTO();
        BeanKit.copyProperties(logModifyReqDTO, logDto, DEMO_CONVERTER);
        return super.modifyById(logDto);
    }

    public Boolean modifyAllColumn(final LogModifyReqDTO logModifyReqDTO) {
        if (Func.isEmpty(logModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        LogDTO logDto = new LogDTO();
        BeanKit.copyProperties(logModifyReqDTO, logDto, DEMO_CONVERTER);
        return super.modifyAllColumnById(logDto);
    }

    public Boolean removeByParams(final LogRemoveReqDTO logRemoveReqDTO) {
        if (Func.isEmpty(logRemoveReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        LogDTO logParamsDTO = new LogDTO();
        BeanKit.copyProperties(logRemoveReqDTO, logParamsDTO, DEMO_CONVERTER);
        return super.remove(logParamsDTO);
    }


    @Override
    protected List<LogDTO> entityToDTOList(final List<LogEntity> logEntityList) {
        List<LogDTO> logDtoList = null;
        if (!Func.isEmpty(logEntityList)) {
            logDtoList = Lists.newArrayList();
            for (LogEntity logEntity : logEntityList) {
                logDtoList.add(entityToDTO(logEntity));
            }
        }
        return logDtoList;
    }

    @Override
    protected LogDTO entityToDTO(final LogEntity logEntity) {
        LogDTO logDto = null;
        if (!Func.isEmpty(logEntity)) {
            logDto = new LogDTO();
            BeanKit.copyProperties(logEntity, logDto);
        }
        return logDto;
    }

    @Override
    protected List<LogEntity> dtoToEntityList(final List<LogDTO> logDtoList) {
        List<LogEntity> logEntityList = null;
        if (!Func.isEmpty(logDtoList)) {
            logEntityList = Lists.newArrayList();
            for (LogDTO logDto : logDtoList) {
                logEntityList.add(dtoToEntity(logDto));
            }
        }
        return logEntityList;
    }

    @Override
    protected LogEntity dtoToEntity(final LogDTO logDto) {
        LogEntity logEntity = null;
        if (!Func.isEmpty(logDto)) {
            logEntity = new LogEntity();
            BeanKit.copyProperties(logDto, logEntity);
        }
        return logEntity;
    }

    @Override
    protected LogEntity mapToEntity(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new LogEntity();
        }
        return (LogEntity) MapKit.toBean(map, LogEntity.class);
    }

    @Override
    protected LogDTO mapToDto(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new LogDTO();
        }
        return (LogDTO) MapKit.toBean(map, LogDTO.class);
    }
}
