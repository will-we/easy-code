package com.jeasy.userrole.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.jeasy.base.service.impl.BaseServiceImpl;
import com.jeasy.userrole.dto.*;
import com.jeasy.userrole.entity.UserRoleEntity;
import com.jeasy.userrole.manager.UserRoleManager;
import com.jeasy.userrole.service.UserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户角色 ServiceImpl
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Service
public class UserRoleServiceImpl extends BaseServiceImpl<UserRoleManager, UserRoleEntity, UserRoleDTO> implements UserRoleService {

    @Override
    public List<UserRoleListResDTO> list(final UserRoleListReqDTO userroleListReqDTO) {
        return manager.list(userroleListReqDTO);
    }

    @Override
    public List<UserRoleListResDTO> list1_1_0(final UserRoleListReqDTO userroleListReqDTO) {
        return manager.list1_1_0(userroleListReqDTO);
    }

    @Override
    public List<UserRoleListResDTO> list1_2_0(final UserRoleListReqDTO userroleListReqDTO) {
        return manager.list1_2_0(userroleListReqDTO);
    }

    @Override
    public List<UserRoleListResDTO> list1_3_0(final UserRoleListReqDTO userroleListReqDTO) {
        return manager.list1_3_0(userroleListReqDTO);
    }

    @Override
    public UserRoleListResDTO listOne(final UserRoleListReqDTO userroleListReqDTO) {
        return manager.listOne(userroleListReqDTO);
    }

    @Override
    public Page<UserRolePageResDTO> pagination(final UserRolePageReqDTO userrolePageReqDTO, final Integer currentPage, final Integer pageSize) {
        return manager.pagination(userrolePageReqDTO, currentPage, pageSize);
    }

    @Override
    public Boolean add(final UserRoleAddReqDTO userroleAddReqDTO) {
        return manager.add(userroleAddReqDTO);
    }

    @Override
    public Boolean addAllColumn(final UserRoleAddReqDTO userroleAddReqDTO) {
        return manager.addAllColumn(userroleAddReqDTO);
    }

    @Override
    public Boolean addBatchAllColumn(final List<UserRoleAddReqDTO> userroleAddReqDTOList) {
        return manager.addBatchAllColumn(userroleAddReqDTOList);
    }

    @Override
    public UserRoleShowResDTO show(final Long id) {
        return manager.show(id);
    }

    @Override
    public List<UserRoleShowResDTO> showByIds(final List<Long> ids) {
        return manager.showByIds(ids);
    }

    @Override
    public Boolean modify(final UserRoleModifyReqDTO userroleModifyReqDTO) {
        return manager.modify(userroleModifyReqDTO);
    }

    @Override
    public Boolean modifyAllColumn(final UserRoleModifyReqDTO userroleModifyReqDTO) {
        return manager.modifyAllColumn(userroleModifyReqDTO);
    }

    @Override
    public Boolean removeByParams(final UserRoleRemoveReqDTO userroleRemoveReqDTO) {
        return manager.removeByParams(userroleRemoveReqDTO);
    }
}
