package com.jeasy.userrole.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.jeasy.base.service.BaseService;
import com.jeasy.userrole.dto.*;

import java.util.List;

/**
 * 用户角色 Service
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
public interface UserRoleService extends BaseService<UserRoleDTO> {

    /**
     * 列表
     *
     * @param userroleListReqDTO 入参DTO
     * @return
     */
    List<UserRoleListResDTO> list(UserRoleListReqDTO userroleListReqDTO);

    List<UserRoleListResDTO> list1_1_0(UserRoleListReqDTO userroleListReqDTO);

    List<UserRoleListResDTO> list1_2_0(UserRoleListReqDTO userroleListReqDTO);

    List<UserRoleListResDTO> list1_3_0(UserRoleListReqDTO userroleListReqDTO);

    /**
     * First查询
     *
     * @param userroleListReqDTO 入参DTO
     * @return
     */
    UserRoleListResDTO listOne(UserRoleListReqDTO userroleListReqDTO);

    /**
     * 分页
     *
     * @param userrolePageReqDTO 入参DTO
     * @param currentPage 当前页
     * @param pageSize   每页大小
     * @return
     */
    Page<UserRolePageResDTO> pagination(UserRolePageReqDTO userrolePageReqDTO, Integer currentPage, Integer pageSize);

    /**
     * 新增
     *
     * @param userroleAddReqDTO 入参DTO
     * @return
     */
    Boolean add(UserRoleAddReqDTO userroleAddReqDTO);

    /**
     * 新增(所有字段)
     *
     * @param userroleAddReqDTO 入参DTO
     * @return
     */
    Boolean addAllColumn(UserRoleAddReqDTO userroleAddReqDTO);

    /**
     * 批量新增(所有字段)
     *
     * @param userroleAddReqDTOList 入参DTO
     * @return
     */
    Boolean addBatchAllColumn(List<UserRoleAddReqDTO> userroleAddReqDTOList);

    /**
     * 详情
     *
     * @param id 主键ID
     * @return
     */
    UserRoleShowResDTO show(Long id);

    /**
     * 批量详情
     *
     * @param ids 主键IDs
     * @return
     */
    List<UserRoleShowResDTO> showByIds(List<Long> ids);

    /**
     * 修改
     *
     * @param userroleModifyReqDTO 入参DTO
     * @return
     */
    Boolean modify(UserRoleModifyReqDTO userroleModifyReqDTO);

    /**
     * 修改(所有字段)
     *
     * @param userroleModifyReqDTO 入参DTO
     * @return
     */
    Boolean modifyAllColumn(UserRoleModifyReqDTO userroleModifyReqDTO);

    /**
     * 参数删除
     *
     * @param userroleRemoveReqDTO 入参DTO
     * @return
     */
    Boolean removeByParams(UserRoleRemoveReqDTO userroleRemoveReqDTO);
}
