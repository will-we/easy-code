package com.jeasy.userrole.manager;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import com.jeasy.base.manager.impl.BaseManagerImpl;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.object.Converter;
import com.jeasy.common.object.MapKit;
import com.jeasy.common.spring.SpringContextHolder;
import com.jeasy.exception.MessageException;
import com.jeasy.userrole.dao.UserRoleDAO;
import com.jeasy.userrole.dto.*;
import com.jeasy.userrole.entity.UserRoleEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 用户角色 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Component
public class UserRoleManager extends BaseManagerImpl<UserRoleDAO, UserRoleEntity, UserRoleDTO> {

    /**
     * this is a converter demo only for BeanKit.copyProperties
     *
     * @see BeanKit#copyProperties(Object source, Object target, Converter... converters)
     */
    private static final Converter<String, String> DEMO_CONVERTER = new Converter<String, String>("filed1", "filed2") {
        @Override
        public String convert(final String val) {
            return val;
        }
    };

    public static UserRoleManager me() {
        return SpringContextHolder.getBean(UserRoleManager.class);
    }

    public List<UserRoleListResDTO> list(final UserRoleListReqDTO userroleListReqDTO) {
        UserRoleDTO userroleParamsDTO = new UserRoleDTO();
        if (!Func.isEmpty(userroleListReqDTO)) {
            BeanKit.copyProperties(userroleListReqDTO, userroleParamsDTO, DEMO_CONVERTER);
        }

        List<UserRoleDTO> userroleDtoList = super.findList(userroleParamsDTO);

        if (!Func.isEmpty(userroleDtoList)) {
            List<UserRoleListResDTO> items = Lists.newArrayList();
            for (UserRoleDTO userroleDto : userroleDtoList) {
                UserRoleListResDTO userroleListResDTO = new UserRoleListResDTO();
                BeanKit.copyProperties(userroleDto, userroleListResDTO, DEMO_CONVERTER);
                items.add(userroleListResDTO);
            }
            return items;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<UserRoleListResDTO> list1_1_0(final UserRoleListReqDTO userroleListReqDTO) {
        return list(userroleListReqDTO);
    }

    public List<UserRoleListResDTO> list1_2_0(final UserRoleListReqDTO userroleListReqDTO) {
        return list(userroleListReqDTO);
    }

    public List<UserRoleListResDTO> list1_3_0(final UserRoleListReqDTO userroleListReqDTO) {
        return list(userroleListReqDTO);
    }

    public UserRoleListResDTO listOne(final UserRoleListReqDTO userroleListReqDTO) {
        UserRoleDTO userroleParamsDTO = new UserRoleDTO();
        if (!Func.isEmpty(userroleListReqDTO)) {
            BeanKit.copyProperties(userroleListReqDTO, userroleParamsDTO, DEMO_CONVERTER);
        }

        UserRoleDTO userroleDto = super.findOne(userroleParamsDTO);
        if (!Func.isEmpty(userroleDto)) {
            UserRoleListResDTO userroleListResDTO = new UserRoleListResDTO();
            BeanKit.copyProperties(userroleDto, userroleListResDTO, DEMO_CONVERTER);
            return userroleListResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Page<UserRolePageResDTO> pagination(final UserRolePageReqDTO userrolePageReqDTO, final Integer currentPage, final Integer pageSize) {
        UserRoleDTO userroleParamsDTO = new UserRoleDTO();
        if (!Func.isEmpty(userrolePageReqDTO)) {
            BeanKit.copyProperties(userrolePageReqDTO, userroleParamsDTO, DEMO_CONVERTER);
        }

        Page<UserRoleDTO> userroleDTOPage = super.findPage(userroleParamsDTO, currentPage, pageSize);

        if (Func.isNotEmpty(userroleDTOPage) && Func.isNotEmpty(userroleDTOPage.getRecords())) {
            List<UserRolePageResDTO> userrolePageResDTOs = Lists.newArrayList();
            for (UserRoleDTO userroleDto : userroleDTOPage.getRecords()) {
                UserRolePageResDTO userrolePageResDTO = new UserRolePageResDTO();
                BeanKit.copyProperties(userroleDto, userrolePageResDTO, DEMO_CONVERTER);
                userrolePageResDTOs.add(userrolePageResDTO);
            }

            Page<UserRolePageResDTO> userrolePageResDTOPage = new Page<>();
            userrolePageResDTOPage.setRecords(userrolePageResDTOs);
            userrolePageResDTOPage.setTotal(userroleDTOPage.getTotal());
            return userrolePageResDTOPage;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean add(final UserRoleAddReqDTO userroleAddReqDTO) {
        if (Func.isEmpty(userroleAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        UserRoleDTO userroleDto = new UserRoleDTO();
        BeanKit.copyProperties(userroleAddReqDTO, userroleDto, DEMO_CONVERTER);
        return super.save(userroleDto);
    }

    public Boolean addAllColumn(final UserRoleAddReqDTO userroleAddReqDTO) {
        if (Func.isEmpty(userroleAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        UserRoleDTO userroleDto = new UserRoleDTO();
        BeanKit.copyProperties(userroleAddReqDTO, userroleDto, DEMO_CONVERTER);
        return super.saveAllColumn(userroleDto);
    }

    public Boolean addBatchAllColumn(final List<UserRoleAddReqDTO> userroleAddReqDTOList) {
        if (Func.isEmpty(userroleAddReqDTOList)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        List<UserRoleDTO> userroleDTOList = Lists.newArrayList();
        for (UserRoleAddReqDTO userroleAddReqDTO : userroleAddReqDTOList) {
            UserRoleDTO userroleDto = new UserRoleDTO();
            BeanKit.copyProperties(userroleAddReqDTO, userroleDto, DEMO_CONVERTER);
            userroleDTOList.add(userroleDto);
        }
        return super.saveBatchAllColumn(userroleDTOList);
    }

    public UserRoleShowResDTO show(final Long id) {
        UserRoleDTO userroleDto = super.findById(id);

        if (!Func.isEmpty(userroleDto)) {
            UserRoleShowResDTO userroleShowResDTO = new UserRoleShowResDTO();
            BeanKit.copyProperties(userroleDto, userroleShowResDTO, DEMO_CONVERTER);
            return userroleShowResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<UserRoleShowResDTO> showByIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(ModelResult.CODE_200, "集合不能为空且大小大于0");
        }

        List<UserRoleDTO> userroleDtoList = super.findBatchIds(ids);

        if (!Func.isEmpty(userroleDtoList)) {
            List<UserRoleShowResDTO> userroleShowResDTOList = Lists.newArrayList();
            for (UserRoleDTO userroleDto : userroleDtoList) {
                UserRoleShowResDTO userroleShowResDTO = new UserRoleShowResDTO();
                BeanKit.copyProperties(userroleDto, userroleShowResDTO, DEMO_CONVERTER);
                userroleShowResDTOList.add(userroleShowResDTO);
            }
            return userroleShowResDTOList;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean modify(final UserRoleModifyReqDTO userroleModifyReqDTO) {
        if (Func.isEmpty(userroleModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        UserRoleDTO userroleDto = new UserRoleDTO();
        BeanKit.copyProperties(userroleModifyReqDTO, userroleDto, DEMO_CONVERTER);
        return super.modifyById(userroleDto);
    }

    public Boolean modifyAllColumn(final UserRoleModifyReqDTO userroleModifyReqDTO) {
        if (Func.isEmpty(userroleModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        UserRoleDTO userroleDto = new UserRoleDTO();
        BeanKit.copyProperties(userroleModifyReqDTO, userroleDto, DEMO_CONVERTER);
        return super.modifyAllColumnById(userroleDto);
    }

    public Boolean removeByParams(final UserRoleRemoveReqDTO userroleRemoveReqDTO) {
        if (Func.isEmpty(userroleRemoveReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        UserRoleDTO userroleParamsDTO = new UserRoleDTO();
        BeanKit.copyProperties(userroleRemoveReqDTO, userroleParamsDTO, DEMO_CONVERTER);
        return super.remove(userroleParamsDTO);
    }


    @Override
    protected List<UserRoleDTO> entityToDTOList(final List<UserRoleEntity> userroleEntityList) {
        List<UserRoleDTO> userroleDtoList = null;
        if (!Func.isEmpty(userroleEntityList)) {
            userroleDtoList = Lists.newArrayList();
            for (UserRoleEntity userroleEntity : userroleEntityList) {
                userroleDtoList.add(entityToDTO(userroleEntity));
            }
        }
        return userroleDtoList;
    }

    @Override
    protected UserRoleDTO entityToDTO(final UserRoleEntity userroleEntity) {
        UserRoleDTO userroleDto = null;
        if (!Func.isEmpty(userroleEntity)) {
            userroleDto = new UserRoleDTO();
            BeanKit.copyProperties(userroleEntity, userroleDto);
        }
        return userroleDto;
    }

    @Override
    protected List<UserRoleEntity> dtoToEntityList(final List<UserRoleDTO> userroleDtoList) {
        List<UserRoleEntity> userroleEntityList = null;
        if (!Func.isEmpty(userroleDtoList)) {
            userroleEntityList = Lists.newArrayList();
            for (UserRoleDTO userroleDto : userroleDtoList) {
                userroleEntityList.add(dtoToEntity(userroleDto));
            }
        }
        return userroleEntityList;
    }

    @Override
    protected UserRoleEntity dtoToEntity(final UserRoleDTO userroleDto) {
        UserRoleEntity userroleEntity = null;
        if (!Func.isEmpty(userroleDto)) {
            userroleEntity = new UserRoleEntity();
            BeanKit.copyProperties(userroleDto, userroleEntity);
        }
        return userroleEntity;
    }

    @Override
    protected UserRoleEntity mapToEntity(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new UserRoleEntity();
        }
        return (UserRoleEntity) MapKit.toBean(map, UserRoleEntity.class);
    }

    @Override
    protected UserRoleDTO mapToDto(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new UserRoleDTO();
        }
        return (UserRoleDTO) MapKit.toBean(map, UserRoleDTO.class);
    }
}
