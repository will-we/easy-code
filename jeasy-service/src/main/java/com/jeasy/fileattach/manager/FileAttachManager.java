package com.jeasy.fileattach.manager;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import com.jeasy.base.manager.impl.BaseManagerImpl;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.object.Converter;
import com.jeasy.common.object.MapKit;
import com.jeasy.common.spring.SpringContextHolder;
import com.jeasy.exception.MessageException;
import com.jeasy.fileattach.dao.FileAttachDAO;
import com.jeasy.fileattach.dto.*;
import com.jeasy.fileattach.entity.FileAttachEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 文件附件 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Component
public class FileAttachManager extends BaseManagerImpl<FileAttachDAO, FileAttachEntity, FileAttachDTO> {

    /**
     * this is a converter demo only for BeanKit.copyProperties
     *
     * @see BeanKit#copyProperties(Object source, Object target, Converter... converters)
     */
    private static final Converter<String, String> DEMO_CONVERTER = new Converter<String, String>("filed1", "filed2") {
        @Override
        public String convert(final String val) {
            return val;
        }
    };

    public static FileAttachManager me() {
        return SpringContextHolder.getBean(FileAttachManager.class);
    }

    public List<FileAttachListResDTO> list(final FileAttachListReqDTO fileattachListReqDTO) {
        FileAttachDTO fileattachParamsDTO = new FileAttachDTO();
        if (!Func.isEmpty(fileattachListReqDTO)) {
            BeanKit.copyProperties(fileattachListReqDTO, fileattachParamsDTO, DEMO_CONVERTER);
        }

        List<FileAttachDTO> fileattachDtoList = super.findList(fileattachParamsDTO);

        if (!Func.isEmpty(fileattachDtoList)) {
            List<FileAttachListResDTO> items = Lists.newArrayList();
            for (FileAttachDTO fileattachDto : fileattachDtoList) {
                FileAttachListResDTO fileattachListResDTO = new FileAttachListResDTO();
                BeanKit.copyProperties(fileattachDto, fileattachListResDTO, DEMO_CONVERTER);
                items.add(fileattachListResDTO);
            }
            return items;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<FileAttachListResDTO> list1_1_0(final FileAttachListReqDTO fileattachListReqDTO) {
        return list(fileattachListReqDTO);
    }

    public List<FileAttachListResDTO> list1_2_0(final FileAttachListReqDTO fileattachListReqDTO) {
        return list(fileattachListReqDTO);
    }

    public List<FileAttachListResDTO> list1_3_0(final FileAttachListReqDTO fileattachListReqDTO) {
        return list(fileattachListReqDTO);
    }

    public FileAttachListResDTO listOne(final FileAttachListReqDTO fileattachListReqDTO) {
        FileAttachDTO fileattachParamsDTO = new FileAttachDTO();
        if (!Func.isEmpty(fileattachListReqDTO)) {
            BeanKit.copyProperties(fileattachListReqDTO, fileattachParamsDTO, DEMO_CONVERTER);
        }

        FileAttachDTO fileattachDto = super.findOne(fileattachParamsDTO);
        if (!Func.isEmpty(fileattachDto)) {
            FileAttachListResDTO fileattachListResDTO = new FileAttachListResDTO();
            BeanKit.copyProperties(fileattachDto, fileattachListResDTO, DEMO_CONVERTER);
            return fileattachListResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Page<FileAttachPageResDTO> pagination(final FileAttachPageReqDTO fileattachPageReqDTO, final Integer currentPage, final Integer pageSize) {
        FileAttachDTO fileattachParamsDTO = new FileAttachDTO();
        if (!Func.isEmpty(fileattachPageReqDTO)) {
            BeanKit.copyProperties(fileattachPageReqDTO, fileattachParamsDTO, DEMO_CONVERTER);
        }

        Page<FileAttachDTO> fileattachDTOPage = super.findPage(fileattachParamsDTO, currentPage, pageSize);

        if (Func.isNotEmpty(fileattachDTOPage) && Func.isNotEmpty(fileattachDTOPage.getRecords())) {
            List<FileAttachPageResDTO> fileattachPageResDTOs = Lists.newArrayList();
            for (FileAttachDTO fileattachDto : fileattachDTOPage.getRecords()) {
                FileAttachPageResDTO fileattachPageResDTO = new FileAttachPageResDTO();
                BeanKit.copyProperties(fileattachDto, fileattachPageResDTO, DEMO_CONVERTER);
                fileattachPageResDTOs.add(fileattachPageResDTO);
            }

            Page<FileAttachPageResDTO> fileattachPageResDTOPage = new Page<>();
            fileattachPageResDTOPage.setRecords(fileattachPageResDTOs);
            fileattachPageResDTOPage.setTotal(fileattachDTOPage.getTotal());
            return fileattachPageResDTOPage;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean add(final FileAttachAddReqDTO fileattachAddReqDTO) {
        if (Func.isEmpty(fileattachAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        FileAttachDTO fileattachDto = new FileAttachDTO();
        BeanKit.copyProperties(fileattachAddReqDTO, fileattachDto, DEMO_CONVERTER);
        return super.save(fileattachDto);
    }

    public Boolean addAllColumn(final FileAttachAddReqDTO fileattachAddReqDTO) {
        if (Func.isEmpty(fileattachAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        FileAttachDTO fileattachDto = new FileAttachDTO();
        BeanKit.copyProperties(fileattachAddReqDTO, fileattachDto, DEMO_CONVERTER);
        return super.saveAllColumn(fileattachDto);
    }

    public Boolean addBatchAllColumn(final List<FileAttachAddReqDTO> fileattachAddReqDTOList) {
        if (Func.isEmpty(fileattachAddReqDTOList)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        List<FileAttachDTO> fileattachDTOList = Lists.newArrayList();
        for (FileAttachAddReqDTO fileattachAddReqDTO : fileattachAddReqDTOList) {
            FileAttachDTO fileattachDto = new FileAttachDTO();
            BeanKit.copyProperties(fileattachAddReqDTO, fileattachDto, DEMO_CONVERTER);
            fileattachDTOList.add(fileattachDto);
        }
        return super.saveBatchAllColumn(fileattachDTOList);
    }

    public FileAttachShowResDTO show(final Long id) {
        FileAttachDTO fileattachDto = super.findById(id);

        if (!Func.isEmpty(fileattachDto)) {
            FileAttachShowResDTO fileattachShowResDTO = new FileAttachShowResDTO();
            BeanKit.copyProperties(fileattachDto, fileattachShowResDTO, DEMO_CONVERTER);
            return fileattachShowResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<FileAttachShowResDTO> showByIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(ModelResult.CODE_200, "集合不能为空且大小大于0");
        }

        List<FileAttachDTO> fileattachDtoList = super.findBatchIds(ids);

        if (!Func.isEmpty(fileattachDtoList)) {
            List<FileAttachShowResDTO> fileattachShowResDTOList = Lists.newArrayList();
            for (FileAttachDTO fileattachDto : fileattachDtoList) {
                FileAttachShowResDTO fileattachShowResDTO = new FileAttachShowResDTO();
                BeanKit.copyProperties(fileattachDto, fileattachShowResDTO, DEMO_CONVERTER);
                fileattachShowResDTOList.add(fileattachShowResDTO);
            }
            return fileattachShowResDTOList;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean modify(final FileAttachModifyReqDTO fileattachModifyReqDTO) {
        if (Func.isEmpty(fileattachModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        FileAttachDTO fileattachDto = new FileAttachDTO();
        BeanKit.copyProperties(fileattachModifyReqDTO, fileattachDto, DEMO_CONVERTER);
        return super.modifyById(fileattachDto);
    }

    public Boolean modifyAllColumn(final FileAttachModifyReqDTO fileattachModifyReqDTO) {
        if (Func.isEmpty(fileattachModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        FileAttachDTO fileattachDto = new FileAttachDTO();
        BeanKit.copyProperties(fileattachModifyReqDTO, fileattachDto, DEMO_CONVERTER);
        return super.modifyAllColumnById(fileattachDto);
    }

    public Boolean removeByParams(final FileAttachRemoveReqDTO fileattachRemoveReqDTO) {
        if (Func.isEmpty(fileattachRemoveReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        FileAttachDTO fileattachParamsDTO = new FileAttachDTO();
        BeanKit.copyProperties(fileattachRemoveReqDTO, fileattachParamsDTO, DEMO_CONVERTER);
        return super.remove(fileattachParamsDTO);
    }


    @Override
    protected List<FileAttachDTO> entityToDTOList(final List<FileAttachEntity> fileattachEntityList) {
        List<FileAttachDTO> fileattachDtoList = null;
        if (!Func.isEmpty(fileattachEntityList)) {
            fileattachDtoList = Lists.newArrayList();
            for (FileAttachEntity fileattachEntity : fileattachEntityList) {
                fileattachDtoList.add(entityToDTO(fileattachEntity));
            }
        }
        return fileattachDtoList;
    }

    @Override
    protected FileAttachDTO entityToDTO(final FileAttachEntity fileattachEntity) {
        FileAttachDTO fileattachDto = null;
        if (!Func.isEmpty(fileattachEntity)) {
            fileattachDto = new FileAttachDTO();
            BeanKit.copyProperties(fileattachEntity, fileattachDto);
        }
        return fileattachDto;
    }

    @Override
    protected List<FileAttachEntity> dtoToEntityList(final List<FileAttachDTO> fileattachDtoList) {
        List<FileAttachEntity> fileattachEntityList = null;
        if (!Func.isEmpty(fileattachDtoList)) {
            fileattachEntityList = Lists.newArrayList();
            for (FileAttachDTO fileattachDto : fileattachDtoList) {
                fileattachEntityList.add(dtoToEntity(fileattachDto));
            }
        }
        return fileattachEntityList;
    }

    @Override
    protected FileAttachEntity dtoToEntity(final FileAttachDTO fileattachDto) {
        FileAttachEntity fileattachEntity = null;
        if (!Func.isEmpty(fileattachDto)) {
            fileattachEntity = new FileAttachEntity();
            BeanKit.copyProperties(fileattachDto, fileattachEntity);
        }
        return fileattachEntity;
    }

    @Override
    protected FileAttachEntity mapToEntity(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new FileAttachEntity();
        }
        return (FileAttachEntity) MapKit.toBean(map, FileAttachEntity.class);
    }

    @Override
    protected FileAttachDTO mapToDto(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new FileAttachDTO();
        }
        return (FileAttachDTO) MapKit.toBean(map, FileAttachDTO.class);
    }
}
