package com.jeasy.user.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.jeasy.base.service.BaseService;
import com.jeasy.user.dto.*;

import java.util.List;

/**
 * 用户 Service
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
public interface UserService extends BaseService<UserDTO> {

    /**
     * 列表
     *
     * @param userListReqDTO 入参DTO
     * @return
     */
    List<UserListResDTO> list(UserListReqDTO userListReqDTO);

    List<UserListResDTO> list1_1_0(UserListReqDTO userListReqDTO);

    List<UserListResDTO> list1_2_0(UserListReqDTO userListReqDTO);

    List<UserListResDTO> list1_3_0(UserListReqDTO userListReqDTO);

    /**
     * First查询
     *
     * @param userListReqDTO 入参DTO
     * @return
     */
    UserListResDTO listOne(UserListReqDTO userListReqDTO);

    /**
     * 分页
     *
     * @param userPageReqDTO 入参DTO
     * @param currentPage 当前页
     * @param pageSize   每页大小
     * @return
     */
    Page<UserPageResDTO> pagination(UserPageReqDTO userPageReqDTO, Integer currentPage, Integer pageSize);

    /**
     * 新增
     *
     * @param userAddReqDTO 入参DTO
     * @return
     */
    Boolean add(UserAddReqDTO userAddReqDTO);

    /**
     * 新增(所有字段)
     *
     * @param userAddReqDTO 入参DTO
     * @return
     */
    Boolean addAllColumn(UserAddReqDTO userAddReqDTO);

    /**
     * 批量新增(所有字段)
     *
     * @param userAddReqDTOList 入参DTO
     * @return
     */
    Boolean addBatchAllColumn(List<UserAddReqDTO> userAddReqDTOList);

    /**
     * 详情
     *
     * @param id 主键ID
     * @return
     */
    UserShowResDTO show(Long id);

    /**
     * 批量详情
     *
     * @param ids 主键IDs
     * @return
     */
    List<UserShowResDTO> showByIds(List<Long> ids);

    /**
     * 修改
     *
     * @param userModifyReqDTO 入参DTO
     * @return
     */
    Boolean modify(UserModifyReqDTO userModifyReqDTO);

    /**
     * 修改(所有字段)
     *
     * @param userModifyReqDTO 入参DTO
     * @return
     */
    Boolean modifyAllColumn(UserModifyReqDTO userModifyReqDTO);

    /**
     * 参数删除
     *
     * @param userRemoveReqDTO 入参DTO
     * @return
     */
    Boolean removeByParams(UserRemoveReqDTO userRemoveReqDTO);
}
