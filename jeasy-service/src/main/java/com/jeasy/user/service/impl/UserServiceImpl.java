package com.jeasy.user.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.jeasy.base.service.impl.BaseServiceImpl;
import com.jeasy.user.dto.*;
import com.jeasy.user.entity.UserEntity;
import com.jeasy.user.manager.UserManager;
import com.jeasy.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户 ServiceImpl
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Service
public class UserServiceImpl extends BaseServiceImpl<UserManager, UserEntity, UserDTO> implements UserService {

    @Override
    public List<UserListResDTO> list(final UserListReqDTO userListReqDTO) {
        return manager.list(userListReqDTO);
    }

    @Override
    public List<UserListResDTO> list1_1_0(final UserListReqDTO userListReqDTO) {
        return manager.list1_1_0(userListReqDTO);
    }

    @Override
    public List<UserListResDTO> list1_2_0(final UserListReqDTO userListReqDTO) {
        return manager.list1_2_0(userListReqDTO);
    }

    @Override
    public List<UserListResDTO> list1_3_0(final UserListReqDTO userListReqDTO) {
        return manager.list1_3_0(userListReqDTO);
    }

    @Override
    public UserListResDTO listOne(final UserListReqDTO userListReqDTO) {
        return manager.listOne(userListReqDTO);
    }

    @Override
    public Page<UserPageResDTO> pagination(final UserPageReqDTO userPageReqDTO, final Integer currentPage, final Integer pageSize) {
        return manager.pagination(userPageReqDTO, currentPage, pageSize);
    }

    @Override
    public Boolean add(final UserAddReqDTO userAddReqDTO) {
        return manager.add(userAddReqDTO);
    }

    @Override
    public Boolean addAllColumn(final UserAddReqDTO userAddReqDTO) {
        return manager.addAllColumn(userAddReqDTO);
    }

    @Override
    public Boolean addBatchAllColumn(final List<UserAddReqDTO> userAddReqDTOList) {
        return manager.addBatchAllColumn(userAddReqDTOList);
    }

    @Override
    public UserShowResDTO show(final Long id) {
        return manager.show(id);
    }

    @Override
    public List<UserShowResDTO> showByIds(final List<Long> ids) {
        return manager.showByIds(ids);
    }

    @Override
    public Boolean modify(final UserModifyReqDTO userModifyReqDTO) {
        return manager.modify(userModifyReqDTO);
    }

    @Override
    public Boolean modifyAllColumn(final UserModifyReqDTO userModifyReqDTO) {
        return manager.modifyAllColumn(userModifyReqDTO);
    }

    @Override
    public Boolean removeByParams(final UserRemoveReqDTO userRemoveReqDTO) {
        return manager.removeByParams(userRemoveReqDTO);
    }
}
