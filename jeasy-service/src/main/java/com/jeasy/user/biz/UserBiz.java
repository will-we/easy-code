package com.jeasy.user.biz;

import com.jeasy.common.spring.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 用户 Biz
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Component
public class UserBiz {

    public static UserBiz me() {
        return SpringContextHolder.getBean(UserBiz.class);
    }
}
