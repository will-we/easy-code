package com.jeasy.user.manager;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import com.jeasy.base.manager.impl.BaseManagerImpl;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.object.Converter;
import com.jeasy.common.object.MapKit;
import com.jeasy.common.spring.SpringContextHolder;
import com.jeasy.common.str.StrKit;
import com.jeasy.exception.MessageException;
import com.jeasy.user.dao.UserDAO;
import com.jeasy.user.dto.*;
import com.jeasy.user.entity.UserEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 用户 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Component
public class UserManager extends BaseManagerImpl<UserDAO, UserEntity, UserDTO> {

    /**
     * this is a converter demo only for BeanKit.copyProperties
     *
     * @see BeanKit#copyProperties(Object source, Object target, Converter... converters)
     */
    private static final Converter<String, String> DEMO_CONVERTER = new Converter<String, String>("filed1", "filed2") {
        @Override
        public String convert(final String val) {
            return val;
        }
    };

    public static UserManager me() {
        return SpringContextHolder.getBean(UserManager.class);
    }

    public List<UserListResDTO> list(final UserListReqDTO userListReqDTO) {
        UserDTO userParamsDTO = new UserDTO();
        if (!Func.isEmpty(userListReqDTO)) {
            BeanKit.copyProperties(userListReqDTO, userParamsDTO, DEMO_CONVERTER);
        }

        List<UserDTO> userDtoList = super.findList(userParamsDTO);

        if (!Func.isEmpty(userDtoList)) {
            List<UserListResDTO> items = Lists.newArrayList();
            for (UserDTO userDto : userDtoList) {
                UserListResDTO userListResDTO = new UserListResDTO();
                BeanKit.copyProperties(userDto, userListResDTO, DEMO_CONVERTER);
                items.add(userListResDTO);
            }
            return items;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<UserListResDTO> list1_1_0(final UserListReqDTO userListReqDTO) {
        return list(userListReqDTO);
    }

    public List<UserListResDTO> list1_2_0(final UserListReqDTO userListReqDTO) {
        return list(userListReqDTO);
    }

    public List<UserListResDTO> list1_3_0(final UserListReqDTO userListReqDTO) {
        return list(userListReqDTO);
    }

    public UserListResDTO listOne(final UserListReqDTO userListReqDTO) {
        UserDTO userParamsDTO = new UserDTO();
        if (!Func.isEmpty(userListReqDTO)) {
            BeanKit.copyProperties(userListReqDTO, userParamsDTO, DEMO_CONVERTER);
        }

        UserDTO userDto = super.findOne(userParamsDTO);
        if (!Func.isEmpty(userDto)) {
            UserListResDTO userListResDTO = new UserListResDTO();
            BeanKit.copyProperties(userDto, userListResDTO, DEMO_CONVERTER);
            return userListResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Page<UserPageResDTO> pagination(final UserPageReqDTO userPageReqDTO, final Integer currentPage, final Integer pageSize) {
        UserDTO userParamsDTO = new UserDTO();
        if (!Func.isEmpty(userPageReqDTO)) {
            BeanKit.copyProperties(userPageReqDTO, userParamsDTO, DEMO_CONVERTER);
        }

        Page<UserDTO> userDTOPage = super.findPage(userParamsDTO, currentPage, pageSize);

        if (Func.isNotEmpty(userDTOPage) && Func.isNotEmpty(userDTOPage.getRecords())) {
            List<UserPageResDTO> userPageResDTOs = Lists.newArrayList();
            for (UserDTO userDto : userDTOPage.getRecords()) {
                UserPageResDTO userPageResDTO = new UserPageResDTO();
                BeanKit.copyProperties(userDto, userPageResDTO, DEMO_CONVERTER);
                userPageResDTOs.add(userPageResDTO);
            }

            Page<UserPageResDTO> userPageResDTOPage = new Page<>();
            userPageResDTOPage.setRecords(userPageResDTOs);
            userPageResDTOPage.setTotal(userDTOPage.getTotal());
            return userPageResDTOPage;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean add(final UserAddReqDTO userAddReqDTO) {
        if (Func.isEmpty(userAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        UserDTO userDto = new UserDTO();
        BeanKit.copyProperties(userAddReqDTO, userDto, DEMO_CONVERTER);
        userDto.setPwd(StrKit.EMPTY);
        return super.save(userDto);
    }

    public Boolean addAllColumn(final UserAddReqDTO userAddReqDTO) {
        if (Func.isEmpty(userAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        UserDTO userDto = new UserDTO();
        BeanKit.copyProperties(userAddReqDTO, userDto, DEMO_CONVERTER);
        return super.saveAllColumn(userDto);
    }

    public Boolean addBatchAllColumn(final List<UserAddReqDTO> userAddReqDTOList) {
        if (Func.isEmpty(userAddReqDTOList)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        List<UserDTO> userDTOList = Lists.newArrayList();
        for (UserAddReqDTO userAddReqDTO : userAddReqDTOList) {
            UserDTO userDto = new UserDTO();
            BeanKit.copyProperties(userAddReqDTO, userDto, DEMO_CONVERTER);
            userDTOList.add(userDto);
        }
        return super.saveBatchAllColumn(userDTOList);
    }

    public UserShowResDTO show(final Long id) {
        UserDTO userDto = super.findById(id);

        if (!Func.isEmpty(userDto)) {
            UserShowResDTO userShowResDTO = new UserShowResDTO();
            BeanKit.copyProperties(userDto, userShowResDTO, DEMO_CONVERTER);
            return userShowResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<UserShowResDTO> showByIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(ModelResult.CODE_200, "集合不能为空且大小大于0");
        }

        List<UserDTO> userDtoList = super.findBatchIds(ids);

        if (!Func.isEmpty(userDtoList)) {
            List<UserShowResDTO> userShowResDTOList = Lists.newArrayList();
            for (UserDTO userDto : userDtoList) {
                UserShowResDTO userShowResDTO = new UserShowResDTO();
                BeanKit.copyProperties(userDto, userShowResDTO, DEMO_CONVERTER);
                userShowResDTOList.add(userShowResDTO);
            }
            return userShowResDTOList;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean modify(final UserModifyReqDTO userModifyReqDTO) {
        if (Func.isEmpty(userModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        UserDTO userDto = new UserDTO();
        BeanKit.copyProperties(userModifyReqDTO, userDto, DEMO_CONVERTER);
        return super.modifyById(userDto);
    }

    public Boolean modifyAllColumn(final UserModifyReqDTO userModifyReqDTO) {
        if (Func.isEmpty(userModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        UserDTO userDto = new UserDTO();
        BeanKit.copyProperties(userModifyReqDTO, userDto, DEMO_CONVERTER);
        return super.modifyAllColumnById(userDto);
    }

    public Boolean removeByParams(final UserRemoveReqDTO userRemoveReqDTO) {
        if (Func.isEmpty(userRemoveReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        UserDTO userParamsDTO = new UserDTO();
        BeanKit.copyProperties(userRemoveReqDTO, userParamsDTO, DEMO_CONVERTER);
        return super.remove(userParamsDTO);
    }


    @Override
    protected List<UserDTO> entityToDTOList(final List<UserEntity> userEntityList) {
        List<UserDTO> userDtoList = null;
        if (!Func.isEmpty(userEntityList)) {
            userDtoList = Lists.newArrayList();
            for (UserEntity userEntity : userEntityList) {
                userDtoList.add(entityToDTO(userEntity));
            }
        }
        return userDtoList;
    }

    @Override
    protected UserDTO entityToDTO(final UserEntity userEntity) {
        UserDTO userDto = null;
        if (!Func.isEmpty(userEntity)) {
            userDto = new UserDTO();
            BeanKit.copyProperties(userEntity, userDto);
        }
        return userDto;
    }

    @Override
    protected List<UserEntity> dtoToEntityList(final List<UserDTO> userDtoList) {
        List<UserEntity> userEntityList = null;
        if (!Func.isEmpty(userDtoList)) {
            userEntityList = Lists.newArrayList();
            for (UserDTO userDto : userDtoList) {
                userEntityList.add(dtoToEntity(userDto));
            }
        }
        return userEntityList;
    }

    @Override
    protected UserEntity dtoToEntity(final UserDTO userDto) {
        UserEntity userEntity = null;
        if (!Func.isEmpty(userDto)) {
            userEntity = new UserEntity();
            BeanKit.copyProperties(userDto, userEntity);
        }
        return userEntity;
    }

    @Override
    protected UserEntity mapToEntity(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new UserEntity();
        }
        return (UserEntity) MapKit.toBean(map, UserEntity.class);
    }

    @Override
    protected UserDTO mapToDto(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new UserDTO();
        }
        return (UserDTO) MapKit.toBean(map, UserDTO.class);
    }
}
