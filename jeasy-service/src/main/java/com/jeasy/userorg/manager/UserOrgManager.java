package com.jeasy.userorg.manager;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import com.jeasy.base.manager.impl.BaseManagerImpl;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.object.Converter;
import com.jeasy.common.object.MapKit;
import com.jeasy.common.spring.SpringContextHolder;
import com.jeasy.exception.MessageException;
import com.jeasy.userorg.dao.UserOrgDAO;
import com.jeasy.userorg.dto.*;
import com.jeasy.userorg.entity.UserOrgEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 用户机构 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Component
public class UserOrgManager extends BaseManagerImpl<UserOrgDAO, UserOrgEntity, UserOrgDTO> {

    /**
     * this is a converter demo only for BeanKit.copyProperties
     *
     * @see BeanKit#copyProperties(Object source, Object target, Converter... converters)
     */
    private static final Converter<String, String> DEMO_CONVERTER = new Converter<String, String>("filed1", "filed2") {
        @Override
        public String convert(final String val) {
            return val;
        }
    };

    public static UserOrgManager me() {
        return SpringContextHolder.getBean(UserOrgManager.class);
    }

    public List<UserOrgListResDTO> list(final UserOrgListReqDTO userorgListReqDTO) {
        UserOrgDTO userorgParamsDTO = new UserOrgDTO();
        if (!Func.isEmpty(userorgListReqDTO)) {
            BeanKit.copyProperties(userorgListReqDTO, userorgParamsDTO, DEMO_CONVERTER);
        }

        List<UserOrgDTO> userorgDtoList = super.findList(userorgParamsDTO);

        if (!Func.isEmpty(userorgDtoList)) {
            List<UserOrgListResDTO> items = Lists.newArrayList();
            for (UserOrgDTO userorgDto : userorgDtoList) {
                UserOrgListResDTO userorgListResDTO = new UserOrgListResDTO();
                BeanKit.copyProperties(userorgDto, userorgListResDTO, DEMO_CONVERTER);
                items.add(userorgListResDTO);
            }
            return items;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<UserOrgListResDTO> list1_1_0(final UserOrgListReqDTO userorgListReqDTO) {
        return list(userorgListReqDTO);
    }

    public List<UserOrgListResDTO> list1_2_0(final UserOrgListReqDTO userorgListReqDTO) {
        return list(userorgListReqDTO);
    }

    public List<UserOrgListResDTO> list1_3_0(final UserOrgListReqDTO userorgListReqDTO) {
        return list(userorgListReqDTO);
    }

    public UserOrgListResDTO listOne(final UserOrgListReqDTO userorgListReqDTO) {
        UserOrgDTO userorgParamsDTO = new UserOrgDTO();
        if (!Func.isEmpty(userorgListReqDTO)) {
            BeanKit.copyProperties(userorgListReqDTO, userorgParamsDTO, DEMO_CONVERTER);
        }

        UserOrgDTO userorgDto = super.findOne(userorgParamsDTO);
        if (!Func.isEmpty(userorgDto)) {
            UserOrgListResDTO userorgListResDTO = new UserOrgListResDTO();
            BeanKit.copyProperties(userorgDto, userorgListResDTO, DEMO_CONVERTER);
            return userorgListResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Page<UserOrgPageResDTO> pagination(final UserOrgPageReqDTO userorgPageReqDTO, final Integer currentPage, final Integer pageSize) {
        UserOrgDTO userorgParamsDTO = new UserOrgDTO();
        if (!Func.isEmpty(userorgPageReqDTO)) {
            BeanKit.copyProperties(userorgPageReqDTO, userorgParamsDTO, DEMO_CONVERTER);
        }

        Page<UserOrgDTO> userorgDTOPage = super.findPage(userorgParamsDTO, currentPage, pageSize);

        if (Func.isNotEmpty(userorgDTOPage) && Func.isNotEmpty(userorgDTOPage.getRecords())) {
            List<UserOrgPageResDTO> userorgPageResDTOs = Lists.newArrayList();
            for (UserOrgDTO userorgDto : userorgDTOPage.getRecords()) {
                UserOrgPageResDTO userorgPageResDTO = new UserOrgPageResDTO();
                BeanKit.copyProperties(userorgDto, userorgPageResDTO, DEMO_CONVERTER);
                userorgPageResDTOs.add(userorgPageResDTO);
            }

            Page<UserOrgPageResDTO> userorgPageResDTOPage = new Page<>();
            userorgPageResDTOPage.setRecords(userorgPageResDTOs);
            userorgPageResDTOPage.setTotal(userorgDTOPage.getTotal());
            return userorgPageResDTOPage;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean add(final UserOrgAddReqDTO userorgAddReqDTO) {
        if (Func.isEmpty(userorgAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        UserOrgDTO userorgDto = new UserOrgDTO();
        BeanKit.copyProperties(userorgAddReqDTO, userorgDto, DEMO_CONVERTER);
        return super.save(userorgDto);
    }

    public Boolean addAllColumn(final UserOrgAddReqDTO userorgAddReqDTO) {
        if (Func.isEmpty(userorgAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        UserOrgDTO userorgDto = new UserOrgDTO();
        BeanKit.copyProperties(userorgAddReqDTO, userorgDto, DEMO_CONVERTER);
        return super.saveAllColumn(userorgDto);
    }

    public Boolean addBatchAllColumn(final List<UserOrgAddReqDTO> userorgAddReqDTOList) {
        if (Func.isEmpty(userorgAddReqDTOList)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        List<UserOrgDTO> userorgDTOList = Lists.newArrayList();
        for (UserOrgAddReqDTO userorgAddReqDTO : userorgAddReqDTOList) {
            UserOrgDTO userorgDto = new UserOrgDTO();
            BeanKit.copyProperties(userorgAddReqDTO, userorgDto, DEMO_CONVERTER);
            userorgDTOList.add(userorgDto);
        }
        return super.saveBatchAllColumn(userorgDTOList);
    }

    public UserOrgShowResDTO show(final Long id) {
        UserOrgDTO userorgDto = super.findById(id);

        if (!Func.isEmpty(userorgDto)) {
            UserOrgShowResDTO userorgShowResDTO = new UserOrgShowResDTO();
            BeanKit.copyProperties(userorgDto, userorgShowResDTO, DEMO_CONVERTER);
            return userorgShowResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<UserOrgShowResDTO> showByIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(ModelResult.CODE_200, "集合不能为空且大小大于0");
        }

        List<UserOrgDTO> userorgDtoList = super.findBatchIds(ids);

        if (!Func.isEmpty(userorgDtoList)) {
            List<UserOrgShowResDTO> userorgShowResDTOList = Lists.newArrayList();
            for (UserOrgDTO userorgDto : userorgDtoList) {
                UserOrgShowResDTO userorgShowResDTO = new UserOrgShowResDTO();
                BeanKit.copyProperties(userorgDto, userorgShowResDTO, DEMO_CONVERTER);
                userorgShowResDTOList.add(userorgShowResDTO);
            }
            return userorgShowResDTOList;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean modify(final UserOrgModifyReqDTO userorgModifyReqDTO) {
        if (Func.isEmpty(userorgModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        UserOrgDTO userorgDto = new UserOrgDTO();
        BeanKit.copyProperties(userorgModifyReqDTO, userorgDto, DEMO_CONVERTER);
        return super.modifyById(userorgDto);
    }

    public Boolean modifyAllColumn(final UserOrgModifyReqDTO userorgModifyReqDTO) {
        if (Func.isEmpty(userorgModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        UserOrgDTO userorgDto = new UserOrgDTO();
        BeanKit.copyProperties(userorgModifyReqDTO, userorgDto, DEMO_CONVERTER);
        return super.modifyAllColumnById(userorgDto);
    }

    public Boolean removeByParams(final UserOrgRemoveReqDTO userorgRemoveReqDTO) {
        if (Func.isEmpty(userorgRemoveReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        UserOrgDTO userorgParamsDTO = new UserOrgDTO();
        BeanKit.copyProperties(userorgRemoveReqDTO, userorgParamsDTO, DEMO_CONVERTER);
        return super.remove(userorgParamsDTO);
    }


    @Override
    protected List<UserOrgDTO> entityToDTOList(final List<UserOrgEntity> userorgEntityList) {
        List<UserOrgDTO> userorgDtoList = null;
        if (!Func.isEmpty(userorgEntityList)) {
            userorgDtoList = Lists.newArrayList();
            for (UserOrgEntity userorgEntity : userorgEntityList) {
                userorgDtoList.add(entityToDTO(userorgEntity));
            }
        }
        return userorgDtoList;
    }

    @Override
    protected UserOrgDTO entityToDTO(final UserOrgEntity userorgEntity) {
        UserOrgDTO userorgDto = null;
        if (!Func.isEmpty(userorgEntity)) {
            userorgDto = new UserOrgDTO();
            BeanKit.copyProperties(userorgEntity, userorgDto);
        }
        return userorgDto;
    }

    @Override
    protected List<UserOrgEntity> dtoToEntityList(final List<UserOrgDTO> userorgDtoList) {
        List<UserOrgEntity> userorgEntityList = null;
        if (!Func.isEmpty(userorgDtoList)) {
            userorgEntityList = Lists.newArrayList();
            for (UserOrgDTO userorgDto : userorgDtoList) {
                userorgEntityList.add(dtoToEntity(userorgDto));
            }
        }
        return userorgEntityList;
    }

    @Override
    protected UserOrgEntity dtoToEntity(final UserOrgDTO userorgDto) {
        UserOrgEntity userorgEntity = null;
        if (!Func.isEmpty(userorgDto)) {
            userorgEntity = new UserOrgEntity();
            BeanKit.copyProperties(userorgDto, userorgEntity);
        }
        return userorgEntity;
    }

    @Override
    protected UserOrgEntity mapToEntity(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new UserOrgEntity();
        }
        return (UserOrgEntity) MapKit.toBean(map, UserOrgEntity.class);
    }

    @Override
    protected UserOrgDTO mapToDto(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new UserOrgDTO();
        }
        return (UserOrgDTO) MapKit.toBean(map, UserOrgDTO.class);
    }
}
