package com.jeasy.roleresource.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.jeasy.base.service.BaseService;
import com.jeasy.roleresource.dto.*;

import java.util.List;

/**
 * 角色资源 Service
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
public interface RoleResourceService extends BaseService<RoleResourceDTO> {

    /**
     * 列表
     *
     * @param roleresourceListReqDTO 入参DTO
     * @return
     */
    List<RoleResourceListResDTO> list(RoleResourceListReqDTO roleresourceListReqDTO);

    List<RoleResourceListResDTO> list1_1_0(RoleResourceListReqDTO roleresourceListReqDTO);

    List<RoleResourceListResDTO> list1_2_0(RoleResourceListReqDTO roleresourceListReqDTO);

    List<RoleResourceListResDTO> list1_3_0(RoleResourceListReqDTO roleresourceListReqDTO);

    /**
     * First查询
     *
     * @param roleresourceListReqDTO 入参DTO
     * @return
     */
    RoleResourceListResDTO listOne(RoleResourceListReqDTO roleresourceListReqDTO);

    /**
     * 分页
     *
     * @param roleresourcePageReqDTO 入参DTO
     * @param currentPage 当前页
     * @param pageSize   每页大小
     * @return
     */
    Page<RoleResourcePageResDTO> pagination(RoleResourcePageReqDTO roleresourcePageReqDTO, Integer currentPage, Integer pageSize);

    /**
     * 新增
     *
     * @param roleresourceAddReqDTO 入参DTO
     * @return
     */
    Boolean add(RoleResourceAddReqDTO roleresourceAddReqDTO);

    /**
     * 新增(所有字段)
     *
     * @param roleresourceAddReqDTO 入参DTO
     * @return
     */
    Boolean addAllColumn(RoleResourceAddReqDTO roleresourceAddReqDTO);

    /**
     * 批量新增(所有字段)
     *
     * @param roleresourceAddReqDTOList 入参DTO
     * @return
     */
    Boolean addBatchAllColumn(List<RoleResourceAddReqDTO> roleresourceAddReqDTOList);

    /**
     * 详情
     *
     * @param id 主键ID
     * @return
     */
    RoleResourceShowResDTO show(Long id);

    /**
     * 批量详情
     *
     * @param ids 主键IDs
     * @return
     */
    List<RoleResourceShowResDTO> showByIds(List<Long> ids);

    /**
     * 修改
     *
     * @param roleresourceModifyReqDTO 入参DTO
     * @return
     */
    Boolean modify(RoleResourceModifyReqDTO roleresourceModifyReqDTO);

    /**
     * 修改(所有字段)
     *
     * @param roleresourceModifyReqDTO 入参DTO
     * @return
     */
    Boolean modifyAllColumn(RoleResourceModifyReqDTO roleresourceModifyReqDTO);

    /**
     * 参数删除
     *
     * @param roleresourceRemoveReqDTO 入参DTO
     * @return
     */
    Boolean removeByParams(RoleResourceRemoveReqDTO roleresourceRemoveReqDTO);

    List<RoleResourceDTO> findByRoleIds(List<Long> roleIdList);
}
