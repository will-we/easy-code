package com.jeasy.roleresource.manager;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import com.jeasy.base.manager.impl.BaseManagerImpl;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.object.Converter;
import com.jeasy.common.object.MapKit;
import com.jeasy.common.spring.SpringContextHolder;
import com.jeasy.exception.MessageException;
import com.jeasy.roleresource.dao.RoleResourceDAO;
import com.jeasy.roleresource.dto.*;
import com.jeasy.roleresource.entity.RoleResourceEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 角色资源 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Component
public class RoleResourceManager extends BaseManagerImpl<RoleResourceDAO, RoleResourceEntity, RoleResourceDTO> {

    /**
     * this is a converter demo only for BeanKit.copyProperties
     *
     * @see BeanKit#copyProperties(Object source, Object target, Converter... converters)
     */
    private static final Converter<String, String> DEMO_CONVERTER = new Converter<String, String>("filed1", "filed2") {
        @Override
        public String convert(final String val) {
            return val;
        }
    };

    public static RoleResourceManager me() {
        return SpringContextHolder.getBean(RoleResourceManager.class);
    }

    public List<RoleResourceListResDTO> list(final RoleResourceListReqDTO roleresourceListReqDTO) {
        RoleResourceDTO roleresourceParamsDTO = new RoleResourceDTO();
        if (!Func.isEmpty(roleresourceListReqDTO)) {
            BeanKit.copyProperties(roleresourceListReqDTO, roleresourceParamsDTO, DEMO_CONVERTER);
        }

        List<RoleResourceDTO> roleresourceDtoList = super.findList(roleresourceParamsDTO);

        if (!Func.isEmpty(roleresourceDtoList)) {
            List<RoleResourceListResDTO> items = Lists.newArrayList();
            for (RoleResourceDTO roleresourceDto : roleresourceDtoList) {
                RoleResourceListResDTO roleresourceListResDTO = new RoleResourceListResDTO();
                BeanKit.copyProperties(roleresourceDto, roleresourceListResDTO, DEMO_CONVERTER);
                items.add(roleresourceListResDTO);
            }
            return items;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<RoleResourceListResDTO> list1_1_0(final RoleResourceListReqDTO roleresourceListReqDTO) {
        return list(roleresourceListReqDTO);
    }

    public List<RoleResourceListResDTO> list1_2_0(final RoleResourceListReqDTO roleresourceListReqDTO) {
        return list(roleresourceListReqDTO);
    }

    public List<RoleResourceListResDTO> list1_3_0(final RoleResourceListReqDTO roleresourceListReqDTO) {
        return list(roleresourceListReqDTO);
    }

    public RoleResourceListResDTO listOne(final RoleResourceListReqDTO roleresourceListReqDTO) {
        RoleResourceDTO roleresourceParamsDTO = new RoleResourceDTO();
        if (!Func.isEmpty(roleresourceListReqDTO)) {
            BeanKit.copyProperties(roleresourceListReqDTO, roleresourceParamsDTO, DEMO_CONVERTER);
        }

        RoleResourceDTO roleresourceDto = super.findOne(roleresourceParamsDTO);
        if (!Func.isEmpty(roleresourceDto)) {
            RoleResourceListResDTO roleresourceListResDTO = new RoleResourceListResDTO();
            BeanKit.copyProperties(roleresourceDto, roleresourceListResDTO, DEMO_CONVERTER);
            return roleresourceListResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Page<RoleResourcePageResDTO> pagination(final RoleResourcePageReqDTO roleresourcePageReqDTO, final Integer currentPage, final Integer pageSize) {
        RoleResourceDTO roleresourceParamsDTO = new RoleResourceDTO();
        if (!Func.isEmpty(roleresourcePageReqDTO)) {
            BeanKit.copyProperties(roleresourcePageReqDTO, roleresourceParamsDTO, DEMO_CONVERTER);
        }

        Page<RoleResourceDTO> roleresourceDTOPage = super.findPage(roleresourceParamsDTO, currentPage, pageSize);

        if (Func.isNotEmpty(roleresourceDTOPage) && Func.isNotEmpty(roleresourceDTOPage.getRecords())) {
            List<RoleResourcePageResDTO> roleresourcePageResDTOs = Lists.newArrayList();
            for (RoleResourceDTO roleresourceDto : roleresourceDTOPage.getRecords()) {
                RoleResourcePageResDTO roleresourcePageResDTO = new RoleResourcePageResDTO();
                BeanKit.copyProperties(roleresourceDto, roleresourcePageResDTO, DEMO_CONVERTER);
                roleresourcePageResDTOs.add(roleresourcePageResDTO);
            }

            Page<RoleResourcePageResDTO> roleresourcePageResDTOPage = new Page<>();
            roleresourcePageResDTOPage.setRecords(roleresourcePageResDTOs);
            roleresourcePageResDTOPage.setTotal(roleresourceDTOPage.getTotal());
            return roleresourcePageResDTOPage;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean add(final RoleResourceAddReqDTO roleresourceAddReqDTO) {
        if (Func.isEmpty(roleresourceAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        RoleResourceDTO roleresourceDto = new RoleResourceDTO();
        BeanKit.copyProperties(roleresourceAddReqDTO, roleresourceDto, DEMO_CONVERTER);
        return super.save(roleresourceDto);
    }

    public Boolean addAllColumn(final RoleResourceAddReqDTO roleresourceAddReqDTO) {
        if (Func.isEmpty(roleresourceAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        RoleResourceDTO roleresourceDto = new RoleResourceDTO();
        BeanKit.copyProperties(roleresourceAddReqDTO, roleresourceDto, DEMO_CONVERTER);
        return super.saveAllColumn(roleresourceDto);
    }

    public Boolean addBatchAllColumn(final List<RoleResourceAddReqDTO> roleresourceAddReqDTOList) {
        if (Func.isEmpty(roleresourceAddReqDTOList)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        List<RoleResourceDTO> roleresourceDTOList = Lists.newArrayList();
        for (RoleResourceAddReqDTO roleresourceAddReqDTO : roleresourceAddReqDTOList) {
            RoleResourceDTO roleresourceDto = new RoleResourceDTO();
            BeanKit.copyProperties(roleresourceAddReqDTO, roleresourceDto, DEMO_CONVERTER);
            roleresourceDTOList.add(roleresourceDto);
        }
        return super.saveBatchAllColumn(roleresourceDTOList);
    }

    public RoleResourceShowResDTO show(final Long id) {
        RoleResourceDTO roleresourceDto = super.findById(id);

        if (!Func.isEmpty(roleresourceDto)) {
            RoleResourceShowResDTO roleresourceShowResDTO = new RoleResourceShowResDTO();
            BeanKit.copyProperties(roleresourceDto, roleresourceShowResDTO, DEMO_CONVERTER);
            return roleresourceShowResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<RoleResourceShowResDTO> showByIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(ModelResult.CODE_200, "集合不能为空且大小大于0");
        }

        List<RoleResourceDTO> roleresourceDtoList = super.findBatchIds(ids);

        if (!Func.isEmpty(roleresourceDtoList)) {
            List<RoleResourceShowResDTO> roleresourceShowResDTOList = Lists.newArrayList();
            for (RoleResourceDTO roleresourceDto : roleresourceDtoList) {
                RoleResourceShowResDTO roleresourceShowResDTO = new RoleResourceShowResDTO();
                BeanKit.copyProperties(roleresourceDto, roleresourceShowResDTO, DEMO_CONVERTER);
                roleresourceShowResDTOList.add(roleresourceShowResDTO);
            }
            return roleresourceShowResDTOList;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean modify(final RoleResourceModifyReqDTO roleresourceModifyReqDTO) {
        if (Func.isEmpty(roleresourceModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        RoleResourceDTO roleresourceDto = new RoleResourceDTO();
        BeanKit.copyProperties(roleresourceModifyReqDTO, roleresourceDto, DEMO_CONVERTER);
        return super.modifyById(roleresourceDto);
    }

    public Boolean modifyAllColumn(final RoleResourceModifyReqDTO roleresourceModifyReqDTO) {
        if (Func.isEmpty(roleresourceModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        RoleResourceDTO roleresourceDto = new RoleResourceDTO();
        BeanKit.copyProperties(roleresourceModifyReqDTO, roleresourceDto, DEMO_CONVERTER);
        return super.modifyAllColumnById(roleresourceDto);
    }

    public Boolean removeByParams(final RoleResourceRemoveReqDTO roleresourceRemoveReqDTO) {
        if (Func.isEmpty(roleresourceRemoveReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        RoleResourceDTO roleresourceParamsDTO = new RoleResourceDTO();
        BeanKit.copyProperties(roleresourceRemoveReqDTO, roleresourceParamsDTO, DEMO_CONVERTER);
        return super.remove(roleresourceParamsDTO);
    }

    public List<RoleResourceDTO> findByRoleIds(final List<Long> roleIdList) {
        if (Func.isEmpty(roleIdList)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        return super.findList(new EntityWrapper<RoleResourceEntity>().isWhere(Boolean.FALSE).in(RoleResourceEntity.DB_COL_ROLE_ID, roleIdList));
    }

    @Override
    protected List<RoleResourceDTO> entityToDTOList(final List<RoleResourceEntity> roleresourceEntityList) {
        List<RoleResourceDTO> roleresourceDtoList = null;
        if (!Func.isEmpty(roleresourceEntityList)) {
            roleresourceDtoList = Lists.newArrayList();
            for (RoleResourceEntity roleresourceEntity : roleresourceEntityList) {
                roleresourceDtoList.add(entityToDTO(roleresourceEntity));
            }
        }
        return roleresourceDtoList;
    }

    @Override
    protected RoleResourceDTO entityToDTO(final RoleResourceEntity roleresourceEntity) {
        RoleResourceDTO roleresourceDto = null;
        if (!Func.isEmpty(roleresourceEntity)) {
            roleresourceDto = new RoleResourceDTO();
            BeanKit.copyProperties(roleresourceEntity, roleresourceDto);
        }
        return roleresourceDto;
    }

    @Override
    protected List<RoleResourceEntity> dtoToEntityList(final List<RoleResourceDTO> roleresourceDtoList) {
        List<RoleResourceEntity> roleresourceEntityList = null;
        if (!Func.isEmpty(roleresourceDtoList)) {
            roleresourceEntityList = Lists.newArrayList();
            for (RoleResourceDTO roleresourceDto : roleresourceDtoList) {
                roleresourceEntityList.add(dtoToEntity(roleresourceDto));
            }
        }
        return roleresourceEntityList;
    }

    @Override
    protected RoleResourceEntity dtoToEntity(final RoleResourceDTO roleresourceDto) {
        RoleResourceEntity roleresourceEntity = null;
        if (!Func.isEmpty(roleresourceDto)) {
            roleresourceEntity = new RoleResourceEntity();
            BeanKit.copyProperties(roleresourceDto, roleresourceEntity);
        }
        return roleresourceEntity;
    }

    @Override
    protected RoleResourceEntity mapToEntity(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new RoleResourceEntity();
        }
        return (RoleResourceEntity) MapKit.toBean(map, RoleResourceEntity.class);
    }

    @Override
    protected RoleResourceDTO mapToDto(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new RoleResourceDTO();
        }
        return (RoleResourceDTO) MapKit.toBean(map, RoleResourceDTO.class);
    }
}
