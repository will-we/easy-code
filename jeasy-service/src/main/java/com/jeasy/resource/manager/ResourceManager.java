package com.jeasy.resource.manager;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import com.jeasy.base.manager.impl.BaseManagerImpl;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.object.Converter;
import com.jeasy.common.object.MapKit;
import com.jeasy.common.spring.SpringContextHolder;
import com.jeasy.exception.MessageException;
import com.jeasy.resource.dao.ResourceDAO;
import com.jeasy.resource.dto.*;
import com.jeasy.resource.entity.ResourceEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 菜单 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Component
public class ResourceManager extends BaseManagerImpl<ResourceDAO, ResourceEntity, ResourceDTO> {

    /**
     * this is a converter demo only for BeanKit.copyProperties
     *
     * @see BeanKit#copyProperties(Object source, Object target, Converter... converters)
     */
    private static final Converter<String, String> DEMO_CONVERTER = new Converter<String, String>("filed1", "filed2") {
        @Override
        public String convert(final String val) {
            return val;
        }
    };

    public static ResourceManager me() {
        return SpringContextHolder.getBean(ResourceManager.class);
    }

    public List<ResourceListResDTO> list(final ResourceListReqDTO resourceListReqDTO) {
        ResourceDTO resourceParamsDTO = new ResourceDTO();
        if (!Func.isEmpty(resourceListReqDTO)) {
            BeanKit.copyProperties(resourceListReqDTO, resourceParamsDTO, DEMO_CONVERTER);
        }

        List<ResourceDTO> resourceDtoList = super.findList(resourceParamsDTO);

        if (!Func.isEmpty(resourceDtoList)) {
            List<ResourceListResDTO> items = Lists.newArrayList();
            for (ResourceDTO resourceDto : resourceDtoList) {
                ResourceListResDTO resourceListResDTO = new ResourceListResDTO();
                BeanKit.copyProperties(resourceDto, resourceListResDTO, DEMO_CONVERTER);
                items.add(resourceListResDTO);
            }
            return items;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<ResourceListResDTO> list1_1_0(final ResourceListReqDTO resourceListReqDTO) {
        return list(resourceListReqDTO);
    }

    public List<ResourceListResDTO> list1_2_0(final ResourceListReqDTO resourceListReqDTO) {
        return list(resourceListReqDTO);
    }

    public List<ResourceListResDTO> list1_3_0(final ResourceListReqDTO resourceListReqDTO) {
        return list(resourceListReqDTO);
    }

    public ResourceListResDTO listOne(final ResourceListReqDTO resourceListReqDTO) {
        ResourceDTO resourceParamsDTO = new ResourceDTO();
        if (!Func.isEmpty(resourceListReqDTO)) {
            BeanKit.copyProperties(resourceListReqDTO, resourceParamsDTO, DEMO_CONVERTER);
        }

        ResourceDTO resourceDto = super.findOne(resourceParamsDTO);
        if (!Func.isEmpty(resourceDto)) {
            ResourceListResDTO resourceListResDTO = new ResourceListResDTO();
            BeanKit.copyProperties(resourceDto, resourceListResDTO, DEMO_CONVERTER);
            return resourceListResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Page<ResourcePageResDTO> pagination(final ResourcePageReqDTO resourcePageReqDTO, final Integer currentPage, final Integer pageSize) {
        ResourceDTO resourceParamsDTO = new ResourceDTO();
        if (!Func.isEmpty(resourcePageReqDTO)) {
            BeanKit.copyProperties(resourcePageReqDTO, resourceParamsDTO, DEMO_CONVERTER);
        }

        Page<ResourceDTO> resourceDTOPage = super.findPage(resourceParamsDTO, currentPage, pageSize);

        if (Func.isNotEmpty(resourceDTOPage) && Func.isNotEmpty(resourceDTOPage.getRecords())) {
            List<ResourcePageResDTO> resourcePageResDTOs = Lists.newArrayList();
            for (ResourceDTO resourceDto : resourceDTOPage.getRecords()) {
                ResourcePageResDTO resourcePageResDTO = new ResourcePageResDTO();
                BeanKit.copyProperties(resourceDto, resourcePageResDTO, DEMO_CONVERTER);
                resourcePageResDTOs.add(resourcePageResDTO);
            }

            Page<ResourcePageResDTO> resourcePageResDTOPage = new Page<>();
            resourcePageResDTOPage.setRecords(resourcePageResDTOs);
            resourcePageResDTOPage.setTotal(resourceDTOPage.getTotal());
            return resourcePageResDTOPage;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean add(final ResourceAddReqDTO resourceAddReqDTO) {
        if (Func.isEmpty(resourceAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        ResourceDTO resourceDto = new ResourceDTO();
        BeanKit.copyProperties(resourceAddReqDTO, resourceDto, DEMO_CONVERTER);
        return super.save(resourceDto);
    }

    public Boolean addAllColumn(final ResourceAddReqDTO resourceAddReqDTO) {
        if (Func.isEmpty(resourceAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        ResourceDTO resourceDto = new ResourceDTO();
        BeanKit.copyProperties(resourceAddReqDTO, resourceDto, DEMO_CONVERTER);
        return super.saveAllColumn(resourceDto);
    }

    public Boolean addBatchAllColumn(final List<ResourceAddReqDTO> resourceAddReqDTOList) {
        if (Func.isEmpty(resourceAddReqDTOList)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        List<ResourceDTO> resourceDTOList = Lists.newArrayList();
        for (ResourceAddReqDTO resourceAddReqDTO : resourceAddReqDTOList) {
            ResourceDTO resourceDto = new ResourceDTO();
            BeanKit.copyProperties(resourceAddReqDTO, resourceDto, DEMO_CONVERTER);
            resourceDTOList.add(resourceDto);
        }
        return super.saveBatchAllColumn(resourceDTOList);
    }

    public ResourceShowResDTO show(final Long id) {
        ResourceDTO resourceDto = super.findById(id);

        if (!Func.isEmpty(resourceDto)) {
            ResourceShowResDTO resourceShowResDTO = new ResourceShowResDTO();
            BeanKit.copyProperties(resourceDto, resourceShowResDTO, DEMO_CONVERTER);
            return resourceShowResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<ResourceShowResDTO> showByIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(ModelResult.CODE_200, "集合不能为空且大小大于0");
        }

        List<ResourceDTO> resourceDtoList = super.findBatchIds(ids);

        if (!Func.isEmpty(resourceDtoList)) {
            List<ResourceShowResDTO> resourceShowResDTOList = Lists.newArrayList();
            for (ResourceDTO resourceDto : resourceDtoList) {
                ResourceShowResDTO resourceShowResDTO = new ResourceShowResDTO();
                BeanKit.copyProperties(resourceDto, resourceShowResDTO, DEMO_CONVERTER);
                resourceShowResDTOList.add(resourceShowResDTO);
            }
            return resourceShowResDTOList;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean modify(final ResourceModifyReqDTO resourceModifyReqDTO) {
        if (Func.isEmpty(resourceModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        ResourceDTO resourceDto = new ResourceDTO();
        BeanKit.copyProperties(resourceModifyReqDTO, resourceDto, DEMO_CONVERTER);
        return super.modifyById(resourceDto);
    }

    public Boolean modifyAllColumn(final ResourceModifyReqDTO resourceModifyReqDTO) {
        if (Func.isEmpty(resourceModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        ResourceDTO resourceDto = new ResourceDTO();
        BeanKit.copyProperties(resourceModifyReqDTO, resourceDto, DEMO_CONVERTER);
        return super.modifyAllColumnById(resourceDto);
    }

    public Boolean removeByParams(final ResourceRemoveReqDTO resourceRemoveReqDTO) {
        if (Func.isEmpty(resourceRemoveReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        ResourceDTO resourceParamsDTO = new ResourceDTO();
        BeanKit.copyProperties(resourceRemoveReqDTO, resourceParamsDTO, DEMO_CONVERTER);
        return super.remove(resourceParamsDTO);
    }


    @Override
    protected List<ResourceDTO> entityToDTOList(final List<ResourceEntity> resourceEntityList) {
        List<ResourceDTO> resourceDtoList = null;
        if (!Func.isEmpty(resourceEntityList)) {
            resourceDtoList = Lists.newArrayList();
            for (ResourceEntity resourceEntity : resourceEntityList) {
                resourceDtoList.add(entityToDTO(resourceEntity));
            }
        }
        return resourceDtoList;
    }

    @Override
    protected ResourceDTO entityToDTO(final ResourceEntity resourceEntity) {
        ResourceDTO resourceDto = null;
        if (!Func.isEmpty(resourceEntity)) {
            resourceDto = new ResourceDTO();
            BeanKit.copyProperties(resourceEntity, resourceDto);
        }
        return resourceDto;
    }

    @Override
    protected List<ResourceEntity> dtoToEntityList(final List<ResourceDTO> resourceDtoList) {
        List<ResourceEntity> resourceEntityList = null;
        if (!Func.isEmpty(resourceDtoList)) {
            resourceEntityList = Lists.newArrayList();
            for (ResourceDTO resourceDto : resourceDtoList) {
                resourceEntityList.add(dtoToEntity(resourceDto));
            }
        }
        return resourceEntityList;
    }

    @Override
    protected ResourceEntity dtoToEntity(final ResourceDTO resourceDto) {
        ResourceEntity resourceEntity = null;
        if (!Func.isEmpty(resourceDto)) {
            resourceEntity = new ResourceEntity();
            BeanKit.copyProperties(resourceDto, resourceEntity);
        }
        return resourceEntity;
    }

    @Override
    protected ResourceEntity mapToEntity(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new ResourceEntity();
        }
        return (ResourceEntity) MapKit.toBean(map, ResourceEntity.class);
    }

    @Override
    protected ResourceDTO mapToDto(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new ResourceDTO();
        }
        return (ResourceDTO) MapKit.toBean(map, ResourceDTO.class);
    }
}
