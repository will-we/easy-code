package com.jeasy.resource.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.jeasy.base.service.BaseService;
import com.jeasy.resource.dto.*;

import java.util.List;

/**
 * 菜单 Service
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
public interface ResourceService extends BaseService<ResourceDTO> {

    /**
     * 列表
     *
     * @param resourceListReqDTO 入参DTO
     * @return
     */
    List<ResourceListResDTO> list(ResourceListReqDTO resourceListReqDTO);

    List<ResourceListResDTO> list1_1_0(ResourceListReqDTO resourceListReqDTO);

    List<ResourceListResDTO> list1_2_0(ResourceListReqDTO resourceListReqDTO);

    List<ResourceListResDTO> list1_3_0(ResourceListReqDTO resourceListReqDTO);

    /**
     * First查询
     *
     * @param resourceListReqDTO 入参DTO
     * @return
     */
    ResourceListResDTO listOne(ResourceListReqDTO resourceListReqDTO);

    /**
     * 分页
     *
     * @param resourcePageReqDTO 入参DTO
     * @param currentPage 当前页
     * @param pageSize   每页大小
     * @return
     */
    Page<ResourcePageResDTO> pagination(ResourcePageReqDTO resourcePageReqDTO, Integer currentPage, Integer pageSize);

    /**
     * 新增
     *
     * @param resourceAddReqDTO 入参DTO
     * @return
     */
    Boolean add(ResourceAddReqDTO resourceAddReqDTO);

    /**
     * 新增(所有字段)
     *
     * @param resourceAddReqDTO 入参DTO
     * @return
     */
    Boolean addAllColumn(ResourceAddReqDTO resourceAddReqDTO);

    /**
     * 批量新增(所有字段)
     *
     * @param resourceAddReqDTOList 入参DTO
     * @return
     */
    Boolean addBatchAllColumn(List<ResourceAddReqDTO> resourceAddReqDTOList);

    /**
     * 详情
     *
     * @param id 主键ID
     * @return
     */
    ResourceShowResDTO show(Long id);

    /**
     * 批量详情
     *
     * @param ids 主键IDs
     * @return
     */
    List<ResourceShowResDTO> showByIds(List<Long> ids);

    /**
     * 修改
     *
     * @param resourceModifyReqDTO 入参DTO
     * @return
     */
    Boolean modify(ResourceModifyReqDTO resourceModifyReqDTO);

    /**
     * 修改(所有字段)
     *
     * @param resourceModifyReqDTO 入参DTO
     * @return
     */
    Boolean modifyAllColumn(ResourceModifyReqDTO resourceModifyReqDTO);

    /**
     * 参数删除
     *
     * @param resourceRemoveReqDTO 入参DTO
     * @return
     */
    Boolean removeByParams(ResourceRemoveReqDTO resourceRemoveReqDTO);
}
