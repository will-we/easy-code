package com.jeasy.dictionary.biz;

import com.jeasy.common.spring.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 字典 Biz
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Component
public class DictionaryBiz {

    public static DictionaryBiz me() {
        return SpringContextHolder.getBean(DictionaryBiz.class);
    }
}
