package com.jeasy.dictionary.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.jeasy.base.service.BaseService;
import com.jeasy.dictionary.dto.*;

import java.util.List;

/**
 * 字典 Service
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
public interface DictionaryService extends BaseService<DictionaryDTO> {

    /**
     * 列表
     *
     * @param dictionaryListReqDTO 入参DTO
     * @return
     */
    List<DictionaryListResDTO> list(DictionaryListReqDTO dictionaryListReqDTO);

    List<DictionaryListResDTO> list1_1_0(DictionaryListReqDTO dictionaryListReqDTO);

    List<DictionaryListResDTO> list1_2_0(DictionaryListReqDTO dictionaryListReqDTO);

    List<DictionaryListResDTO> list1_3_0(DictionaryListReqDTO dictionaryListReqDTO);

    /**
     * First查询
     *
     * @param dictionaryListReqDTO 入参DTO
     * @return
     */
    DictionaryListResDTO listOne(DictionaryListReqDTO dictionaryListReqDTO);

    /**
     * 分页
     *
     * @param dictionaryPageReqDTO 入参DTO
     * @param currentPage 当前页
     * @param pageSize   每页大小
     * @return
     */
    Page<DictionaryPageResDTO> pagination(DictionaryPageReqDTO dictionaryPageReqDTO, Integer currentPage, Integer pageSize);

    /**
     * 新增
     *
     * @param dictionaryAddReqDTO 入参DTO
     * @return
     */
    Boolean add(DictionaryAddReqDTO dictionaryAddReqDTO);

    /**
     * 新增(所有字段)
     *
     * @param dictionaryAddReqDTO 入参DTO
     * @return
     */
    Boolean addAllColumn(DictionaryAddReqDTO dictionaryAddReqDTO);

    /**
     * 批量新增(所有字段)
     *
     * @param dictionaryAddReqDTOList 入参DTO
     * @return
     */
    Boolean addBatchAllColumn(List<DictionaryAddReqDTO> dictionaryAddReqDTOList);

    /**
     * 详情
     *
     * @param id 主键ID
     * @return
     */
    DictionaryShowResDTO show(Long id);

    /**
     * 批量详情
     *
     * @param ids 主键IDs
     * @return
     */
    List<DictionaryShowResDTO> showByIds(List<Long> ids);

    /**
     * 修改
     *
     * @param dictionaryModifyReqDTO 入参DTO
     * @return
     */
    Boolean modify(DictionaryModifyReqDTO dictionaryModifyReqDTO);

    /**
     * 修改(所有字段)
     *
     * @param dictionaryModifyReqDTO 入参DTO
     * @return
     */
    Boolean modifyAllColumn(DictionaryModifyReqDTO dictionaryModifyReqDTO);

    /**
     * 参数删除
     *
     * @param dictionaryRemoveReqDTO 入参DTO
     * @return
     */
    Boolean removeByParams(DictionaryRemoveReqDTO dictionaryRemoveReqDTO);
}
