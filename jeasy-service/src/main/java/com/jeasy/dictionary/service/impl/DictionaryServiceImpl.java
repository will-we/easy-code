package com.jeasy.dictionary.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.jeasy.base.service.impl.BaseServiceImpl;
import com.jeasy.dictionary.dto.*;
import com.jeasy.dictionary.entity.DictionaryEntity;
import com.jeasy.dictionary.manager.DictionaryManager;
import com.jeasy.dictionary.service.DictionaryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典 ServiceImpl
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Service
public class DictionaryServiceImpl extends BaseServiceImpl<DictionaryManager, DictionaryEntity, DictionaryDTO> implements DictionaryService {

    @Override
    public List<DictionaryListResDTO> list(final DictionaryListReqDTO dictionaryListReqDTO) {
        return manager.list(dictionaryListReqDTO);
    }

    @Override
    public List<DictionaryListResDTO> list1_1_0(final DictionaryListReqDTO dictionaryListReqDTO) {
        return manager.list1_1_0(dictionaryListReqDTO);
    }

    @Override
    public List<DictionaryListResDTO> list1_2_0(final DictionaryListReqDTO dictionaryListReqDTO) {
        return manager.list1_2_0(dictionaryListReqDTO);
    }

    @Override
    public List<DictionaryListResDTO> list1_3_0(final DictionaryListReqDTO dictionaryListReqDTO) {
        return manager.list1_3_0(dictionaryListReqDTO);
    }

    @Override
    public DictionaryListResDTO listOne(final DictionaryListReqDTO dictionaryListReqDTO) {
        return manager.listOne(dictionaryListReqDTO);
    }

    @Override
    public Page<DictionaryPageResDTO> pagination(final DictionaryPageReqDTO dictionaryPageReqDTO, final Integer currentPage, final Integer pageSize) {
        return manager.pagination(dictionaryPageReqDTO, currentPage, pageSize);
    }

    @Override
    public Boolean add(final DictionaryAddReqDTO dictionaryAddReqDTO) {
        return manager.add(dictionaryAddReqDTO);
    }

    @Override
    public Boolean addAllColumn(final DictionaryAddReqDTO dictionaryAddReqDTO) {
        return manager.addAllColumn(dictionaryAddReqDTO);
    }

    @Override
    public Boolean addBatchAllColumn(final List<DictionaryAddReqDTO> dictionaryAddReqDTOList) {
        return manager.addBatchAllColumn(dictionaryAddReqDTOList);
    }

    @Override
    public DictionaryShowResDTO show(final Long id) {
        return manager.show(id);
    }

    @Override
    public List<DictionaryShowResDTO> showByIds(final List<Long> ids) {
        return manager.showByIds(ids);
    }

    @Override
    public Boolean modify(final DictionaryModifyReqDTO dictionaryModifyReqDTO) {
        return manager.modify(dictionaryModifyReqDTO);
    }

    @Override
    public Boolean modifyAllColumn(final DictionaryModifyReqDTO dictionaryModifyReqDTO) {
        return manager.modifyAllColumn(dictionaryModifyReqDTO);
    }

    @Override
    public Boolean removeByParams(final DictionaryRemoveReqDTO dictionaryRemoveReqDTO) {
        return manager.removeByParams(dictionaryRemoveReqDTO);
    }
}
