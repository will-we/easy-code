package com.jeasy.dictionary.manager;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import com.jeasy.base.manager.impl.BaseManagerImpl;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.object.Converter;
import com.jeasy.common.object.MapKit;
import com.jeasy.common.spring.SpringContextHolder;
import com.jeasy.exception.MessageException;
import com.jeasy.dictionary.dao.DictionaryDAO;
import com.jeasy.dictionary.dto.*;
import com.jeasy.dictionary.entity.DictionaryEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 字典 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
@Component
public class DictionaryManager extends BaseManagerImpl<DictionaryDAO, DictionaryEntity, DictionaryDTO> {

    /**
     * this is a converter demo only for BeanKit.copyProperties
     *
     * @see BeanKit#copyProperties(Object source, Object target, Converter... converters)
     */
    private static final Converter<String, String> DEMO_CONVERTER = new Converter<String, String>("filed1", "filed2") {
        @Override
        public String convert(final String val) {
            return val;
        }
    };

    public static DictionaryManager me() {
        return SpringContextHolder.getBean(DictionaryManager.class);
    }

    public List<DictionaryListResDTO> list(final DictionaryListReqDTO dictionaryListReqDTO) {
        DictionaryDTO dictionaryParamsDTO = new DictionaryDTO();
        if (!Func.isEmpty(dictionaryListReqDTO)) {
            BeanKit.copyProperties(dictionaryListReqDTO, dictionaryParamsDTO, DEMO_CONVERTER);
        }

        List<DictionaryDTO> dictionaryDtoList = super.findList(dictionaryParamsDTO);

        if (!Func.isEmpty(dictionaryDtoList)) {
            List<DictionaryListResDTO> items = Lists.newArrayList();
            for (DictionaryDTO dictionaryDto : dictionaryDtoList) {
                DictionaryListResDTO dictionaryListResDTO = new DictionaryListResDTO();
                BeanKit.copyProperties(dictionaryDto, dictionaryListResDTO, DEMO_CONVERTER);
                items.add(dictionaryListResDTO);
            }
            return items;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<DictionaryListResDTO> list1_1_0(final DictionaryListReqDTO dictionaryListReqDTO) {
        return list(dictionaryListReqDTO);
    }

    public List<DictionaryListResDTO> list1_2_0(final DictionaryListReqDTO dictionaryListReqDTO) {
        return list(dictionaryListReqDTO);
    }

    public List<DictionaryListResDTO> list1_3_0(final DictionaryListReqDTO dictionaryListReqDTO) {
        return list(dictionaryListReqDTO);
    }

    public DictionaryListResDTO listOne(final DictionaryListReqDTO dictionaryListReqDTO) {
        DictionaryDTO dictionaryParamsDTO = new DictionaryDTO();
        if (!Func.isEmpty(dictionaryListReqDTO)) {
            BeanKit.copyProperties(dictionaryListReqDTO, dictionaryParamsDTO, DEMO_CONVERTER);
        }

        DictionaryDTO dictionaryDto = super.findOne(dictionaryParamsDTO);
        if (!Func.isEmpty(dictionaryDto)) {
            DictionaryListResDTO dictionaryListResDTO = new DictionaryListResDTO();
            BeanKit.copyProperties(dictionaryDto, dictionaryListResDTO, DEMO_CONVERTER);
            return dictionaryListResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Page<DictionaryPageResDTO> pagination(final DictionaryPageReqDTO dictionaryPageReqDTO, final Integer currentPage, final Integer pageSize) {
        DictionaryDTO dictionaryParamsDTO = new DictionaryDTO();
        if (!Func.isEmpty(dictionaryPageReqDTO)) {
            BeanKit.copyProperties(dictionaryPageReqDTO, dictionaryParamsDTO, DEMO_CONVERTER);
        }

        Page<DictionaryDTO> dictionaryDTOPage = super.findPage(dictionaryParamsDTO, currentPage, pageSize);

        if (Func.isNotEmpty(dictionaryDTOPage) && Func.isNotEmpty(dictionaryDTOPage.getRecords())) {
            List<DictionaryPageResDTO> dictionaryPageResDTOs = Lists.newArrayList();
            for (DictionaryDTO dictionaryDto : dictionaryDTOPage.getRecords()) {
                DictionaryPageResDTO dictionaryPageResDTO = new DictionaryPageResDTO();
                BeanKit.copyProperties(dictionaryDto, dictionaryPageResDTO, DEMO_CONVERTER);
                dictionaryPageResDTOs.add(dictionaryPageResDTO);
            }

            Page<DictionaryPageResDTO> dictionaryPageResDTOPage = new Page<>();
            dictionaryPageResDTOPage.setRecords(dictionaryPageResDTOs);
            dictionaryPageResDTOPage.setTotal(dictionaryDTOPage.getTotal());
            return dictionaryPageResDTOPage;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean add(final DictionaryAddReqDTO dictionaryAddReqDTO) {
        if (Func.isEmpty(dictionaryAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        DictionaryDTO dictionaryDto = new DictionaryDTO();
        BeanKit.copyProperties(dictionaryAddReqDTO, dictionaryDto, DEMO_CONVERTER);
        return super.save(dictionaryDto);
    }

    public Boolean addAllColumn(final DictionaryAddReqDTO dictionaryAddReqDTO) {
        if (Func.isEmpty(dictionaryAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        DictionaryDTO dictionaryDto = new DictionaryDTO();
        BeanKit.copyProperties(dictionaryAddReqDTO, dictionaryDto, DEMO_CONVERTER);
        return super.saveAllColumn(dictionaryDto);
    }

    public Boolean addBatchAllColumn(final List<DictionaryAddReqDTO> dictionaryAddReqDTOList) {
        if (Func.isEmpty(dictionaryAddReqDTOList)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        List<DictionaryDTO> dictionaryDTOList = Lists.newArrayList();
        for (DictionaryAddReqDTO dictionaryAddReqDTO : dictionaryAddReqDTOList) {
            DictionaryDTO dictionaryDto = new DictionaryDTO();
            BeanKit.copyProperties(dictionaryAddReqDTO, dictionaryDto, DEMO_CONVERTER);
            dictionaryDTOList.add(dictionaryDto);
        }
        return super.saveBatchAllColumn(dictionaryDTOList);
    }

    public DictionaryShowResDTO show(final Long id) {
        DictionaryDTO dictionaryDto = super.findById(id);

        if (!Func.isEmpty(dictionaryDto)) {
            DictionaryShowResDTO dictionaryShowResDTO = new DictionaryShowResDTO();
            BeanKit.copyProperties(dictionaryDto, dictionaryShowResDTO, DEMO_CONVERTER);
            return dictionaryShowResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<DictionaryShowResDTO> showByIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(ModelResult.CODE_200, "集合不能为空且大小大于0");
        }

        List<DictionaryDTO> dictionaryDtoList = super.findBatchIds(ids);

        if (!Func.isEmpty(dictionaryDtoList)) {
            List<DictionaryShowResDTO> dictionaryShowResDTOList = Lists.newArrayList();
            for (DictionaryDTO dictionaryDto : dictionaryDtoList) {
                DictionaryShowResDTO dictionaryShowResDTO = new DictionaryShowResDTO();
                BeanKit.copyProperties(dictionaryDto, dictionaryShowResDTO, DEMO_CONVERTER);
                dictionaryShowResDTOList.add(dictionaryShowResDTO);
            }
            return dictionaryShowResDTOList;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean modify(final DictionaryModifyReqDTO dictionaryModifyReqDTO) {
        if (Func.isEmpty(dictionaryModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        DictionaryDTO dictionaryDto = new DictionaryDTO();
        BeanKit.copyProperties(dictionaryModifyReqDTO, dictionaryDto, DEMO_CONVERTER);
        return super.modifyById(dictionaryDto);
    }

    public Boolean modifyAllColumn(final DictionaryModifyReqDTO dictionaryModifyReqDTO) {
        if (Func.isEmpty(dictionaryModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        DictionaryDTO dictionaryDto = new DictionaryDTO();
        BeanKit.copyProperties(dictionaryModifyReqDTO, dictionaryDto, DEMO_CONVERTER);
        return super.modifyAllColumnById(dictionaryDto);
    }

    public Boolean removeByParams(final DictionaryRemoveReqDTO dictionaryRemoveReqDTO) {
        if (Func.isEmpty(dictionaryRemoveReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        DictionaryDTO dictionaryParamsDTO = new DictionaryDTO();
        BeanKit.copyProperties(dictionaryRemoveReqDTO, dictionaryParamsDTO, DEMO_CONVERTER);
        return super.remove(dictionaryParamsDTO);
    }


    @Override
    protected List<DictionaryDTO> entityToDTOList(final List<DictionaryEntity> dictionaryEntityList) {
        List<DictionaryDTO> dictionaryDtoList = null;
        if (!Func.isEmpty(dictionaryEntityList)) {
            dictionaryDtoList = Lists.newArrayList();
            for (DictionaryEntity dictionaryEntity : dictionaryEntityList) {
                dictionaryDtoList.add(entityToDTO(dictionaryEntity));
            }
        }
        return dictionaryDtoList;
    }

    @Override
    protected DictionaryDTO entityToDTO(final DictionaryEntity dictionaryEntity) {
        DictionaryDTO dictionaryDto = null;
        if (!Func.isEmpty(dictionaryEntity)) {
            dictionaryDto = new DictionaryDTO();
            BeanKit.copyProperties(dictionaryEntity, dictionaryDto);
        }
        return dictionaryDto;
    }

    @Override
    protected List<DictionaryEntity> dtoToEntityList(final List<DictionaryDTO> dictionaryDtoList) {
        List<DictionaryEntity> dictionaryEntityList = null;
        if (!Func.isEmpty(dictionaryDtoList)) {
            dictionaryEntityList = Lists.newArrayList();
            for (DictionaryDTO dictionaryDto : dictionaryDtoList) {
                dictionaryEntityList.add(dtoToEntity(dictionaryDto));
            }
        }
        return dictionaryEntityList;
    }

    @Override
    protected DictionaryEntity dtoToEntity(final DictionaryDTO dictionaryDto) {
        DictionaryEntity dictionaryEntity = null;
        if (!Func.isEmpty(dictionaryDto)) {
            dictionaryEntity = new DictionaryEntity();
            BeanKit.copyProperties(dictionaryDto, dictionaryEntity);
        }
        return dictionaryEntity;
    }

    @Override
    protected DictionaryEntity mapToEntity(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new DictionaryEntity();
        }
        return (DictionaryEntity) MapKit.toBean(map, DictionaryEntity.class);
    }

    @Override
    protected DictionaryDTO mapToDto(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new DictionaryDTO();
        }
        return (DictionaryDTO) MapKit.toBean(map, DictionaryDTO.class);
    }
}
