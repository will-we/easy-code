package com.jeasy.resource.api;

import com.jeasy.BaseJUnitTester4SpringContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 菜单 ApiJUnitTest
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
public class ResourceApiJUnitTest extends BaseJUnitTester4SpringContext {

    @Autowired
    private ResourceApi resourceApi;

    @Test
    public void testList() {
        System.out.println(resourceApi.list(null).toString());
    }

    @Test
    public void testList1_1_0() {
        System.out.println(resourceApi.list1_1_0(null).toString());
    }

    @Test
    public void testList1_2_0() {
        System.out.println(resourceApi.list1_2_0(null).toString());
    }

    @Test
    public void testList1_3_0() {
        System.out.println(resourceApi.list1_3_0(null).toString());
    }

    @Test
    public void testPage() {
        System.out.println(resourceApi.page(null, 1, 10).toString());
    }

    @Test
    public void testAdd() {
        System.out.println(resourceApi.add(null).toString());
    }

    @Test
    public void testShow() {
        System.out.println(resourceApi.show(2l).toString());
    }

    @Test
    public void testModify() {
        System.out.println(resourceApi.modify(null).toString());
    }

    @Test
    public void testRemove() {
        System.out.println(resourceApi.remove(2l).toString());
    }
}
