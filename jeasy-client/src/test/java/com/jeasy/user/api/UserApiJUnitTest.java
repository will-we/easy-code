package com.jeasy.user.api;

import com.jeasy.BaseJUnitTester4SpringContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 用户 ApiJUnitTest
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
public class UserApiJUnitTest extends BaseJUnitTester4SpringContext {

    @Autowired
    private UserApi userApi;

    @Test
    public void testList() {
        System.out.println(userApi.list(null).toString());
    }

    @Test
    public void testList1_1_0() {
        System.out.println(userApi.list1_1_0(null).toString());
    }

    @Test
    public void testList1_2_0() {
        System.out.println(userApi.list1_2_0(null).toString());
    }

    @Test
    public void testList1_3_0() {
        System.out.println(userApi.list1_3_0(null).toString());
    }

    @Test
    public void testPage() {
        System.out.println(userApi.page(null, 1, 10).toString());
    }

    @Test
    public void testAdd() {
        System.out.println(userApi.add(null).toString());
    }

    @Test
    public void testShow() {
        System.out.println(userApi.show(2l).toString());
    }

    @Test
    public void testModify() {
        System.out.println(userApi.modify(null).toString());
    }

    @Test
    public void testRemove() {
        System.out.println(userApi.remove(2l).toString());
    }
}
