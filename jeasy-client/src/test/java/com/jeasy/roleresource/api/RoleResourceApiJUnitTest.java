package com.jeasy.roleresource.api;

import com.jeasy.BaseJUnitTester4SpringContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 角色资源 ApiJUnitTest
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
public class RoleResourceApiJUnitTest extends BaseJUnitTester4SpringContext {

    @Autowired
    private RoleResourceApi roleResourceApi;

    @Test
    public void testList() {
        System.out.println(roleResourceApi.list(null).toString());
    }

    @Test
    public void testList1_1_0() {
        System.out.println(roleResourceApi.list1_1_0(null).toString());
    }

    @Test
    public void testList1_2_0() {
        System.out.println(roleResourceApi.list1_2_0(null).toString());
    }

    @Test
    public void testList1_3_0() {
        System.out.println(roleResourceApi.list1_3_0(null).toString());
    }

    @Test
    public void testPage() {
        System.out.println(roleResourceApi.page(null, 1, 10).toString());
    }

    @Test
    public void testAdd() {
        System.out.println(roleResourceApi.add(null).toString());
    }

    @Test
    public void testShow() {
        System.out.println(roleResourceApi.show(2l).toString());
    }

    @Test
    public void testModify() {
        System.out.println(roleResourceApi.modify(null).toString());
    }

    @Test
    public void testRemove() {
        System.out.println(roleResourceApi.remove(2l).toString());
    }
}
