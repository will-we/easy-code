package com.jeasy.log.api;

import com.jeasy.BaseJUnitTester4SpringContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 日志 ApiJUnitTest
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
public class LogApiJUnitTest extends BaseJUnitTester4SpringContext {

    @Autowired
    private LogApi logApi;

    @Test
    public void testList() {
        System.out.println(logApi.list(null).toString());
    }

    @Test
    public void testList1_1_0() {
        System.out.println(logApi.list1_1_0(null).toString());
    }

    @Test
    public void testList1_2_0() {
        System.out.println(logApi.list1_2_0(null).toString());
    }

    @Test
    public void testList1_3_0() {
        System.out.println(logApi.list1_3_0(null).toString());
    }

    @Test
    public void testPage() {
        System.out.println(logApi.page(null, 1, 10).toString());
    }

    @Test
    public void testAdd() {
        System.out.println(logApi.add(null).toString());
    }

    @Test
    public void testShow() {
        System.out.println(logApi.show(2l).toString());
    }

    @Test
    public void testModify() {
        System.out.println(logApi.modify(null).toString());
    }

    @Test
    public void testRemove() {
        System.out.println(logApi.remove(2l).toString());
    }
}
