package com.jeasy.userorg.api;

import com.jeasy.BaseJUnitTester4SpringContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 用户机构 ApiJUnitTest
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@Slf4j
public class UserOrgApiJUnitTest extends BaseJUnitTester4SpringContext {

    @Autowired
    private UserOrgApi userOrgApi;

    @Test
    public void testList() {
        System.out.println(userOrgApi.list(null).toString());
    }

    @Test
    public void testList1_1_0() {
        System.out.println(userOrgApi.list1_1_0(null).toString());
    }

    @Test
    public void testList1_2_0() {
        System.out.println(userOrgApi.list1_2_0(null).toString());
    }

    @Test
    public void testList1_3_0() {
        System.out.println(userOrgApi.list1_3_0(null).toString());
    }

    @Test
    public void testPage() {
        System.out.println(userOrgApi.page(null, 1, 10).toString());
    }

    @Test
    public void testAdd() {
        System.out.println(userOrgApi.add(null).toString());
    }

    @Test
    public void testShow() {
        System.out.println(userOrgApi.show(2l).toString());
    }

    @Test
    public void testModify() {
        System.out.println(userOrgApi.modify(null).toString());
    }

    @Test
    public void testRemove() {
        System.out.println(userOrgApi.remove(2l).toString());
    }
}
