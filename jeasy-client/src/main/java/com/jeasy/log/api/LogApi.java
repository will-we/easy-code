package com.jeasy.log.api;

import com.jeasy.base.client.API;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.base.web.resolver.FromJson;
import com.jeasy.common.charset.CharsetKit;
import com.jeasy.httphelper.annotation.Header;
import com.jeasy.httphelper.annotation.WSRequest;
import com.jeasy.httphelper.exception.WSException;
import com.jeasy.httphelper.model.WSRequestContext;
import com.jeasy.httphelper.request.WSAnnotationHttpRequest;
import com.jeasy.log.dto.*;

/**
 * 日志 API
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@API
public abstract class LogApi extends WSAnnotationHttpRequest {

    @Override
    public void init(WSRequestContext context) throws WSException {
    }

    @WSRequest(
        name = "列表",
        url = "http://127.0.0.1:8080/skbg/log/list",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult<LogListResDTO> list(@FromJson(key = "body") LogListReqDTO logListReqDTO);

    @WSRequest(
        name = "列表1.1.0",
        url = "http://127.0.0.1:8080/skbg/log/list",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class,
        headers = {
            @Header(name = "version", value = "1.1.0")
        }
    )
    public abstract ModelResult<LogListResDTO> list1_1_0(@FromJson(key = "body") LogListReqDTO logListReqDTO);

    @WSRequest(
        name = "列表1.2.0",
        url = "http://127.0.0.1:8080/skbg/log/list",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class,
        headers = {
            @Header(name = "version", value = "1.2.0"),
            @Header(name = "platform", value = "APP")
        }
    )
    public abstract ModelResult<LogListResDTO> list1_2_0(@FromJson(key = "body") LogListReqDTO logListReqDTO);

    @WSRequest(
        name = "列表1.3.0",
        url = "http://127.0.0.1:8080/skbg/log/list",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class,
        headers = {
            @Header(name = "version", value = "1.3.0"),
            @Header(name = "platform", value = "APP"),
            @Header(name = "device", value = "IOS")
        }
    )
    public abstract ModelResult<LogListResDTO> list1_3_0(@FromJson(key = "body") LogListReqDTO logListReqDTO);

    @WSRequest(
        name = "分页",
        url = "http://127.0.0.1:8080/skbg/log/page",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class,
        headers = {
            @Header(name = "version", value = "1.0.0"),
            @Header(name = "platform", value = "APP"),
            @Header(name = "device", value = "IOS")
        }
    )
    public abstract ModelResult<LogPageResDTO> page(@FromJson(key = "body") LogPageReqDTO logPageReqDTO, @FromJson(key = "pageNo") Integer pageNo, @FromJson(key = "pageSize") Integer pageSize);

    @WSRequest(
        name = "新增",
        url = "http://127.0.0.1:8080/skbg/log/add",
        method = WSRequest.MethodType.POST,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult add(@FromJson(key = "body") LogAddReqDTO logAddReqDTO);

    @WSRequest(
        name = "详情",
        url = "http://127.0.0.1:8080/skbg/log/show",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult<LogShowResDTO> show(@FromJson(key = "id") Long id);

    @WSRequest(
        name = "更新",
        url = "http://127.0.0.1:8080/skbg/log/modify",
        method = WSRequest.MethodType.POST,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult modify(@FromJson(key = "body") LogModifyReqDTO logModifyReqDTO);

    @WSRequest(
        name = "删除",
        url = "http://127.0.0.1:8080/skbg/log/remove",
        method = WSRequest.MethodType.POST,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult remove(@FromJson(key = "id") Long id);
}
