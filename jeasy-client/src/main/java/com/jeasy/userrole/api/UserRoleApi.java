package com.jeasy.userrole.api;

import com.jeasy.base.client.API;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.base.web.resolver.FromJson;
import com.jeasy.common.charset.CharsetKit;
import com.jeasy.httphelper.annotation.Header;
import com.jeasy.httphelper.annotation.WSRequest;
import com.jeasy.httphelper.exception.WSException;
import com.jeasy.httphelper.model.WSRequestContext;
import com.jeasy.httphelper.request.WSAnnotationHttpRequest;
import com.jeasy.userrole.dto.*;

/**
 * 用户角色 API
 *
 * @author taomk
 * @version 1.0
 * @since 2017/08/21 18:29
 */
@API
public abstract class UserRoleApi extends WSAnnotationHttpRequest {

    @Override
    public void init(WSRequestContext context) throws WSException {
    }

    @WSRequest(
        name = "列表",
        url = "http://127.0.0.1:8080/skbg/userrole/list",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult<UserRoleListResDTO> list(@FromJson(key = "body") UserRoleListReqDTO userroleListReqDTO);

    @WSRequest(
        name = "列表1.1.0",
        url = "http://127.0.0.1:8080/skbg/userrole/list",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class,
        headers = {
            @Header(name = "version", value = "1.1.0")
        }
    )
    public abstract ModelResult<UserRoleListResDTO> list1_1_0(@FromJson(key = "body") UserRoleListReqDTO userroleListReqDTO);

    @WSRequest(
        name = "列表1.2.0",
        url = "http://127.0.0.1:8080/skbg/userrole/list",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class,
        headers = {
            @Header(name = "version", value = "1.2.0"),
            @Header(name = "platform", value = "APP")
        }
    )
    public abstract ModelResult<UserRoleListResDTO> list1_2_0(@FromJson(key = "body") UserRoleListReqDTO userroleListReqDTO);

    @WSRequest(
        name = "列表1.3.0",
        url = "http://127.0.0.1:8080/skbg/userrole/list",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class,
        headers = {
            @Header(name = "version", value = "1.3.0"),
            @Header(name = "platform", value = "APP"),
            @Header(name = "device", value = "IOS")
        }
    )
    public abstract ModelResult<UserRoleListResDTO> list1_3_0(@FromJson(key = "body") UserRoleListReqDTO userroleListReqDTO);

    @WSRequest(
        name = "分页",
        url = "http://127.0.0.1:8080/skbg/userrole/page",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class,
        headers = {
            @Header(name = "version", value = "1.0.0"),
            @Header(name = "platform", value = "APP"),
            @Header(name = "device", value = "IOS")
        }
    )
    public abstract ModelResult<UserRolePageResDTO> page(@FromJson(key = "body") UserRolePageReqDTO userrolePageReqDTO, @FromJson(key = "pageNo") Integer pageNo, @FromJson(key = "pageSize") Integer pageSize);

    @WSRequest(
        name = "新增",
        url = "http://127.0.0.1:8080/skbg/userrole/add",
        method = WSRequest.MethodType.POST,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult add(@FromJson(key = "body") UserRoleAddReqDTO userroleAddReqDTO);

    @WSRequest(
        name = "详情",
        url = "http://127.0.0.1:8080/skbg/userrole/show",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult<UserRoleShowResDTO> show(@FromJson(key = "id") Long id);

    @WSRequest(
        name = "更新",
        url = "http://127.0.0.1:8080/skbg/userrole/modify",
        method = WSRequest.MethodType.POST,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult modify(@FromJson(key = "body") UserRoleModifyReqDTO userroleModifyReqDTO);

    @WSRequest(
        name = "删除",
        url = "http://127.0.0.1:8080/skbg/userrole/remove",
        method = WSRequest.MethodType.POST,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult remove(@FromJson(key = "id") Long id);
}
