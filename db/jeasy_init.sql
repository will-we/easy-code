CREATE DATABASE `jeasy` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE `jeasy`;
--
-- 用户管理-用户
--
DROP TABLE IF EXISTS `su_user`;
CREATE TABLE `su_user` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `loginName` VARCHAR(64) NOT NULL COMMENT '登录名',
  `code` VARCHAR(64) DEFAULT '' COMMENT '编码',
  `pwd` VARCHAR(64) NOT NULL COMMENT '密码',
  `salt` VARCHAR(64) DEFAULT '' COMMENT '加密盐',
  `mobile` VARCHAR(11) DEFAULT '' COMMENT '手机号',

  `statusVal` INT(4) DEFAULT 0 COMMENT '用户状态值:1000=启用,1001=停用',
  `statusCode` VARCHAR(16) DEFAULT '' COMMENT '用户状态编码:字典',

  `remark` VARCHAR(64) DEFAULT '' COMMENT '备注',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户';

--
-- 用户管理-用户角色
--
DROP TABLE IF EXISTS `su_user_role`;
CREATE TABLE `su_user_role` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `userId` BIGINT(8) NOT NULL COMMENT '用户ID',
  `userName` VARCHAR(32) DEFAULT '' COMMENT '用户名称',
  `userCode` VARCHAR(64) DEFAULT '' COMMENT '用户编码',

  `roleId` BIGINT(8) NOT NULL COMMENT '角色ID',
  `roleName` VARCHAR(32) DEFAULT '' COMMENT '角色名称',
  `roleCode` VARCHAR(64) DEFAULT '' COMMENT '角色编码',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户角色';

--
-- 用户管理-用户机构
--
DROP TABLE IF EXISTS `su_user_org`;
CREATE TABLE `su_user_org` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `userId` BIGINT(8) NOT NULL COMMENT '用户ID',
  `userName` VARCHAR(32) DEFAULT '' COMMENT '用户名称',
  `userCode` VARCHAR(64) DEFAULT '' COMMENT '用户标示',

  `orgId` BIGINT(8) NOT NULL COMMENT '机构ID',
  `orgName` VARCHAR(32) DEFAULT '' COMMENT '机构名称',
  `orgCode` VARCHAR(64) DEFAULT '' COMMENT '机构编码',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户机构';


--
-- 权限管理-角色
--
DROP TABLE IF EXISTS `su_role`;
CREATE TABLE `su_role` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `code` VARCHAR(64) DEFAULT '' COMMENT '编码',

  `remark` VARCHAR(64) DEFAULT '' COMMENT '备注',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='角色';

--
-- 权限管理-资源(菜单&操作)
--
DROP TABLE IF EXISTS `su_resource`;
CREATE TABLE `su_resource` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `code` VARCHAR(32) DEFAULT '' COMMENT '编码',
  `url` VARCHAR(128) DEFAULT '' COMMENT 'URL',
  `icon` VARCHAR(64) DEFAULT '' COMMENT '图标',

  `remark` VARCHAR(64) DEFAULT '' COMMENT '备注/描述',

  `pid` BIGINT(8) DEFAULT 0 COMMENT '父ID',
  `sort` TINYINT(1) DEFAULT 0 COMMENT '排序',
  `isMenu` TINYINT(1) NOT NULL COMMENT '是否菜单:0=否,1=是',
  `isLeaf` TINYINT(1) NOT NULL COMMENT '是否叶子节点:0=否,1=是',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='菜单';

--
-- 权限管理-角色资源
--
DROP TABLE IF EXISTS `su_role_resource`;
CREATE TABLE `su_role_resource` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `roleId` BIGINT(8) NOT NULL COMMENT '角色ID',
  `roleName` VARCHAR(32) DEFAULT '' COMMENT '角色名称',
  `roleCode` VARCHAR(64) DEFAULT '' COMMENT '角色编码',

  `resourceId` BIGINT(8) NOT NULL COMMENT '资源ID',
  `resourceName` VARCHAR(32) DEFAULT '' COMMENT '资源名称',
  `resourceCode` VARCHAR(64) DEFAULT '' COMMENT '资源编码',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='角色资源';

--
-- 权限管理-机构
--
DROP TABLE IF EXISTS `su_organization`;
CREATE TABLE `su_organization` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `code` VARCHAR(64) DEFAULT '' COMMENT '编码',
  `address` VARCHAR(128) DEFAULT '' COMMENT '地址',

  `typeVal` INT(4) DEFAULT 0 COMMENT '机构类型值',
  `typeCode` VARCHAR(16) DEFAULT '' COMMENT '机构类型编码:字典',

  `sort` TINYINT(1) DEFAULT 0 COMMENT '排序',
  `isLeaf` TINYINT(1) NOT NULL COMMENT '是否叶子节点:0=否,1=是',
  `pid` BIGINT(8) DEFAULT 0 COMMENT '父ID',
  `icon` VARCHAR(128) DEFAULT '' COMMENT '图标',

  `remark` VARCHAR(64) DEFAULT '' COMMENT '备注/描述',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='机构';

--
-- 基础数据-日志
--
DROP TABLE IF EXISTS `bd_log`;
CREATE TABLE `bd_log` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `tableName` VARCHAR(32) NOT NULL COMMENT '表名称',
  `recordId` BIGINT(8) NOT NULL COMMENT '记录ID',
  `fieldName` VARCHAR(128) DEFAULT '' COMMENT '字段名称',

  `logTypeVal` INT(4) DEFAULT 0 COMMENT '日志类型值',
  `logTypeCode` VARCHAR(16) DEFAULT '' COMMENT '日志类型编码:字典',

  `optTypeVal` INT(4) DEFAULT 0 COMMENT '操作类型值',
  `optTypeCode` VARCHAR(16) DEFAULT '' COMMENT '操作类型编码:字典',
  `optDesc` VARCHAR(128) DEFAULT '' COMMENT '操作类型描述',

  `beforeValue` VARCHAR(128) DEFAULT '' COMMENT '操作前值',
  `afterValue` VARCHAR(128) DEFAULT '' COMMENT '操作后值',

  `remark` VARCHAR(256) DEFAULT '' COMMENT '备注',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='日志';

--
-- 基础数据-字典
--
DROP TABLE IF EXISTS `bd_dictionary`;
CREATE TABLE `bd_dictionary` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `code` VARCHAR(16) NOT NULL COMMENT '编号',
  `value` INT(4) NOT NULL COMMENT '值',

  `type` VARCHAR(16) DEFAULT '' COMMENT '类型',
  `sort` TINYINT(1) DEFAULT 0 COMMENT '排序',

  `pid` BIGINT(8) DEFAULT 0 COMMENT '父ID',
  `pcode` VARCHAR(16) DEFAULT '' COMMENT '父编号',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='字典';

--
-- 基础数据-文件附件
--
DROP TABLE IF EXISTS `bd_file_attach`;
CREATE TABLE `bd_file_attach` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `tableName` VARCHAR(32) NOT NULL COMMENT '表名称',
  `recordId` BIGINT(8) NOT NULL COMMENT '记录ID',

  `name` VARCHAR(64) DEFAULT '' COMMENT '文件原名称',
  `url` VARCHAR(256) DEFAULT '' COMMENT '文件URL',
  `iconUrl` VARCHAR(256) DEFAULT '' COMMENT '文件图标URL',
  `previewUrl` VARCHAR(256) DEFAULT '' COMMENT '文件预览URL',

  `createAt` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `createBy` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `createName` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `updateAt` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `updateBy` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `updateName` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `isDel` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `isTest` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文件附件';

USE `jeasy`;

--
-- 初始化:字典
--
TRUNCATE `bd_dictionary`;
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  (1, '启用', 'QY', 1000, 'YHZT', 0, 0, '', 1497151665245, 0, 'SYSTEM', 1497151665245, 0, 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  (2, '停用', 'TY', 1001, 'YHZT', 0, 0, '', 1497151665288, 0, 'SYSTEM', 1497151665288, 0, 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `sort`, `pid`, `pcode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  (3, '其他', 'QT', 2000, 'JGLX', 0, 0, '', 1497151665290, 0, 'SYSTEM', 1497151665290, 0, 'SYSTEM', 0, 0);

--
-- 初始化:菜单资源+角色资源
--
TRUNCATE `su_resource`;
TRUNCATE `su_role_resource`;
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (1, '用户管理', 'YHGL', '', 'icon-lessee', '', 0, 0, 1, 0, 1497155485517, 0, 'SYSTEM', 1497155485517, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (1, 1, '超级管理员', 'CJGLY', 1, '用户管理', 'YHGL', 1497155485533, 0, 'SYSTEM', 1497155485533, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (2, '人员管理', 'YHGL:RYGL', '1.html', 'icon-nav', '', 1, 0, 1, 1, 1497155485533, 0, 'SYSTEM', 1497155485533, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (2, 1, '超级管理员', 'CJGLY', 2, '人员管理', 'YHGL:RYGL', 1497155485533, 0, 'SYSTEM', 1497155485533, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (3, '查询', 'YHGL:RYGL:CX', '', '', '', 2, 0, 0, 1, 1497155485533, 0, 'SYSTEM', 1497155485533, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (3, 1, '超级管理员', 'CJGLY', 3, '查询', 'YHGL:RYGL:CX', 1497155485534, 0, 'SYSTEM', 1497155485534, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (4, '添加', 'YHGL:RYGL:TJ', '', '', '', 2, 0, 0, 1, 1497155485534, 0, 'SYSTEM', 1497155485534, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (4, 1, '超级管理员', 'CJGLY', 4, '添加', 'YHGL:RYGL:TJ', 1497155485534, 0, 'SYSTEM', 1497155485534, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (5, '修改', 'YHGL:RYGL:XG', '', '', '', 2, 0, 0, 1, 1497155485534, 0, 'SYSTEM', 1497155485534, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (5, 1, '超级管理员', 'CJGLY', 5, '修改', 'YHGL:RYGL:XG', 1497155485534, 0, 'SYSTEM', 1497155485534, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (6, '删除', 'YHGL:RYGL:SC', '', '', '', 2, 0, 0, 1, 1497155485535, 0, 'SYSTEM', 1497155485535, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (6, 1, '超级管理员', 'CJGLY', 6, '删除', 'YHGL:RYGL:SC', 1497155485535, 0, 'SYSTEM', 1497155485535, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (7, '角色管理', 'YHGL:JSGL', '2.html', 'icon-nav', '', 1, 0, 1, 1, 1497155485535, 0, 'SYSTEM', 1497155485535, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (7, 1, '超级管理员', 'CJGLY', 7, '角色管理', 'YHGL:JSGL', 1497155485535, 0, 'SYSTEM', 1497155485535, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (8, '查询', 'YHGL:JSGL:CX', '', '', '', 7, 0, 0, 1, 1497155485535, 0, 'SYSTEM', 1497155485535, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (8, 1, '超级管理员', 'CJGLY', 8, '查询', 'YHGL:JSGL:CX', 1497155485535, 0, 'SYSTEM', 1497155485535, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (9, '添加', 'YHGL:JSGL:TJ', '', '', '', 7, 0, 0, 1, 1497155485536, 0, 'SYSTEM', 1497155485536, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (9, 1, '超级管理员', 'CJGLY', 9, '添加', 'YHGL:JSGL:TJ', 1497155485536, 0, 'SYSTEM', 1497155485536, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (10, '修改', 'YHGL:JSGL:XG', '', '', '', 7, 0, 0, 1, 1497155485536, 0, 'SYSTEM', 1497155485536, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (10, 1, '超级管理员', 'CJGLY', 10, '修改', 'YHGL:JSGL:XG', 1497155485536, 0, 'SYSTEM', 1497155485536, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (11, '删除', 'YHGL:JSGL:SC', '', '', '', 7, 0, 0, 1, 1497155485536, 0, 'SYSTEM', 1497155485536, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (11, 1, '超级管理员', 'CJGLY', 11, '删除', 'YHGL:JSGL:SC', 1497155485537, 0, 'SYSTEM', 1497155485537, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (12, '组织机构', 'YHGL:ZZJG', '3.html', 'icon-nav', '', 1, 0, 1, 1, 1497155485537, 0, 'SYSTEM', 1497155485537, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (12, 1, '超级管理员', 'CJGLY', 12, '组织机构', 'YHGL:ZZJG', 1497155485537, 0, 'SYSTEM', 1497155485537, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (13, '查询', 'YHGL:ZZJG:CX', '', '', '', 12, 0, 0, 1, 1497155485537, 0, 'SYSTEM', 1497155485537, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (13, 1, '超级管理员', 'CJGLY', 13, '查询', 'YHGL:ZZJG:CX', 1497155485537, 0, 'SYSTEM', 1497155485537, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (14, '添加', 'YHGL:ZZJG:TJ', '', '', '', 12, 0, 0, 1, 1497155485538, 0, 'SYSTEM', 1497155485538, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (14, 1, '超级管理员', 'CJGLY', 14, '添加', 'YHGL:ZZJG:TJ', 1497155485538, 0, 'SYSTEM', 1497155485538, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (15, '修改', 'YHGL:ZZJG:XG', '', '', '', 12, 0, 0, 1, 1497155485538, 0, 'SYSTEM', 1497155485538, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (15, 1, '超级管理员', 'CJGLY', 15, '修改', 'YHGL:ZZJG:XG', 1497155485538, 0, 'SYSTEM', 1497155485538, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (16, '删除', 'YHGL:ZZJG:SC', '', '', '', 12, 0, 0, 1, 1497155485538, 0, 'SYSTEM', 1497155485538, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (16, 1, '超级管理员', 'CJGLY', 16, '删除', 'YHGL:ZZJG:SC', 1497155485539, 0, 'SYSTEM', 1497155485539, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (17, '菜单权限', 'YHGL:CDQX', '4.html', 'icon-nav', '', 1, 0, 1, 1, 1497155485539, 0, 'SYSTEM', 1497155485539, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (17, 1, '超级管理员', 'CJGLY', 17, '菜单权限', 'YHGL:CDQX', 1497155485539, 0, 'SYSTEM', 1497155485539, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (18, '查询', 'YHGL:CDQX:CX', '', '', '', 17, 0, 0, 1, 1497155485539, 0, 'SYSTEM', 1497155485539, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (18, 1, '超级管理员', 'CJGLY', 18, '查询', 'YHGL:CDQX:CX', 1497155485539, 0, 'SYSTEM', 1497155485539, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (19, '添加', 'YHGL:CDQX:TJ', '', '', '', 17, 0, 0, 1, 1497155485540, 0, 'SYSTEM', 1497155485540, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (19, 1, '超级管理员', 'CJGLY', 19, '添加', 'YHGL:CDQX:TJ', 1497155485540, 0, 'SYSTEM', 1497155485540, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (20, '修改', 'YHGL:CDQX:XG', '', '', '', 17, 0, 0, 1, 1497155485540, 0, 'SYSTEM', 1497155485540, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (20, 1, '超级管理员', 'CJGLY', 20, '修改', 'YHGL:CDQX:XG', 1497155485540, 0, 'SYSTEM', 1497155485540, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (21, '删除', 'YHGL:CDQX:SC', '', '', '', 17, 0, 0, 1, 1497155485541, 0, 'SYSTEM', 1497155485541, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (21, 1, '超级管理员', 'CJGLY', 21, '删除', 'YHGL:CDQX:SC', 1497155485541, 0, 'SYSTEM', 1497155485541, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (22, '基础数据', 'JCSJ', '', 'icon-car', '', 0, 0, 1, 0, 1497155485541, 0, 'SYSTEM', 1497155485541, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (22, 1, '超级管理员', 'CJGLY', 22, '基础数据', 'JCSJ', 1497155485541, 0, 'SYSTEM', 1497155485541, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (23, '公共码表', 'JCSJ:GGMB', '5.html', 'icon-nav', '', 22, 0, 1, 1, 1497155485541, 0, 'SYSTEM', 1497155485541, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (23, 1, '超级管理员', 'CJGLY', 23, '公共码表', 'JCSJ:GGMB', 1497155485541, 0, 'SYSTEM', 1497155485541, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (24, '查询', 'JCSJ:GGMB:CX', '', '', '', 23, 0, 0, 1, 1497155485542, 0, 'SYSTEM', 1497155485542, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (24, 1, '超级管理员', 'CJGLY', 24, '查询', 'JCSJ:GGMB:CX', 1497155485542, 0, 'SYSTEM', 1497155485542, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (25, '添加', 'JCSJ:GGMB:TJ', '', '', '', 23, 0, 0, 1, 1497155485542, 0, 'SYSTEM', 1497155485542, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (25, 1, '超级管理员', 'CJGLY', 25, '添加', 'JCSJ:GGMB:TJ', 1497155485542, 0, 'SYSTEM', 1497155485542, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (26, '修改', 'JCSJ:GGMB:XG', '', '', '', 23, 0, 0, 1, 1497155485542, 0, 'SYSTEM', 1497155485542, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (26, 1, '超级管理员', 'CJGLY', 26, '修改', 'JCSJ:GGMB:XG', 1497155485543, 0, 'SYSTEM', 1497155485543, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (27, '删除', 'JCSJ:GGMB:SC', '', '', '', 23, 0, 0, 1, 1497155485543, 0, 'SYSTEM', 1497155485543, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (27, 1, '超级管理员', 'CJGLY', 27, '删除', 'JCSJ:GGMB:SC', 1497155485543, 0, 'SYSTEM', 1497155485543, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (28, '日志监控', 'RZJK', '', 'icon-entry', '', 0, 0, 1, 0, 1497155485543, 0, 'SYSTEM', 1497155485543, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (28, 1, '超级管理员', 'CJGLY', 28, '日志监控', 'RZJK', 1497155485543, 0, 'SYSTEM', 1497155485543, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (29, '操作日志', 'RZJK:CZRZ', '6.html', 'icon-nav', '', 28, 0, 1, 1, 1497155485544, 0, 'SYSTEM', 1497155485544, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (29, 1, '超级管理员', 'CJGLY', 29, '操作日志', 'RZJK:CZRZ', 1497155485544, 0, 'SYSTEM', 1497155485544, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (30, '查询', 'RZJK:CZRZ:CX', '', '', '', 29, 0, 0, 1, 1497155485544, 0, 'SYSTEM', 1497155485544, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (30, 1, '超级管理员', 'CJGLY', 30, '查询', 'RZJK:CZRZ:CX', 1497155485544, 0, 'SYSTEM', 1497155485544, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (31, '添加', 'RZJK:CZRZ:TJ', '', '', '', 29, 0, 0, 1, 1497155485544, 0, 'SYSTEM', 1497155485544, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (31, 1, '超级管理员', 'CJGLY', 31, '添加', 'RZJK:CZRZ:TJ', 1497155485545, 0, 'SYSTEM', 1497155485545, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (32, '修改', 'RZJK:CZRZ:XG', '', '', '', 29, 0, 0, 1, 1497155485545, 0, 'SYSTEM', 1497155485545, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (32, 1, '超级管理员', 'CJGLY', 32, '修改', 'RZJK:CZRZ:XG', 1497155485545, 0, 'SYSTEM', 1497155485545, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (33, '删除', 'RZJK:CZRZ:SC', '', '', '', 29, 0, 0, 1, 1497155485545, 0, 'SYSTEM', 1497155485545, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (33, 1, '超级管理员', 'CJGLY', 33, '删除', 'RZJK:CZRZ:SC', 1497155485546, 0, 'SYSTEM', 1497155485546, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (34, '数据监控', 'RZJK:SJJK', '8.html', 'icon-nav', '', 28, 0, 1, 1, 1497155485546, 0, 'SYSTEM', 1497155485546, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (34, 1, '超级管理员', 'CJGLY', 34, '数据监控', 'RZJK:SJJK', 1497155485546, 0, 'SYSTEM', 1497155485546, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (35, '查询', 'RZJK:SJJK:CX', '', '', '', 34, 0, 0, 1, 1497155485546, 0, 'SYSTEM', 1497155485546, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (35, 1, '超级管理员', 'CJGLY', 35, '查询', 'RZJK:SJJK:CX', 1497155485547, 0, 'SYSTEM', 1497155485547, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (36, '添加', 'RZJK:SJJK:TJ', '', '', '', 34, 0, 0, 1, 1497155485547, 0, 'SYSTEM', 1497155485547, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (36, 1, '超级管理员', 'CJGLY', 36, '添加', 'RZJK:SJJK:TJ', 1497155485547, 0, 'SYSTEM', 1497155485547, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (37, '修改', 'RZJK:SJJK:XG', '', '', '', 34, 0, 0, 1, 1497155485547, 0, 'SYSTEM', 1497155485547, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (37, 1, '超级管理员', 'CJGLY', 37, '修改', 'RZJK:SJJK:XG', 1497155485547, 0, 'SYSTEM', 1497155485547, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `isMenu`, `isLeaf`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (38, '删除', 'RZJK:SJJK:SC', '', '', '', 34, 0, 0, 1, 1497155485548, 0, 'SYSTEM', 1497155485548, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `roleId`, `roleName`, `roleCode`, `resourceId`, `resourceName`, `resourceCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES (38, 1, '超级管理员', 'CJGLY', 38, '删除', 'RZJK:SJJK:SC', 1497155485548, 0, 'SYSTEM', 1497155485548, 0, 'SYSTEM', 0, 0);

--
-- 初始化:角色
--
TRUNCATE `su_role`;
INSERT INTO `su_role` (`id`, `name`, `code`, `remark`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`)
VALUES (1, '超级管理员', 'CJGLY', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);

--
-- 初始化:机构
--
TRUNCATE `su_organization`;
INSERT INTO `su_organization` (`id`, `name`, `code`, `typeVal`, `typeCode`, `sort`, `isLeaf`, `pid`, `remark`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  (1, '超级管理组', 'CJGLZ', 2000, 'QT', 0, 1, 0, '谨慎操作',1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);

--
-- 初始化:用户+用户机构+用户角色
--
TRUNCATE `su_user`;
INSERT INTO `su_user` (`id`, `name`, `loginName`, `code`, `pwd`, `salt`, `mobile`, `statusVal`, `statusCode`,  `remark`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  (1, 'admin', 'admin', 'ADMIN', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13011111111', 1000, 'QY', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);


TRUNCATE `su_user_org`;
INSERT INTO `su_user_org` (`id`, `userId`, `userName`, `userCode`, `orgId`, `orgName`, `orgCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  (1, 1, 'admin', 'ADMIN', 1, '超级管理组', 'CJGLZ', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);

TRUNCATE `su_user_role`;
INSERT INTO `su_user_role` (`id`, `userId`, `userName`, `userCode`, `roleId`, `roleName`, `roleCode`, `createAt`, `createBy`, `createName`, `updateAt`, `updateBy`, `updateName`, `isDel`, `isTest`) VALUES
  (1, 1, 'admin', 'ADMIN', 1, '超级管理员', 'CJGLY', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
