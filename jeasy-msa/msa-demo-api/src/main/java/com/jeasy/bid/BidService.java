package com.jeasy.bid;

/**
 * @author TaoBangren
 * @version 1.0
 * @since 2017/5/17 上午9:26
 */
public interface BidService {

    BidResponse bid(BidRequest request);

    void throwNPE() throws NullPointerException;
}
