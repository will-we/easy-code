package com.jeasy.user;

/**
 * @author TaoBangren
 * @version 1.0
 * @since 2017/5/17 上午9:26
 */
public interface UserService {

    User getUser(Long id);

    Long registerUser(User user);
}
