package com.jeasy.base.web.xss;

import com.jeasy.common.ConvertKit;
import com.jeasy.common.Func;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 安全的Filter(过滤SQL和XSS)
 *
 * @author taomk
 * @version 1.0
 * @since 15-8-4 下午4:45
 */
public class HttpServletRequestSafeFilter implements Filter {

    FilterConfig filterConfig = null;

    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    public void destroy() {
        this.filterConfig = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        boolean filterXSS = false;
        boolean filterSQL = false;

        if (Func.isNotEmpty(filterConfig.getInitParameter("filterXSS"))) {
            filterXSS = ConvertKit.toBool(filterConfig.getInitParameter("filterXSS"));
        }

        if (Func.isNotEmpty(filterConfig.getInitParameter("filterSQL"))) {
            filterSQL = ConvertKit.toBool(filterConfig.getInitParameter("filterSQL"));
        }

        chain.doFilter(new HttpServletRequestSafeWrapper((HttpServletRequest) request, filterXSS, filterSQL), response);
    }
}
