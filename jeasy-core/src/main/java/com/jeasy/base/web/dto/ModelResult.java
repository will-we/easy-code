package com.jeasy.base.web.dto;

import com.google.common.collect.Maps;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * ModelResult
 *
 * @author taobr
 * @version 1.0
 * @since 2015/05/13 17:34
 */
@Data
public final class ModelResult<T> {

    public static final String SUCCESS = "success";
    public static final String FAIL = "fail";
    public static final String UNAUTHORIZED = "unauthorized request";

    public static final int CODE_200 = 200;
    public static final int CODE_400 = 400;
    public static final int CODE_500 = 500;

    // (未授权） 请求要求身份验证。对于需要登录的网页，服务器可能返回此响应。
    public static final int CODE_401 = 401;
    // (未授权) 不接受
    public static final int CODE_406 = 406;

    private int code;

    private final Map<String, Object> data = Maps.newHashMap();

    public ModelResult() {
    }

    public ModelResult(final int code) {
        this.code = code;
    }

    public ModelResult setResultPage(final ResultPage resultPage) {
        data.put("totalCount", resultPage.getTotalCount());
        data.put("totalPage", resultPage.getTotalPage());
        data.put("pageSize", resultPage.getPageSize());
        data.put("list", resultPage.getItems());

        data.put("pageNo", resultPage.getPageNo());
        data.put("recordCount", resultPage.getItems() == null ? 0 : resultPage.getItems().size());
        return this;
    }

    public int getTotalCount() {
        if (!data.containsKey("totalCount")) {
            return 0;
        }
        return Integer.valueOf(String.valueOf(data.get("totalCount")));
    }

    public int getTotalPage() {
        if (!data.containsKey("totalPage")) {
            return 0;
        }
        return Integer.valueOf(String.valueOf(data.get("totalPage")));
    }

    public int getPageSize() {
        if (!data.containsKey("pageSize")) {
            return 0;
        }
        return Integer.valueOf(String.valueOf(data.get("pageSize")));
    }

    public int getPageNo() {
        if (!data.containsKey("pageNo")) {
            return 0;
        }
        return Integer.valueOf(String.valueOf(data.get("pageNo")));
    }

    public int getRecordCount() {
        if (!data.containsKey("recordCount")) {
            return 0;
        }
        return Integer.valueOf(String.valueOf(data.get("recordCount")));
    }

    public void setMessage(final String message) {
        data.put("message", message);
    }

    public String getMessage() {
        return String.valueOf(data.get("message"));
    }

    public void setList(final List<T> list) {
        data.put("list", list);
    }

    public List<T> getList() {
        return (List<T>) data.get("list");
    }

    public void setEntity(T entity) {
        data.put("entity", entity);
    }

    public T getEntity() {
        return (T) data.get("entity");
    }
}
