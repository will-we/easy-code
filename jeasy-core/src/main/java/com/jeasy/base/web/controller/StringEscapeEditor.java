package com.jeasy.base.web.controller;

import com.jeasy.common.str.StrKit;
import com.jeasy.common.web.WafKit;
import org.springframework.web.util.HtmlUtils;

import java.beans.PropertyEditorSupport;

public class StringEscapeEditor extends PropertyEditorSupport {

    private final boolean filterXSS = true;

    private final boolean filterSQL = true;

    public StringEscapeEditor() {
    }

    @Override
    public String getAsText() {
        Object value = getValue();
        return value != null ? value.toString() : StrKit.EMPTY;
    }

    @Override
    public void setAsText(final String text) throws IllegalArgumentException {
        if (text == null) {
            setValue(null);
        } else {
            String filterText;
            if (filterXSS) {
                filterText = WafKit.stripXSS(text);
            }

            if (filterSQL) {
                filterText = WafKit.stripSqlInjection(text);
            }
            setValue(HtmlUtils.htmlEscape(filterText));
        }
    }

}
