package com.jeasy.base.manager.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jeasy.base.manager.BaseManager;
import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.base.mybatis.entity.BaseEntity;
import com.jeasy.base.web.dto.CurrentUser;
import com.jeasy.base.web.dto.Device;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.object.MapKit;
import com.jeasy.common.str.StrKit;
import com.jeasy.common.thread.ThreadLocalKit;
import com.jeasy.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * BaseManagerImpl
 *
 * @param <D> DAO
 * @param <E> Entity
 * @param <T> DTO
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
public abstract class BaseManagerImpl<D extends BaseDAO<E>, E extends BaseEntity, T> implements BaseManager<E, T> {

    private static final String ID = "id";

    @Autowired
    protected D dao;

    @Override
    public Boolean save(T dto) {
        if (Func.isEmpty(dto)) {
            throw new ServiceException(ModelResult.CODE_500, "dto is null or empty or empty while invoke save() method");
        }

        E entity = dtoToEntity(dto);
        Integer result = dao.insert(handleSaveInfo(entity));

        if (Func.isNullOrZero(result)) {
            return false;
        }
        BeanKit.setProperty(dto, ID, entity.getId());
        return true;
    }

    @Override
    public Boolean save(final Map<String, Object> dtoMap) {
        if (Func.isEmpty(dtoMap)) {
            throw new ServiceException(ModelResult.CODE_500, "dtoMap is null or empty or empty while invoke save() method");
        }

        E entity = mapToEntity(dtoMap);
        Integer result = dao.insert(handleSaveInfo(entity));

        if (Func.isNullOrZero(result)) {
            return false;
        }
        dtoMap.put(ID, entity.getId());
        return true;
    }

    @Override
    public Boolean saveAllColumn(T dto) {
        if (Func.isEmpty(dto)) {
            throw new ServiceException(ModelResult.CODE_500, "dto is null or empty or empty while invoke saveAllColumn() method");
        }

        E entity = dtoToEntity(dto);
        Integer result = dao.insertAllColumn(handleSaveInfo(entity));

        if (Func.isNullOrZero(result)) {
            return false;
        }
        BeanKit.setProperty(dto, ID, entity.getId());
        return true;
    }

    @Override
    public Boolean saveBatchAllColumn(final List<T> dtos) {
        if (Func.isEmpty(dtos)) {
            throw new ServiceException(ModelResult.CODE_500, "dtos is null or empty while invoke saveBatchAllColumn() method");
        }

        List<E> entityList = Lists.newArrayList();
        for (T dto : dtos) {
            entityList.add(handleSaveInfo(dtoToEntity(dto)));
        }

        Integer result = dao.insertBatchAllColumn(entityList);
        if (Func.isNullOrZero(result)) {
            return false;
        }
        for (int i = 0; i < dtos.size(); i++) {
            BeanKit.setProperty(dtos.get(i), ID, entityList.get(i).getId());
        }
        return true;
    }

    @Override
    public Boolean saveAllColumn(final Map<String, Object> dtoMap) {
        if (Func.isEmpty(dtoMap)) {
            throw new ServiceException(ModelResult.CODE_500, "dtoMap is null or empty while invoke saveAllColumn() method");
        }

        E entity = mapToEntity(dtoMap);
        Integer result = dao.insertAllColumn(handleSaveInfo(entity));

        if (Func.isNullOrZero(result)) {
            return false;
        }
        dtoMap.put(ID, entity.getId());
        return true;
    }

    @Override
    public Boolean removeById(final Long id) {
        if (Func.isNullOrZero(id)) {
            throw new ServiceException(ModelResult.CODE_500, "id is null or empty while invoke removeById() method");
        }

        Integer result = dao.deleteById(id);
        return Func.isNotEmpty(result);
    }

    @Override
    public Boolean remove(T dto) {
        if (Func.isEmpty(dto)) {
            throw new ServiceException(ModelResult.CODE_500, "dto is null or empty while invoke remove() method");
        }

        E entity = dtoToEntity(dto);
        return remove(MapKit.toObjMap(entity));
    }

    @Override
    public Boolean remove(final Map<String, Object> params) {
        if (Func.isEmpty(params)) {
            throw new ServiceException(ModelResult.CODE_500, "params is null or empty while invoke remove() method");
        }

        Integer result = dao.deleteByMap(params);
        return Func.isNotEmpty(result);
    }

    @Override
    public Boolean removeByWrapper(final Wrapper<E> wrapper) {
        if (Func.isEmpty(wrapper)) {
            throw new ServiceException(ModelResult.CODE_500, "wrapper is null or empty while invoke removeByWrapper() method");
        }

        Integer result = dao.delete(wrapper);
        return Func.isNotEmpty(result);
    }

    @Override
    public Boolean removeBatchIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new ServiceException(ModelResult.CODE_500, "ids is null or empty while invoke removeBatchIds() method");
        }

        Integer result = dao.deleteBatchIds(ids);
        return Func.isNotEmpty(result);
    }

    @Override
    public Boolean modifyById(T dto) {
        if (Func.isEmpty(dto)) {
            throw new ServiceException(ModelResult.CODE_500, "dto is null or empty while invoke modifyById() method");
        }

        Integer result = dao.updateById(handleUpdateInfo(dtoToEntity(dto)));
        return Func.isNotEmpty(result);
    }

    @Override
    public Boolean modifyById(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            throw new ServiceException(ModelResult.CODE_500, "map is null or empty while invoke modifyById() method");
        }

        Integer result = dao.updateById(handleUpdateInfo(mapToEntity(map)));
        return Func.isNotEmpty(result);
    }

    @Override
    public Boolean modifyAllColumnById(T dto) {
        if (Func.isEmpty(dto)) {
            throw new ServiceException(ModelResult.CODE_500, "dto is null or empty while invoke modifyAllColumnById() method");
        }

        Integer result = dao.updateAllColumnById(handleUpdateInfo(dtoToEntity(dto)));
        return Func.isNotEmpty(result);
    }

    @Override
    public Boolean modifyAllColumnById(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            throw new ServiceException(ModelResult.CODE_500, "map is null or empty while invoke modifyAllColumnById() method");
        }

        Integer result = dao.updateAllColumnById(handleUpdateInfo(mapToEntity(map)));
        return Func.isNotEmpty(result);
    }

    @Override
    public Boolean modify(T dto, final Map<String, Object> whereMap) {
        if (Func.isEmpty(whereMap)) {
            throw new ServiceException(ModelResult.CODE_500, "whereMap is null or empty while invoke modify() method");
        }

        Integer result = dao.update(handleUpdateInfo(dtoToEntity(dto)), new EntityWrapper<E>().allEq(whereMap));
        return Func.isNotEmpty(result);
    }

    @Override
    public Boolean modify(T dto, final Wrapper<E> wrapper) {
        if (Func.isEmpty(wrapper)) {
            throw new ServiceException(ModelResult.CODE_500, "wrapper is null or empty while invoke modify() method");
        }

        Integer result = dao.update(handleUpdateInfo(dtoToEntity(dto)), wrapper);
        return Func.isNotEmpty(result);
    }

    @Override
    public T findById(final Long id) {
        if (Func.isEmpty(id)) {
            throw new ServiceException(ModelResult.CODE_500, "id is null or empty while invoke findById() method");
        }

        E entity = dao.selectById(id);
        return entityToDTO(entity);
    }

    @Override
    public List<T> findBatchIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new ServiceException(ModelResult.CODE_500, "ids is null or empty while invoke findBatchIds() method");
        }

        List<E> entityList = dao.selectBatchIds(ids);
        return entityToDTOList(entityList);
    }

    @Override
    public List<T> findByMap(final Map<String, Object> params) {
        List<E> entityList = dao.selectByMap(params);
        return entityToDTOList(entityList);
    }

    @Override
    public T findOne(T dto) {
        if (Func.isEmpty(dto)) {
            throw new ServiceException(ModelResult.CODE_500, "dto is null or empty while invoke findOne() method");
        }

        E entity = dao.selectOne(dtoToEntity(dto));
        return entityToDTO(entity);
    }

    @Override
    public T findOne(final Map<String, Object> params) {
        if (Func.isEmpty(params)) {
            throw new ServiceException(ModelResult.CODE_500, "params is null or empty while invoke findOne() method");
        }

        E entity = dao.selectOne(mapToEntity(params));
        return entityToDTO(entity);
    }

    @Override
    public Integer findCount(T dto) {
        return dao.selectCount(new EntityWrapper<>(dtoToEntity(dto)));
    }

    @Override
    public Integer findCount(final Map<String, Object> params) {
        return dao.selectCount(new EntityWrapper<>(mapToEntity(params)));
    }

    @Override
    public Integer findCount(final Wrapper<E> wrapper) {
        return dao.selectCount(wrapper);
    }

    @Override
    public List<T> findList(T dto) {
        List<E> entityList = dao.selectList(new EntityWrapper<>(dtoToEntity(dto)));
        return entityToDTOList(entityList);
    }

    @Override
    public List<T> findList(final Map<String, Object> params) {
        List<E> entityList = dao.selectList(new EntityWrapper<>(mapToEntity(params)));
        return entityToDTOList(entityList);
    }

    @Override
    public List<T> findList(final Wrapper<E> wrapper) {
        List<E> entityList = dao.selectList(wrapper);
        return entityToDTOList(entityList);
    }

    @Override
    public List<Map<String, Object>> findMaps(T dto) {
        return dao.selectMaps(new EntityWrapper<>(dtoToEntity(dto)));
    }

    @Override
    public List<Map<String, Object>> findMaps(final Map<String, Object> params) {
        return dao.selectMaps(new EntityWrapper<>(mapToEntity(params)));
    }

    @Override
    public List<Map<String, Object>> findMaps(final Wrapper<E> wrapper) {
        return dao.selectMaps(wrapper);
    }

    @Override
    public List<Object> findObjs(T dto) {
        return dao.selectObjs(new EntityWrapper<>(dtoToEntity(dto)));
    }

    @Override
    public List<Object> findObjs(final Map<String, Object> params) {
        return dao.selectObjs(new EntityWrapper<>(mapToEntity(params)));
    }

    @Override
    public List<Object> findObjs(final Wrapper<E> wrapper) {
        return dao.selectObjs(wrapper);
    }

    @Override
    public Page<T> findPage(T dto, final Integer currentPage, final Integer limit) {
        Page<T> page = new Page<>(currentPage, limit);
        List<E> entityList = dao.selectPage(page, new EntityWrapper<>(dtoToEntity(dto)));

        page.setRecords(entityToDTOList(entityList));
        return page;
    }

    @Override
    public Page<T> findPage(final Map<String, Object> params, final Integer currentPage, final Integer limit) {
        Page<T> page = new Page<>(currentPage, limit);
        List<E> entityList = dao.selectPage(page, new EntityWrapper<>(mapToEntity(params)));

        page.setRecords(entityToDTOList(entityList));
        return page;
    }

    @Override
    public Page<T> findPage(final Wrapper<E> wrapper, final Integer currentPage, final Integer limit) {
        Page<T> page = new Page<>(currentPage, limit);
        List<E> entityList = dao.selectPage(page, wrapper);

        page.setRecords(entityToDTOList(entityList));
        return page;
    }

    @Override
    public Page<Map<String, Object>> findMapsPage(T dto, final Integer currentPage, final Integer limit) {
        Page<Map<String, Object>> page = new Page<>(currentPage, limit);
        List<Map<String, Object>> entityList = dao.selectMapsPage(page, new EntityWrapper<>(dtoToEntity(dto)));

        page.setRecords(entityList);
        return page;
    }

    @Override
    public Page<Map<String, Object>> findMapsPage(final Map<String, Object> params, final Integer currentPage, final Integer limit) {
        Page<Map<String, Object>> page = new Page<>(currentPage, limit);
        List<Map<String, Object>> entityList = dao.selectMapsPage(page, new EntityWrapper<>(mapToEntity(params)));

        page.setRecords(entityList);
        return page;
    }

    @Override
    public Page<Map<String, Object>> findMapsPage(final Wrapper<E> wrapper, final Integer currentPage, final Integer limit) {
        Page<Map<String, Object>> page = new Page<>(currentPage, limit);
        List<Map<String, Object>> entityList = dao.selectMapsPage(page, wrapper);

        page.setRecords(entityList);
        return page;
    }

    private E handleSaveInfo(E entity) {
        handleDelInfo(entity);
        handleTestInfo(entity);
        handleUpdateInfo(entity);
        handleCreateInfo(entity);
        return entity;
    }

    private Map<String, Object> toUnderlineMap(final Map<String, Object> params) {
        Map<String, Object> underLineMap = Maps.newHashMap();
        if (!Func.isEmpty(params)) {
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                underLineMap.put(StrKit.toUnderlineCase(entry.getKey()), entry.getValue());
            }
        }
        return underLineMap;
    }

    private Map<String, Object> toLowerFirstCamelMap(final Map<String, Object> params) {
        Map<String, Object> lowerFirstCamelMap = Maps.newHashMap();
        if (!Func.isEmpty(params)) {
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                lowerFirstCamelMap.put(StrKit.lowerFirst(StrKit.toCamelCase(entry.getKey())), entry.getValue());
            }
        }
        return lowerFirstCamelMap;
    }

    /**
     * 获取当前登录User信息
     *
     * @return
     */
    protected final CurrentUser getCurrentUser() {
        return ThreadLocalKit.getCurrentUser();
    }

    /**
     * 获取当前登录User设备信息
     *
     * @return
     */
    protected final Device getDevice() {
        return ThreadLocalKit.getDevice();
    }

    /**
     * 处理创建信息
     *
     * @param entity
     * @return
     */
    protected final E handleCreateInfo(E entity) {
        entity.setCreateAt(System.currentTimeMillis());
        if (getCurrentUser() != null) {
            entity.setCreateBy(getCurrentUser().getId());
            entity.setCreateName(getCurrentUser().getName());
        }
        return entity;
    }

    /**
     * 处理更新信息
     *
     * @param entity
     * @return
     */
    protected final E handleUpdateInfo(E entity) {
        entity.setUpdateAt(System.currentTimeMillis());
        if (getCurrentUser() != null) {
            entity.setUpdateBy(getCurrentUser().getId());
            entity.setUpdateName(getCurrentUser().getName());
        }
        return entity;
    }

    /**
     * 处理测试信息
     *
     * @param entity 实体
     * @return
     */
    protected final E handleTestInfo(E entity) {
        if (getCurrentUser() != null) {
            entity.setIsTest(getCurrentUser().getIsTest());
        }
        return entity;
    }

    /**
     * 处理删除信息
     *
     * @param entity 实体
     * @return
     */
    protected final E handleDelInfo(E entity) {
        if (!Func.isEmpty(entity)) {
            entity.setIsDel(0);
        }
        return entity;
    }

    protected abstract List<T> entityToDTOList(List<E> entityList);

    protected abstract T entityToDTO(E entity);

    protected abstract List<E> dtoToEntityList(List<T> dtoList);

    protected abstract E dtoToEntity(T dto);

    protected abstract E mapToEntity(Map<String, Object> map);

    protected abstract T mapToDto(Map<String, Object> map);
}
