package com.jeasy.base.manager;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;

import java.util.List;
import java.util.Map;

/**
 * BaseManager
 *
 * @param <E> Entity
 * @param <T> DTO
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
public interface BaseManager<E, T> {

    /**
     * <p>
     * 插入一条记录
     * </p>
     *
     * @param dto DTO对象
     * @return Boolean
     */
    Boolean save(T dto);

    Boolean save(Map<String, Object> dtoMap);

    /**
     * <p>
     * 插入一条记录(所有字段)
     * </p>
     *
     * @param dto DTO对象
     * @return Boolean
     */
    Boolean saveAllColumn(T dto);

    Boolean saveAllColumn(Map<String, Object> dtoMap);

    Boolean saveBatchAllColumn(List<T> dtos);

    /**
     * <p>
     * 根据 ID 删除
     * </p>
     *
     * @param id 主键ID
     * @return Boolean
     */
    Boolean removeById(Long id);

    /**
     * <p>
     * 根据 entity 条件，删除记录
     * </p>
     *
     * @param dto 实体对象
     * @return int
     */
    Boolean remove(T dto);

    Boolean remove(Map<String, Object> params);

    /**
     * <p>
     * 根据 entity 条件，删除记录
     * </p>
     *
     * @param wrapper 实体对象封装操作类（可以为 null）
     * @return int
     */
    Boolean removeByWrapper(Wrapper<E> wrapper);

    /**
     * <p>
     * 删除（根据ID 批量删除）
     * </p>
     *
     * @param ids 主键ID列表
     * @return int
     */
    Boolean removeBatchIds(List<Long> ids);

    /**
     * <p>
     * 根据 ID 修改
     * </p>
     *
     * @param dto 实体对象
     * @return int
     */
    Boolean modifyById(T dto);

    Boolean modifyById(Map<String, Object> map);

    /**
     * <p>
     * 根据 ID 修改
     * </p>
     *
     * @param dto 实体对象
     * @return int
     */
    Boolean modifyAllColumnById(T dto);

    Boolean modifyAllColumnById(Map<String, Object> map);

    /**
     * <p>
     * 根据 whereMap 条件，更新记录
     * </p>
     *
     * @param dto      实体对象
     * @param whereMap 条件Map
     * @return
     */
    Boolean modify(T dto, Map<String, Object> whereMap);

    /**
     * <p>
     * 根据 whereEntity 条件，更新记录
     * </p>
     *
     * @param dto     DTO
     * @param wrapper 实体对象封装操作类（可以为 null）
     * @return
     */
    Boolean modify(T dto, Wrapper<E> wrapper);

    /**
     * <p>
     * 根据 ID 查询
     * </p>
     *
     * @param id 主键ID
     * @return T
     */
    T findById(Long id);

    /**
     * <p>
     * 查询（根据ID 批量查询）
     * </p>
     *
     * @param ids 主键ID列表
     * @return List<T>
     */
    List<T> findBatchIds(List<Long> ids);

    /**
     * <p>
     * 查询（根据 columnMap 条件）
     * </p>
     *
     * @param params 参数对象
     * @return List<T>
     */
    List<T> findByMap(Map<String, Object> params);

    /**
     * <p>
     * 根据 entity 条件，查询一条记录
     * </p>
     *
     * @param dto 实体对象
     * @return T
     */
    T findOne(T dto);

    T findOne(Map<String, Object> params);

    /**
     * <p>
     * 根据 Wrapper 条件，查询总记录数
     * </p>
     *
     * @param dto 实体对象
     * @return int
     */
    Integer findCount(T dto);

    Integer findCount(Map<String, Object> params);

    /**
     * <p>
     * 根据 Wrapper 条件，查询总记录数
     * </p>
     *
     * @param wrapper 实体对象
     * @return int
     */
    Integer findCount(Wrapper<E> wrapper);

    /**
     * <p>
     * 根据 entity 条件，查询全部记录
     * </p>
     *
     * @param dto 实体对象
     * @return List<T>
     */
    List<T> findList(T dto);

    List<T> findList(Map<String, Object> params);

    /**
     * <p>
     * 根据 entity 条件，查询全部记录
     * </p>
     *
     * @param wrapper 实体对象封装操作类（可以为 null）
     * @return List<T>
     */
    List<T> findList(Wrapper<E> wrapper);

    /**
     * <p>
     * 根据 Wrapper 条件，查询全部记录
     * </p>
     *
     * @param dto 实体对象
     * @return List<T>
     */
    List<Map<String, Object>> findMaps(T dto);

    List<Map<String, Object>> findMaps(Map<String, Object> params);

    /**
     * <p>
     * 根据 Wrapper 条件，查询全部记录
     * </p>
     *
     * @param wrapper 实体对象封装操作类（可以为 null）
     * @return List<T>
     */
    List<Map<String, Object>> findMaps(Wrapper<E> wrapper);

    /**
     * <p>
     * 根据 Wrapper 条件，查询全部记录
     * </p>
     *
     * @param dto 实体对象
     * @return List<Object>
     */
    List<Object> findObjs(T dto);

    List<Object> findObjs(Map<String, Object> params);

    /**
     * <p>
     * 根据 Wrapper 条件，查询全部记录
     * </p>
     *
     * @param wrapper 实体对象封装操作类（可以为 null）
     * @return List<Object>
     */
    List<Object> findObjs(Wrapper<E> wrapper);

    /**
     * <p>
     * 根据 dto 条件，查询全部记录（并翻页）
     * </p>
     *
     * @param currentPage 当前页
     * @param limit  大小
     * @return List<T>
     */
    Page<T> findPage(T dto, Integer currentPage, Integer limit);


    Page<T> findPage(Map<String, Object> params, Integer currentPage, Integer limit);

    /**
     * <p>
     * 根据 entity 条件，查询全部记录（并翻页）
     * </p>
     *
     * @param wrapper 实体对象封装操作类（可以为 null）
     * @return List<T>
     */
    Page<T> findPage(Wrapper<E> wrapper, Integer currentPage, Integer limit);

    /**
     * <p>
     * 根据 dto 条件，查询全部记录（并翻页）
     * </p>
     *
     * @param currentPage 当前页
     * @param limit  大小
     * @return List<Map<String, Object>>
     */
    Page<Map<String, Object>> findMapsPage(T dto, Integer currentPage, Integer limit);

    Page<Map<String, Object>> findMapsPage(Map<String, Object> params, Integer currentPage, Integer limit);

    /**
     * <p>
     * 根据 Wrapper 条件，查询全部记录（并翻页）
     * </p>
     *
     * @param wrapper 实体对象封装操作类
     * @return List<Map<String, Object>>
     */
    Page<Map<String, Object>> findMapsPage(Wrapper<E> wrapper, Integer currentPage, Integer limit);
}
