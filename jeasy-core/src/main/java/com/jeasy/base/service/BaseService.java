package com.jeasy.base.service;

import com.baomidou.mybatisplus.plugins.Page;

import java.util.List;

/**
 * BaseService
 *
 * @param <T> DTO
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
public interface BaseService<T> {

    /**
     * 查询
     */
    List<T> find(T dto);

    /**
     * ID查询
     */
    T findById(Long id);

    /**
     * ID批量查询
     */
    List<T> findByIds(List<Long> ids);

    /**
     * 参数分页查询
     */
    Page<T> pagination(T dto, Integer currentPage, Integer limit);

    /**
     * First查询
     */
    T findOne(T dto);

    /**
     * 保存
     */
    Boolean save(T dto);

    /**
     * 选择保存
     */
    Boolean saveAllColumn(T dto);

    /**
     * 选择保存
     */
    Boolean saveBatchAllColumn(List<T> dtoList);

    /**
     * 修改
     */
    Boolean modify(T dto);

    /**
     * 选择修改
     */
    Boolean modifyAllColumn(T dto);

    /**
     * 删除
     */
    Boolean remove(Long id);

    /**
     * 批量删除
     */
    Boolean removeBatch(List<Long> ids);

    /**
     * 参数删除
     */
    Boolean removeByParams(T dto);
}
