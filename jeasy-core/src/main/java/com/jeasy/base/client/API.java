package com.jeasy.base.client;

import java.lang.annotation.*;

/**
 * @author taomk
 * @version 1.0
 * @since 15-8-25 上午11:58
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface API {
}
