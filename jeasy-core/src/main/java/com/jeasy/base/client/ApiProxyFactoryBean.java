package com.jeasy.base.client;

import com.google.common.collect.Maps;
import com.jeasy.base.web.dto.CurrentUser;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.date.DateKit;
import com.jeasy.common.json.JsonKit;
import com.jeasy.common.thread.ThreadLocalKit;
import com.jeasy.httphelper.exception.WSException;
import com.jeasy.httphelper.request.WSAnnotationHttpRequest;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.cglib.core.SpringNamingPolicy;
import org.springframework.cglib.proxy.*;

import java.lang.reflect.Method;
import java.util.Map;

@Data
@Slf4j
public class ApiProxyFactoryBean<T> implements FactoryBean<T> {

    private String innerClassName;

    public T getObject() throws Exception {
        Class innerClass = Class.forName(innerClassName);
        if (innerClass.isInterface()) {
            return (T) InterfaceProxy.newInstance(innerClass);
        } else {
            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(innerClass);
            enhancer.setNamingPolicy(SpringNamingPolicy.INSTANCE);
            enhancer.setCallback(new MethodInterceptorImpl());
            return (T) enhancer.create();
        }
    }

    public Class<?> getObjectType() {
        try {
            return innerClassName == null ? null : Class.forName(innerClassName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isSingleton() {
        return true;
    }

    static class InterfaceProxy implements InvocationHandler {
        public Object invoke(final Object target, final Method method, final Object[] args) throws Throwable {
            if (method.getReturnType().equals(ModelResult.class)) {
                return invokeByWSRequest(target, method, args);
            }
            return method.invoke(target, args);
        }

        public static <T> T newInstance(final Class<T> innerInterface) {
            ClassLoader classLoader = innerInterface.getClassLoader();
            Class[] interfaces = new Class[]{innerInterface};
            InterfaceProxy proxy = new InterfaceProxy();
            return (T) Proxy.newProxyInstance(classLoader, interfaces, proxy);
        }
    }

    static class MethodInterceptorImpl implements MethodInterceptor {
        public Object intercept(final Object target, final Method method, final Object[] args, final MethodProxy methodProxy) throws Throwable {
            if (method.getReturnType().equals(ModelResult.class)) {
                return invokeByWSRequest(target, method, args);
            }
            return methodProxy.invokeSuper(target, args);
        }
    }

    private static Object invokeByWSRequest(final Object target, final Method method, final Object[] args) throws WSException {
        String className = target.getClass().getName();
        String methodName = method.getName();

        StringBuilder logMsg = new StringBuilder("\nClient API execute report -------- " + DateKit.currentDatetime() + " -------------------------------------");
        logMsg.append("\nService   : ").append(className);
        logMsg.append("\nMethod    : ").append(methodName);

        long startTime = System.currentTimeMillis();
        try {
            // 仅支持@FromJson的参数
            Map<String, Object> params = Maps.newHashMap();
//            Parameter[] parameters = method.getParameters();
//            for (int i = 0; i < parameters.length; i++) {
//                FromJson fromJson = parameters[i].getAnnotation(FromJson.class);
//                if (args[i] == null) continue;
//
//                if (fromJson.key().equals("body")) {
//                    params.putAll(MapKit.toObjMap(args[i]));
//                } else {
//                    params.put(fromJson.key(), args[i]);
//                }
//            }

            ThreadLocalKit.putCurrentMethod(method);
            CurrentUser user = ThreadLocalKit.getCurrentUser();
            ((WSAnnotationHttpRequest) target).addParameter("user", JsonKit.toJson(user));
            ((WSAnnotationHttpRequest) target).addParameter("body", JsonKit.toJson(params));
            logMsg.append("\nParameter : ").append("body=" + JsonKit.toJson(params) + ", user=" + JsonKit.toJson(params));

            Object result = ((WSAnnotationHttpRequest) target).execute().getBody();
            logMsg.append("\nResult    : ").append(JsonKit.toJson(result));
            logMsg.append("\nCost Time : ").append(System.currentTimeMillis() - startTime).append(" ms");
            logMsg.append("\n--------------------------------------------------------------------------------------------");
            log.info(logMsg.toString());
            return result;
        } catch (Throwable e) {
//            MonitorUtils.incCountForException(className, methodName);
            log.error(className + "." + methodName + " Occur Exception : ", e);
            throw e;
        } finally {
            ThreadLocalKit.removeCurrentMethod();
        }
    }
}


