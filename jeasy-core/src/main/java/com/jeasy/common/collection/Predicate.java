package com.jeasy.common.collection;

public interface Predicate<T> {
    boolean evaluate(T o);
}
