package com.jeasy.common.file;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.model.*;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.prop.Prop;
import com.jeasy.common.prop.PropKit;
import com.jeasy.exception.MessageException;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by nick on 17/5/18.
 */
public class S3FileKit {

    public static String uploadFile(CommonsMultipartFile file, String key) {
        Protocol protocol = Protocol.HTTP;
        //取aws的必要参数
        Prop awsProps = PropKit.use("ws.properties");
        String accessKey = awsProps.get("aws.accessKey");
        String secretKey = awsProps.get("aws.secretKey");
        String endpoint = awsProps.get("aws.endpoint");
        String bucketName = awsProps.get("aws.bucketName");

        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setProtocol(protocol);
        AmazonS3 client = new AmazonS3Client(credentials, clientConfig);
        client.setEndpoint(endpoint);
        client.setS3ClientOptions(new S3ClientOptions().withPathStyleAccess(true));
        URL fileUrl = null;

        try {
            PutObjectResult result = client.putObject(bucketName, key, file.getInputStream(), null);
            S3Object s3Object = client.getObject(new GetObjectRequest(bucketName, key));
            //获取一个request
            GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(bucketName, key);
            Date expirationDate = new SimpleDateFormat("yyyy-MM-dd").parse("2099-12-31");
            urlRequest.setExpiration(expirationDate);
            fileUrl = client.generatePresignedUrl(urlRequest);
            if (fileUrl == null) {
                throw new MessageException(ModelResult.CODE_200, "获取文件URL失败，请重新上传！");
            }
            return fileUrl.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static InputStream downloadFile(String key) {
        Protocol protocol = Protocol.HTTP;
        //取aws的必要参数
        Prop awsProps = PropKit.use("ws.properties");
        String accessKey = awsProps.get("aws.accessKey");
        String secretKey = awsProps.get("aws.secretKey");
        String endpoint = awsProps.get("aws.endpoint");
        String bucketName = awsProps.get("aws.bucketName");

        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setProtocol(protocol);
        AmazonS3 client = new AmazonS3Client(credentials, clientConfig);
        client.setEndpoint(endpoint);
        client.setS3ClientOptions(new S3ClientOptions().withPathStyleAccess(true));

        GetObjectRequest rangeObjectRequest = new GetObjectRequest(bucketName, key);
        //rangeObjectRequest.setRange(0, 10); // retrieve 1st 11 bytes.
        S3Object objectPortion = client.getObject(rangeObjectRequest);

        InputStream objectData = objectPortion.getObjectContent();

        return objectData;
    }

}
