package com.jeasy.common.cache;

import org.springframework.cache.interceptor.KeyGenerator;

import java.lang.reflect.Method;

/**
 * 实现新的key generator
 */
public class StringKeyGenerator implements KeyGenerator {

    /**
     * 最大字符串长度
     */
    private static final int MAX_KEY_SIZE = 200;

    @Override
    public Object generate(final Object target, final Method method, final Object... params) {
        StringBuilder sb = new StringBuilder();
        // 增加方法名作为key，防止冲突
        sb.append(method.getName());
        for (Object o : params) {
            sb.append(o.toString());
            sb.append('.');
        }
        String newKey = sb.toString().replaceAll("\\p{Cntrl}]|\\p{Space}", "_");
        if (newKey.getBytes().length >= MAX_KEY_SIZE) {
            return newKey.substring(20, 60) + this.betterHashcode(newKey);
        } else {
            return newKey;
        }
    }

    public String betterHashcode(final String str) {
        long h = 0;
        int len = str.length();
        for (int i = 0; i < len; i++) {
            h = 257 * h + str.charAt(i);
        }
        return String.valueOf(h);
    }

}
