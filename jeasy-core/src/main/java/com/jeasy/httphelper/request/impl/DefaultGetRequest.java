package com.jeasy.httphelper.request.impl;

import com.jeasy.httphelper.annotation.WSRequest;
import com.jeasy.httphelper.exception.WSException;
import com.jeasy.httphelper.model.WSRequestContext;
import com.jeasy.httphelper.request.WSAnnotationHttpRequest;

@WSRequest(
        name = "默认GET请求", url = "{url}", method = WSRequest.MethodType.GET
)
public class DefaultGetRequest extends WSAnnotationHttpRequest {

    @Override
    public void init(WSRequestContext context) throws WSException {

    }
}
