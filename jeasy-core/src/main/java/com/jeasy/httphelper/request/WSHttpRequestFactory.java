package com.jeasy.httphelper.request;

import com.jeasy.httphelper.WSHttpHelperXmlConfig;
import com.jeasy.httphelper.exception.WSException;
import com.jeasy.httphelper.model.config.RequestConfigData;

/**
 * Created by Administrator on 15-12-25.
 */
public class WSHttpRequestFactory {
    public static WSHttpRequest getHttpRequest(String name) throws WSException {
        RequestConfigData requestConfigData = WSHttpHelperXmlConfig.getInstance().getRequestConfigData(name);
        WSXmlConfigHttpRequest request = new WSXmlConfigHttpRequest(requestConfigData);
        return request;
    }
}
