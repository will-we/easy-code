package com.jeasy.scheduler.core;

import java.util.List;


/**
 * 调度配置中心客户端接口，可以有基于数据库的实现，可以有基于ConfigServer的实现
 */
public interface IScheduleDataManager {

    /**
     * 发送心跳信息
     *
     * @param server
     * @throws Exception
     */
    boolean refreshScheduleServer(ScheduleServer server) throws Exception;

    /**
     * 注册服务器
     *
     * @param server
     * @throws Exception
     */
    void registerScheduleServer(ScheduleServer server) throws Exception;


    boolean isLeader(String uuid, List<String> serverList);

    void unRegisterScheduleServer(ScheduleServer server) throws Exception;

    void clearExpireScheduleServer() throws Exception;


    List<String> loadScheduleServerNames() throws Exception;

    void assignTask(String currentUuid, List<String> taskServerList) throws Exception;

    boolean isOwner(String name, String uuid) throws Exception;

    boolean isRunning(String name) throws Exception;

    void addTask(TaskDefine taskDefine) throws Exception;

    void updateTask(TaskDefine taskDefine) throws Exception;

    /**
     * addTask中存储的Key由对象本身的字符串组成，此方法实现重载
     *
     * @param targetBean
     * @param targetMethod
     * @throws Exception
     */
    @Deprecated
    void delTask(String targetBean, String targetMethod) throws Exception;

    void delTask(TaskDefine taskDefine) throws Exception;

    List<TaskDefine> selectTask() throws Exception;

    boolean checkLocalTask(String currentUuid) throws Exception;

    boolean isExistsTask(TaskDefine taskDefine) throws Exception;

    boolean saveRunningInfo(String name, String uuid, String msg) throws Exception;

    boolean saveRunningInfo(String name, String uuid) throws Exception;
}
