package com.jeasy.scheduler.core;

import lombok.Data;

@Data
public class Version {

    private static final String VERSION = "uncode-schedule-1.0.0";

    public static String getVersion() {
        return VERSION;
    }

    public static boolean isCompatible(final String dataVersion) {
        return VERSION.compareTo(dataVersion) >= 0;
    }
}
