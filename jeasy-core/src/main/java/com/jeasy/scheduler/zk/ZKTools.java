package com.jeasy.scheduler.zk;

import com.jeasy.common.Func;
import com.jeasy.common.str.StrKit;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Stat;

import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * zk工具类
 */
@Slf4j
@Data
public class ZKTools {

    public static void createPath(final ZooKeeper zk, final String path, final CreateMode createMode, final List<ACL> acl) throws Exception {
        String[] list = path.split(StrKit.SLASH);
        String zkPath = StrKit.EMPTY;
        for (String str : list) {
            if (StringUtils.isNotEmpty(str)) {
                zkPath = zkPath + StrKit.SLASH + str;
                if (Func.isEmpty(zk.exists(zkPath, false))) {
                    zk.create(zkPath, null, acl, createMode);
                }
            }
        }
    }

    public static void printTree(final ZooKeeper zk, final String path, final Writer writer, final String lineSplitChar) throws Exception {
        String[] list = getTree(zk, path);
        Stat stat = new Stat();
        for (String name : list) {
            byte[] value = zk.getData(name, false, stat);
            if (Func.isEmpty(value)) {
                writer.write(name + lineSplitChar);
            } else {
                writer.write(name + "[v." + stat.getVersion() + "]" + "[" + new String(value) + "]" + lineSplitChar);
            }
        }
    }

    public static void deleteTree(final ZooKeeper zk, final String path) throws Exception {
        String[] list = getTree(zk, path);
        for (int i = list.length - 1; i >= 0; i--) {
            zk.delete(list[i], -1);
        }
    }

    private static String[] getTree(final ZooKeeper zk, final String path) throws Exception {
        if (Func.isEmpty(zk.exists(path, false))) {
            return new String[0];
        }
        List<String> dealList = new ArrayList<>();
        dealList.add(path);
        int index = 0;
        while (index < dealList.size()) {
            String tempPath = dealList.get(index);
            List<String> children = zk.getChildren(tempPath, false);
            if (!tempPath.equalsIgnoreCase(StrKit.SLASH)) {
                tempPath = tempPath + StrKit.SLASH;
            }
            Collections.sort(children);
            for (int i = children.size() - 1; i >= 0; i--) {
                dealList.add(index + 1, tempPath + children.get(i));
            }
            index++;
        }
        return dealList.toArray(new String[dealList.size()]);
    }
}
