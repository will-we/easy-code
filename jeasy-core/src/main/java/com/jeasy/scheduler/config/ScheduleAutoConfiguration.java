package com.jeasy.scheduler.config;

import com.jeasy.common.Func;
import com.jeasy.scheduler.ZKScheduleManager;
import com.jeasy.scheduler.quartz.MethodInvokingJobDetailFactoryBean;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Data
public class ScheduleAutoConfiguration {

    @Autowired
    private ScheduleConfig uncodeScheduleConfig;

    public ZKScheduleManager commonMapper() {
        ZKScheduleManager zkScheduleManager = new ZKScheduleManager();
        zkScheduleManager.setZkConfig(uncodeScheduleConfig.getConfig());
        log.info("=====>ZKScheduleManager inited..");
        return zkScheduleManager;
    }

    public SchedulerFactoryBean quartzSchedulerFactoryBean() {
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        if (Func.isNotEmpty(uncodeScheduleConfig.getQuartzBean())
            && Func.isNotEmpty(uncodeScheduleConfig.getQuartzMethod())
            && Func.isNotEmpty(uncodeScheduleConfig.getQuartzCronExpression())) {
            int len = uncodeScheduleConfig.getQuartzBean().size();
            List<Trigger> triggers = new ArrayList<Trigger>();
            for (int i = 0; i < len; i++) {
                String name = uncodeScheduleConfig.getQuartzBean().get(i);
                String method = uncodeScheduleConfig.getQuartzMethod().get(i);
                String cronExpression = uncodeScheduleConfig.getQuartzCronExpression().get(i);
                if (StringUtils.isNotBlank(name) && StringUtils.isNotBlank(method) && StringUtils.isNotBlank(cronExpression)) {
                    MethodInvokingJobDetailFactoryBean methodInvokingJob = new MethodInvokingJobDetailFactoryBean();
                    methodInvokingJob.setTargetBeanName(name);
                    methodInvokingJob.setTargetMethod(method);
                    CronTriggerFactoryBean cronTrigger = new CronTriggerFactoryBean();
                    cronTrigger.setJobDetail(methodInvokingJob.getObject());
                    triggers.add(cronTrigger.getObject());
                }
            }
            if (Func.isNotEmpty(triggers)) {
                schedulerFactoryBean.setTriggers((Trigger[]) triggers.toArray());
            }
        }
        log.info("=====>QuartzSchedulerFactoryBean inited..");
        return schedulerFactoryBean;
    }
}
