package ${conf.basePackage}.${table.lowerCamelName}.controller;

import com.baomidou.mybatisplus.plugins.Page;
import ${conf.basePackage}.base.web.controller.BaseController;
import ${conf.basePackage}.base.web.dto.ModelResult;
import ${conf.basePackage}.base.web.resolver.FromJson;
import ${conf.basePackage}.common.Func;
import ${conf.basePackage}.doc.annotation.InitField;
import ${conf.basePackage}.doc.annotation.MethodDoc;
import ${conf.basePackage}.doc.annotation.StatusEnum;
import ${conf.basePackage}.${table.lowerCamelName}.dto.*;
import ${conf.basePackage}.${table.lowerCamelName}.service.${table.className}Service;
import ${conf.basePackage}.validate.handler.ValidateNotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * ${table.comment} Controller
 *
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@Slf4j
@Controller
public class ${table.className}Controller extends BaseController<${table.className}Service> {

    @MethodDoc(lists = ${table.className}ListResDTO.class, desc = {"PC端", "${table.comment}-列表", "列表"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/list", method = {RequestMethod.GET})
    @ResponseBody
    public void list(final @FromJson ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        List<${table.className}ListResDTO> items = service.list(${table.lowerCamelName}ListReqDTO);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(lists = ${table.className}ListResDTO.class, desc = {"PC端", "${table.comment}-列表", "列表1.1.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/list", headers = {"version=1.1.0"}, method = {RequestMethod.GET})
    @ResponseBody
    public void list1_1_0(final @FromJson ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        List<${table.className}ListResDTO> items = service.list1_1_0(${table.lowerCamelName}ListReqDTO);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(lists = ${table.className}ListResDTO.class, desc = {"PC端", "${table.comment}-列表", "列表1.2.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/list", headers = {"version=1.2.0", "platform=APP"}, method = {RequestMethod.GET})
    @ResponseBody
    public void list1_2_0(final @FromJson ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        List<${table.className}ListResDTO> items = service.list1_2_0(${table.lowerCamelName}ListReqDTO);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(lists = ${table.className}ListResDTO.class, desc = {"PC端", "${table.comment}-列表", "列表1.3.0"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/list", headers = {"version=1.3.0", "platform=APP", "device=IOS"}, method = {RequestMethod.GET})
    @ResponseBody
    public void list1_3_0(final @FromJson ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        List<${table.className}ListResDTO> items = service.list1_3_0(${table.lowerCamelName}ListReqDTO);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(entity = ${table.className}ListResDTO.class, desc = {"PC端", "${table.comment}-列表", "First查询"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/listOne", method = {RequestMethod.GET})
    @ResponseBody
    public void listOne(final @FromJson ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        ${table.className}ListResDTO entity = service.listOne(${table.lowerCamelName}ListReqDTO);
        responseEntity(ModelResult.CODE_200, ModelResult.SUCCESS, entity);
    }

    @MethodDoc(pages = ${table.className}PageResDTO.class, desc = {"PC端", "${table.comment}-分页", "分页"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/page", method = {RequestMethod.GET})
    @ResponseBody
    public void page(final @FromJson ${table.className}PageReqDTO ${table.lowerCamelName}PageReqDTO,
                     final @InitField(name = "pageNo", value = "1", desc = "当前页码") @FromJson Integer pageNo,
                     final @InitField(name = "pageSize", value = "10", desc = "每页大小") @FromJson Integer pageSize) {
        int currentPage = Func.isNullOrZero(pageNo) ? 1 : pageNo;
        int pageLimit = Func.isNullOrZero(pageSize) ? 10 : pageSize;

        Page<${table.className}PageResDTO> ${table.lowerCamelName}Page = service.pagination(${table.lowerCamelName}PageReqDTO, currentPage, pageLimit);
        responsePage(ModelResult.CODE_200, ModelResult.SUCCESS, ${table.lowerCamelName}Page.getTotal(), ${table.lowerCamelName}Page.getRecords(), pageLimit, currentPage);
    }

    @MethodDoc(desc = {"PC端", "${table.comment}-新增", "新增"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/add", method = {RequestMethod.POST})
    @ResponseBody
    public void add(final @FromJson ${table.className}AddReqDTO ${table.lowerCamelName}AddReqDTO) {
        Boolean isSuccess = service.add(${table.lowerCamelName}AddReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "${table.comment}-新增", "新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/addAllColumn", method = {RequestMethod.POST})
    @ResponseBody
    public void addAllColumn(final @FromJson ${table.className}AddReqDTO ${table.lowerCamelName}AddReqDTO) {
        Boolean isSuccess = service.addAllColumn(${table.lowerCamelName}AddReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "${table.comment}-新增", "批量新增(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/addBatchAllColumn", method = {RequestMethod.POST})
    @ResponseBody
    public void addBatchAllColumn(final @FromJson List<${table.className}AddReqDTO> ${table.lowerCamelName}AddReqDTOList) {
        Boolean isSuccess = service.addBatchAllColumn(${table.lowerCamelName}AddReqDTOList);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(entity = ${table.className}ShowResDTO.class, desc = {"PC端", "${table.comment}-详情", "详情"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/show", method = {RequestMethod.GET})
    @ResponseBody
    public void show(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") Long id) {
        ${table.className}ShowResDTO entity = service.show(id);
        responseEntity(ModelResult.CODE_200, ModelResult.SUCCESS, entity);
    }

    @MethodDoc(entity = ${table.className}ShowResDTO.class, desc = {"PC端", "${table.comment}-详情", "详情(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/showByIds", method = {RequestMethod.GET})
    @ResponseBody
    public void showByIds(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<Long> ids) {
        List<${table.className}ShowResDTO> items = service.showByIds(ids);
        responseList(ModelResult.CODE_200, ModelResult.SUCCESS, items);
    }

    @MethodDoc(desc = {"PC端", "${table.comment}-更新", "更新"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/modify", method = {RequestMethod.POST})
    @ResponseBody
    public void modify(final @FromJson ${table.className}ModifyReqDTO ${table.lowerCamelName}ModifyReqDTO) {
        Boolean isSuccess = service.modify(${table.lowerCamelName}ModifyReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "${table.comment}-更新", "更新(所有字段)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/modifySelective", method = {RequestMethod.POST})
    @ResponseBody
    public void modifyAllColumn(final @FromJson ${table.className}ModifyReqDTO ${table.lowerCamelName}ModifyReqDTO) {
        Boolean isSuccess = service.modifyAllColumn(${table.lowerCamelName}ModifyReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "${table.comment}-删除", "删除"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/remove", method = {RequestMethod.POST})
    @ResponseBody
    public void remove(final @InitField(name = "id", value = "10", desc = "主键ID") @FromJson @ValidateNotNull(message = "ID不允许为空") Long id) {
        Boolean isSuccess = service.remove(id);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "${table.comment}-删除", "删除(批量)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/removeBatch", method = {RequestMethod.POST})
    @ResponseBody
    public void removeBatch(final @InitField(name = "ids", value = "[\"1001\",\"1002\",\"1003\",\"1004\",\"1005\"]", desc = "主键ID集合") @FromJson List<Long> ids) {
        Boolean isSuccess = service.removeBatch(ids);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }

    @MethodDoc(desc = {"PC端", "${table.comment}-删除", "删除(参数)"}, status = StatusEnum.DONE, author = "taomk", finishTime = "2017/05/20 11:22")
    @RequestMapping(value = "${table.lowerCamelName}/removeByParams", method = {RequestMethod.POST})
    @ResponseBody
    public void removeByParams(final @FromJson ${table.className}RemoveReqDTO ${table.lowerCamelName}RemoveReqDTO) {
        Boolean isSuccess = service.removeByParams(${table.lowerCamelName}RemoveReqDTO);
        responseMessage(isSuccess ? ModelResult.CODE_200 : ModelResult.CODE_500, isSuccess ? ModelResult.SUCCESS : ModelResult.FAIL);
    }
}
