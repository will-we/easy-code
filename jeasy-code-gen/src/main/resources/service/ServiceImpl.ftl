package ${conf.basePackage}.${table.lowerCamelName}.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import ${conf.basePackage}.base.service.impl.BaseServiceImpl;
import ${conf.basePackage}.${table.lowerCamelName}.dto.*;
import ${conf.basePackage}.${table.lowerCamelName}.entity.${table.className}Entity;
import ${conf.basePackage}.${table.lowerCamelName}.manager.${table.className}Manager;
import ${conf.basePackage}.${table.lowerCamelName}.service.${table.className}Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ${table.comment} ServiceImpl
 *
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@Slf4j
@Service
public class ${table.className}ServiceImpl extends BaseServiceImpl<${table.className}Manager, ${table.className}Entity, ${table.className}DTO> implements ${table.className}Service {

    @Override
    public List<${table.className}ListResDTO> list(final ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        return manager.list(${table.lowerCamelName}ListReqDTO);
    }

    @Override
    public List<${table.className}ListResDTO> list1_1_0(final ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        return manager.list1_1_0(${table.lowerCamelName}ListReqDTO);
    }

    @Override
    public List<${table.className}ListResDTO> list1_2_0(final ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        return manager.list1_2_0(${table.lowerCamelName}ListReqDTO);
    }

    @Override
    public List<${table.className}ListResDTO> list1_3_0(final ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        return manager.list1_3_0(${table.lowerCamelName}ListReqDTO);
    }

    @Override
    public ${table.className}ListResDTO listOne(final ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        return manager.listOne(${table.lowerCamelName}ListReqDTO);
    }

    @Override
    public Page<${table.className}PageResDTO> pagination(final ${table.className}PageReqDTO ${table.lowerCamelName}PageReqDTO, final Integer currentPage, final Integer pageSize) {
        return manager.pagination(${table.lowerCamelName}PageReqDTO, currentPage, pageSize);
    }

    @Override
    public Boolean add(final ${table.className}AddReqDTO ${table.lowerCamelName}AddReqDTO) {
        return manager.add(${table.lowerCamelName}AddReqDTO);
    }

    @Override
    public Boolean addAllColumn(final ${table.className}AddReqDTO ${table.lowerCamelName}AddReqDTO) {
        return manager.addAllColumn(${table.lowerCamelName}AddReqDTO);
    }

    @Override
    public Boolean addBatchAllColumn(final List<${table.className}AddReqDTO> ${table.lowerCamelName}AddReqDTOList) {
        return manager.addBatchAllColumn(${table.lowerCamelName}AddReqDTOList);
    }

    @Override
    public ${table.className}ShowResDTO show(final Long id) {
        return manager.show(id);
    }

    @Override
    public List<${table.className}ShowResDTO> showByIds(final List<Long> ids) {
        return manager.showByIds(ids);
    }

    @Override
    public Boolean modify(final ${table.className}ModifyReqDTO ${table.lowerCamelName}ModifyReqDTO) {
        return manager.modify(${table.lowerCamelName}ModifyReqDTO);
    }

    @Override
    public Boolean modifyAllColumn(final ${table.className}ModifyReqDTO ${table.lowerCamelName}ModifyReqDTO) {
        return manager.modifyAllColumn(${table.lowerCamelName}ModifyReqDTO);
    }

    @Override
    public Boolean removeByParams(final ${table.className}RemoveReqDTO ${table.lowerCamelName}RemoveReqDTO) {
        return manager.removeByParams(${table.lowerCamelName}RemoveReqDTO);
    }
}
