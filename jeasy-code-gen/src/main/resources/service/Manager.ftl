package ${conf.basePackage}.${table.lowerCamelName}.manager;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import ${conf.basePackage}.base.manager.impl.BaseManagerImpl;
import ${conf.basePackage}.base.web.dto.ModelResult;
import ${conf.basePackage}.common.Func;
import ${conf.basePackage}.common.object.BeanKit;
import ${conf.basePackage}.common.object.Converter;
import ${conf.basePackage}.common.object.MapKit;
import ${conf.basePackage}.common.spring.SpringContextHolder;
import ${conf.basePackage}.exception.MessageException;
import ${conf.basePackage}.${table.lowerCamelName}.dao.${table.className}DAO;
import ${conf.basePackage}.${table.lowerCamelName}.dto.*;
import ${conf.basePackage}.${table.lowerCamelName}.entity.${table.className}Entity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * ${table.comment} Manager
 *
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@Slf4j
@Component
public class ${table.className}Manager extends BaseManagerImpl<${table.className}DAO, ${table.className}Entity, ${table.className}DTO> {

    /**
     * this is a converter demo only for BeanKit.copyProperties
     *
     * @see BeanKit#copyProperties(Object source, Object target, Converter... converters)
     */
    private static final Converter<String, String> DEMO_CONVERTER = new Converter<String, String>("filed1", "filed2") {
        @Override
        public String convert(final String val) {
            return val;
        }
    };

    public static ${table.className}Manager me() {
        return SpringContextHolder.getBean(${table.className}Manager.class);
    }

    public List<${table.className}ListResDTO> list(final ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        ${table.className}DTO ${table.lowerCamelName}ParamsDTO = new ${table.className}DTO();
        if (!Func.isEmpty(${table.lowerCamelName}ListReqDTO)) {
            BeanKit.copyProperties(${table.lowerCamelName}ListReqDTO, ${table.lowerCamelName}ParamsDTO, DEMO_CONVERTER);
        }

        List<${table.className}DTO> ${table.lowerCamelName}DtoList = super.findList(${table.lowerCamelName}ParamsDTO);

        if (!Func.isEmpty(${table.lowerCamelName}DtoList)) {
            List<${table.className}ListResDTO> items = Lists.newArrayList();
            for (${table.className}DTO ${table.lowerCamelName}Dto : ${table.lowerCamelName}DtoList) {
                ${table.className}ListResDTO ${table.lowerCamelName}ListResDTO = new ${table.className}ListResDTO();
                BeanKit.copyProperties(${table.lowerCamelName}Dto, ${table.lowerCamelName}ListResDTO, DEMO_CONVERTER);
                items.add(${table.lowerCamelName}ListResDTO);
            }
            return items;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<${table.className}ListResDTO> list1_1_0(final ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        return list(${table.lowerCamelName}ListReqDTO);
    }

    public List<${table.className}ListResDTO> list1_2_0(final ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        return list(${table.lowerCamelName}ListReqDTO);
    }

    public List<${table.className}ListResDTO> list1_3_0(final ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        return list(${table.lowerCamelName}ListReqDTO);
    }

    public ${table.className}ListResDTO listOne(final ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO) {
        ${table.className}DTO ${table.lowerCamelName}ParamsDTO = new ${table.className}DTO();
        if (!Func.isEmpty(${table.lowerCamelName}ListReqDTO)) {
            BeanKit.copyProperties(${table.lowerCamelName}ListReqDTO, ${table.lowerCamelName}ParamsDTO, DEMO_CONVERTER);
        }

        ${table.className}DTO ${table.lowerCamelName}Dto = super.findOne(${table.lowerCamelName}ParamsDTO);
        if (!Func.isEmpty(${table.lowerCamelName}Dto)) {
            ${table.className}ListResDTO ${table.lowerCamelName}ListResDTO = new ${table.className}ListResDTO();
            BeanKit.copyProperties(${table.lowerCamelName}Dto, ${table.lowerCamelName}ListResDTO, DEMO_CONVERTER);
            return ${table.lowerCamelName}ListResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Page<${table.className}PageResDTO> pagination(final ${table.className}PageReqDTO ${table.lowerCamelName}PageReqDTO, final Integer currentPage, final Integer pageSize) {
        ${table.className}DTO ${table.lowerCamelName}ParamsDTO = new ${table.className}DTO();
        if (!Func.isEmpty(${table.lowerCamelName}PageReqDTO)) {
            BeanKit.copyProperties(${table.lowerCamelName}PageReqDTO, ${table.lowerCamelName}ParamsDTO, DEMO_CONVERTER);
        }

        Page<${table.className}DTO> ${table.lowerCamelName}DTOPage = super.findPage(${table.lowerCamelName}ParamsDTO, currentPage, pageSize);

        if (Func.isNotEmpty(${table.lowerCamelName}DTOPage) && Func.isNotEmpty(${table.lowerCamelName}DTOPage.getRecords())) {
            List<${table.className}PageResDTO> ${table.lowerCamelName}PageResDTOs = Lists.newArrayList();
            for (${table.className}DTO ${table.lowerCamelName}Dto : ${table.lowerCamelName}DTOPage.getRecords()) {
                ${table.className}PageResDTO ${table.lowerCamelName}PageResDTO = new ${table.className}PageResDTO();
                BeanKit.copyProperties(${table.lowerCamelName}Dto, ${table.lowerCamelName}PageResDTO, DEMO_CONVERTER);
                ${table.lowerCamelName}PageResDTOs.add(${table.lowerCamelName}PageResDTO);
            }

            Page<${table.className}PageResDTO> ${table.lowerCamelName}PageResDTOPage = new Page<>();
            ${table.lowerCamelName}PageResDTOPage.setRecords(${table.lowerCamelName}PageResDTOs);
            ${table.lowerCamelName}PageResDTOPage.setTotal(${table.lowerCamelName}DTOPage.getTotal());
            return ${table.lowerCamelName}PageResDTOPage;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean add(final ${table.className}AddReqDTO ${table.lowerCamelName}AddReqDTO) {
        if (Func.isEmpty(${table.lowerCamelName}AddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        ${table.className}DTO ${table.lowerCamelName}Dto = new ${table.className}DTO();
        BeanKit.copyProperties(${table.lowerCamelName}AddReqDTO, ${table.lowerCamelName}Dto, DEMO_CONVERTER);
        return super.save(${table.lowerCamelName}Dto);
    }

    public Boolean addAllColumn(final ${table.className}AddReqDTO ${table.lowerCamelName}AddReqDTO) {
        if (Func.isEmpty(${table.lowerCamelName}AddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        ${table.className}DTO ${table.lowerCamelName}Dto = new ${table.className}DTO();
        BeanKit.copyProperties(${table.lowerCamelName}AddReqDTO, ${table.lowerCamelName}Dto, DEMO_CONVERTER);
        return super.saveAllColumn(${table.lowerCamelName}Dto);
    }

    public Boolean addBatchAllColumn(final List<${table.className}AddReqDTO> ${table.lowerCamelName}AddReqDTOList) {
        if (Func.isEmpty(${table.lowerCamelName}AddReqDTOList)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        List<${table.className}DTO> ${table.lowerCamelName}DTOList = Lists.newArrayList();
        for (${table.className}AddReqDTO ${table.lowerCamelName}AddReqDTO : ${table.lowerCamelName}AddReqDTOList) {
            ${table.className}DTO ${table.lowerCamelName}Dto = new ${table.className}DTO();
            BeanKit.copyProperties(${table.lowerCamelName}AddReqDTO, ${table.lowerCamelName}Dto, DEMO_CONVERTER);
            ${table.lowerCamelName}DTOList.add(${table.lowerCamelName}Dto);
        }
        return super.saveBatchAllColumn(${table.lowerCamelName}DTOList);
    }

    public ${table.className}ShowResDTO show(final Long id) {
        ${table.className}DTO ${table.lowerCamelName}Dto = super.findById(id);

        if (!Func.isEmpty(${table.lowerCamelName}Dto)) {
            ${table.className}ShowResDTO ${table.lowerCamelName}ShowResDTO = new ${table.className}ShowResDTO();
            BeanKit.copyProperties(${table.lowerCamelName}Dto, ${table.lowerCamelName}ShowResDTO, DEMO_CONVERTER);
            return ${table.lowerCamelName}ShowResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<${table.className}ShowResDTO> showByIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(ModelResult.CODE_200, "集合不能为空且大小大于0");
        }

        List<${table.className}DTO> ${table.lowerCamelName}DtoList = super.findBatchIds(ids);

        if (!Func.isEmpty(${table.lowerCamelName}DtoList)) {
            List<${table.className}ShowResDTO> ${table.lowerCamelName}ShowResDTOList = Lists.newArrayList();
            for (${table.className}DTO ${table.lowerCamelName}Dto : ${table.lowerCamelName}DtoList) {
                ${table.className}ShowResDTO ${table.lowerCamelName}ShowResDTO = new ${table.className}ShowResDTO();
                BeanKit.copyProperties(${table.lowerCamelName}Dto, ${table.lowerCamelName}ShowResDTO, DEMO_CONVERTER);
                ${table.lowerCamelName}ShowResDTOList.add(${table.lowerCamelName}ShowResDTO);
            }
            return ${table.lowerCamelName}ShowResDTOList;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean modify(final ${table.className}ModifyReqDTO ${table.lowerCamelName}ModifyReqDTO) {
        if (Func.isEmpty(${table.lowerCamelName}ModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        ${table.className}DTO ${table.lowerCamelName}Dto = new ${table.className}DTO();
        BeanKit.copyProperties(${table.lowerCamelName}ModifyReqDTO, ${table.lowerCamelName}Dto, DEMO_CONVERTER);
        return super.modifyById(${table.lowerCamelName}Dto);
    }

    public Boolean modifyAllColumn(final ${table.className}ModifyReqDTO ${table.lowerCamelName}ModifyReqDTO) {
        if (Func.isEmpty(${table.lowerCamelName}ModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        ${table.className}DTO ${table.lowerCamelName}Dto = new ${table.className}DTO();
        BeanKit.copyProperties(${table.lowerCamelName}ModifyReqDTO, ${table.lowerCamelName}Dto, DEMO_CONVERTER);
        return super.modifyAllColumnById(${table.lowerCamelName}Dto);
    }

    public Boolean removeByParams(final ${table.className}RemoveReqDTO ${table.lowerCamelName}RemoveReqDTO) {
        if (Func.isEmpty(${table.lowerCamelName}RemoveReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        ${table.className}DTO ${table.lowerCamelName}ParamsDTO = new ${table.className}DTO();
        BeanKit.copyProperties(${table.lowerCamelName}RemoveReqDTO, ${table.lowerCamelName}ParamsDTO, DEMO_CONVERTER);
        return super.remove(${table.lowerCamelName}ParamsDTO);
    }


    @Override
    protected List<${table.className}DTO> entityToDTOList(final List<${table.className}Entity> ${table.lowerCamelName}EntityList) {
        List<${table.className}DTO> ${table.lowerCamelName}DtoList = null;
        if (!Func.isEmpty(${table.lowerCamelName}EntityList)) {
            ${table.lowerCamelName}DtoList = Lists.newArrayList();
            for (${table.className}Entity ${table.lowerCamelName}Entity : ${table.lowerCamelName}EntityList) {
                ${table.lowerCamelName}DtoList.add(entityToDTO(${table.lowerCamelName}Entity));
            }
        }
        return ${table.lowerCamelName}DtoList;
    }

    @Override
    protected ${table.className}DTO entityToDTO(final ${table.className}Entity ${table.lowerCamelName}Entity) {
        ${table.className}DTO ${table.lowerCamelName}Dto = null;
        if (!Func.isEmpty(${table.lowerCamelName}Entity)) {
            ${table.lowerCamelName}Dto = new ${table.className}DTO();
            BeanKit.copyProperties(${table.lowerCamelName}Entity, ${table.lowerCamelName}Dto);
        }
        return ${table.lowerCamelName}Dto;
    }

    @Override
    protected List<${table.className}Entity> dtoToEntityList(final List<${table.className}DTO> ${table.lowerCamelName}DtoList) {
        List<${table.className}Entity> ${table.lowerCamelName}EntityList = null;
        if (!Func.isEmpty(${table.lowerCamelName}DtoList)) {
            ${table.lowerCamelName}EntityList = Lists.newArrayList();
            for (${table.className}DTO ${table.lowerCamelName}Dto : ${table.lowerCamelName}DtoList) {
                ${table.lowerCamelName}EntityList.add(dtoToEntity(${table.lowerCamelName}Dto));
            }
        }
        return ${table.lowerCamelName}EntityList;
    }

    @Override
    protected ${table.className}Entity dtoToEntity(final ${table.className}DTO ${table.lowerCamelName}Dto) {
        ${table.className}Entity ${table.lowerCamelName}Entity = null;
        if (!Func.isEmpty(${table.lowerCamelName}Dto)) {
            ${table.lowerCamelName}Entity = new ${table.className}Entity();
            BeanKit.copyProperties(${table.lowerCamelName}Dto, ${table.lowerCamelName}Entity);
        }
        return ${table.lowerCamelName}Entity;
    }

    @Override
    protected ${table.className}Entity mapToEntity(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new ${table.className}Entity();
        }
        return (${table.className}Entity) MapKit.toBean(map, ${table.className}Entity.class);
    }

    @Override
    protected ${table.className}DTO mapToDto(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new ${table.className}DTO();
        }
        return (${table.className}DTO) MapKit.toBean(map, ${table.className}DTO.class);
    }
}
