package ${conf.basePackage}.${table.lowerCamelName}.service;

import com.baomidou.mybatisplus.plugins.Page;
import ${conf.basePackage}.base.service.BaseService;
import ${conf.basePackage}.${table.lowerCamelName}.dto.*;

import java.util.List;

/**
 * ${table.comment} Service
 *
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
public interface ${table.className}Service extends BaseService<${table.className}DTO> {

    /**
     * 列表
     *
     * @param ${table.lowerCamelName}ListReqDTO 入参DTO
     * @return
     */
    List<${table.className}ListResDTO> list(${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO);

    List<${table.className}ListResDTO> list1_1_0(${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO);

    List<${table.className}ListResDTO> list1_2_0(${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO);

    List<${table.className}ListResDTO> list1_3_0(${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO);

    /**
     * First查询
     *
     * @param ${table.lowerCamelName}ListReqDTO 入参DTO
     * @return
     */
    ${table.className}ListResDTO listOne(${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO);

    /**
     * 分页
     *
     * @param ${table.lowerCamelName}PageReqDTO 入参DTO
     * @param currentPage 当前页
     * @param pageSize   每页大小
     * @return
     */
    Page<${table.className}PageResDTO> pagination(${table.className}PageReqDTO ${table.lowerCamelName}PageReqDTO, Integer currentPage, Integer pageSize);

    /**
     * 新增
     *
     * @param ${table.lowerCamelName}AddReqDTO 入参DTO
     * @return
     */
    Boolean add(${table.className}AddReqDTO ${table.lowerCamelName}AddReqDTO);

    /**
     * 新增(所有字段)
     *
     * @param ${table.lowerCamelName}AddReqDTO 入参DTO
     * @return
     */
    Boolean addAllColumn(${table.className}AddReqDTO ${table.lowerCamelName}AddReqDTO);

    /**
     * 批量新增(所有字段)
     *
     * @param ${table.lowerCamelName}AddReqDTOList 入参DTO
     * @return
     */
    Boolean addBatchAllColumn(List<${table.className}AddReqDTO> ${table.lowerCamelName}AddReqDTOList);

    /**
     * 详情
     *
     * @param id 主键ID
     * @return
     */
    ${table.className}ShowResDTO show(Long id);

    /**
     * 批量详情
     *
     * @param ids 主键IDs
     * @return
     */
    List<${table.className}ShowResDTO> showByIds(List<Long> ids);

    /**
     * 修改
     *
     * @param ${table.lowerCamelName}ModifyReqDTO 入参DTO
     * @return
     */
    Boolean modify(${table.className}ModifyReqDTO ${table.lowerCamelName}ModifyReqDTO);

    /**
     * 修改(所有字段)
     *
     * @param ${table.lowerCamelName}ModifyReqDTO 入参DTO
     * @return
     */
    Boolean modifyAllColumn(${table.className}ModifyReqDTO ${table.lowerCamelName}ModifyReqDTO);

    /**
     * 参数删除
     *
     * @param ${table.lowerCamelName}RemoveReqDTO 入参DTO
     * @return
     */
    Boolean removeByParams(${table.className}RemoveReqDTO ${table.lowerCamelName}RemoveReqDTO);
}
