package ${conf.basePackage}.${table.lowerCamelName}.entity;

<#list table.columns as col>
<#if (col.classImport != "")>
import ${col.classImport};
</#if>
</#list>
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import ${conf.basePackage}.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * ${table.comment}
 *
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@Data
@TableName("${table.name}")
public class ${table.className}Entity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    <#list table.columns as col>
    <#if col.camelName != "id" && col.camelName != "isDel" && col.camelName != "isTest" && col.camelName != "createAt" && col.camelName != "createBy" && col.camelName != "createName" && col.camelName != "updateAt" && col.camelName != "updateBy" && col.camelName != "updateName">
    public static final String DB_COL_${col.underLineUpperName} = "${col.name}";

    </#if>
    </#list>

    <#list table.columns as col>
    <#if col.camelName != "id" && col.camelName != "isDel" && col.camelName != "isTest" && col.camelName != "createAt" && col.camelName != "createBy" && col.camelName != "createName" && col.camelName != "updateAt" && col.camelName != "updateBy" && col.camelName != "updateName">
    /**
     * ${col.comment}
     */
    @TableField("${col.name}")
    private ${col.javaType} ${col.camelName};

    </#if>
    </#list>
}
