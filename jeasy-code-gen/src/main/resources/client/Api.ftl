package ${conf.basePackage}.${table.lowerCamelName}.api;

import ${conf.basePackage}.base.client.API;
import ${conf.basePackage}.base.web.dto.ModelResult;
import ${conf.basePackage}.base.web.resolver.FromJson;
import ${conf.basePackage}.common.charset.CharsetKit;
import ${conf.basePackage}.httphelper.annotation.Header;
import ${conf.basePackage}.httphelper.annotation.WSRequest;
import ${conf.basePackage}.httphelper.exception.WSException;
import ${conf.basePackage}.httphelper.model.WSRequestContext;
import ${conf.basePackage}.httphelper.request.WSAnnotationHttpRequest;
import ${conf.basePackage}.${table.lowerCamelName}.dto.*;

/**
 * ${table.comment} API
 *
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@API
public abstract class ${table.className}Api extends WSAnnotationHttpRequest {

    @Override
    public void init(WSRequestContext context) throws WSException {
    }

    @WSRequest(
        name = "列表",
        url = "${conf.apiAppName}/${table.lowerCamelName}/list",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult<${table.className}ListResDTO> list(@FromJson(key = "body") ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO);

    @WSRequest(
        name = "列表1.1.0",
        url = "${conf.apiAppName}/${table.lowerCamelName}/list",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class,
        headers = {
            @Header(name = "version", value = "1.1.0")
        }
    )
    public abstract ModelResult<${table.className}ListResDTO> list1_1_0(@FromJson(key = "body") ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO);

    @WSRequest(
        name = "列表1.2.0",
        url = "${conf.apiAppName}/${table.lowerCamelName}/list",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class,
        headers = {
            @Header(name = "version", value = "1.2.0"),
            @Header(name = "platform", value = "APP")
        }
    )
    public abstract ModelResult<${table.className}ListResDTO> list1_2_0(@FromJson(key = "body") ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO);

    @WSRequest(
        name = "列表1.3.0",
        url = "${conf.apiAppName}/${table.lowerCamelName}/list",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class,
        headers = {
            @Header(name = "version", value = "1.3.0"),
            @Header(name = "platform", value = "APP"),
            @Header(name = "device", value = "IOS")
        }
    )
    public abstract ModelResult<${table.className}ListResDTO> list1_3_0(@FromJson(key = "body") ${table.className}ListReqDTO ${table.lowerCamelName}ListReqDTO);

    @WSRequest(
        name = "分页",
        url = "${conf.apiAppName}/${table.lowerCamelName}/page",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class,
        headers = {
            @Header(name = "version", value = "1.0.0"),
            @Header(name = "platform", value = "APP"),
            @Header(name = "device", value = "IOS")
        }
    )
    public abstract ModelResult<${table.className}PageResDTO> page(@FromJson(key = "body") ${table.className}PageReqDTO ${table.lowerCamelName}PageReqDTO, @FromJson(key = "pageNo") Integer pageNo, @FromJson(key = "pageSize") Integer pageSize);

    @WSRequest(
        name = "新增",
        url = "${conf.apiAppName}/${table.lowerCamelName}/add",
        method = WSRequest.MethodType.POST,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult add(@FromJson(key = "body") ${table.className}AddReqDTO ${table.lowerCamelName}AddReqDTO);

    @WSRequest(
        name = "详情",
        url = "${conf.apiAppName}/${table.lowerCamelName}/show",
        method = WSRequest.MethodType.GET,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult<${table.className}ShowResDTO> show(@FromJson(key = "id") Long id);

    @WSRequest(
        name = "更新",
        url = "${conf.apiAppName}/${table.lowerCamelName}/modify",
        method = WSRequest.MethodType.POST,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult modify(@FromJson(key = "body") ${table.className}ModifyReqDTO ${table.lowerCamelName}ModifyReqDTO);

    @WSRequest(
        name = "删除",
        url = "${conf.apiAppName}/${table.lowerCamelName}/remove",
        method = WSRequest.MethodType.POST,
        responseType = WSRequest.ResponseType.JSON,
        charset = CharsetKit.DEFAULT_ENCODE,
        resultClass = ModelResult.class
    )
    public abstract ModelResult remove(@FromJson(key = "id") Long id);
}
