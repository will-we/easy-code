package com.jeasy;

import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Maps;
import com.jeasy.common.charset.CharsetKit;
import com.jeasy.common.pinyin.PinYinKit;
import com.jeasy.common.str.StrKit;
import com.jeasy.common.template.TemplateKit;

import java.util.List;
import java.util.Map;

/**
 * @author TaoBangren
 * @version 1.0
 * @since 2017/4/8 下午3:00
 */
public class DbInitMain {
    public static void main(String[] args) {
//        initDictionary();
        initResourcesAndRole();
//        initPriceCarInfo();
//        genDictionaryKit();
    }

    private static String params =
            "用户状态:1000=启用,1001=停用|" +
            "机构类型:2000=其他";

    private static void genDictionaryKit() {
        Map<String, List<DicType>> model = Maps.newHashMap();
        List<DicType> dicTypes = Lists.newArrayList();
        model.put("dicTypes", dicTypes);
        String[] array1 = StrKit.split(params, "|");
        for (String str : array1) {
            String[] tempArr = StrKit.split(str, ":");
            String[] valArr = StrKit.split(tempArr[1], ",");

            DicType dicType = new DicType();
            dicType.setName(tempArr[0]);
            dicType.setCode(PinYinKit.getPinYinHeadChar(tempArr[0]).toUpperCase());
            dicTypes.add(dicType);

            List<Dic> dics = Lists.newArrayList();
            for (String val : valArr) {
                String[] arr = StrKit.split(val, "=");
                Dic dic = new Dic();
                Map<String, String> params = Maps.newHashMap();
                params.put("\\(", "_");
                params.put("\\)", "");

                dic.setName(arr[1]);
                dic.setCode(StrKit.replace(PinYinKit.getPinYinHeadChar(arr[1]).toUpperCase(), params));
                dic.setOrigCode(PinYinKit.getPinYinHeadChar(arr[1]).toUpperCase());
                dics.add(dic);
            }
            dicType.setDics(dics);
        }

        TemplateKit.executeFreemarker("/Users/TaoBangren/git@osc/easy-code/jeasy-code-gen/src/main/resources", "DictionaryKit.ftl", CharsetKit.DEFAULT_ENCODE, model, "/Users/TaoBangren/git@osc/easy-code/jeasy-code-gen/src/main/java/com/jeasy", "DictionaryKit.java");
    }

    private static void initDictionary() {
        String[] array1 = StrKit.split(params, "|");

        int i = 1;
        for (String str : array1) {
            String[] tempArr = StrKit.split(str, ":");
            String[] valArr = StrKit.split(tempArr[1], ",");

            for (String val : valArr) {
                String[] arr = StrKit.split(val, "=");

                System.out.println(
                    StrKit.format("INSERT INTO `bd_dictionary` " +
                            "(`id`, `name`, `code`, `value`, `type`, `sort`, `pid`, `pcode`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                            "VALUES " +
                            "({}, '{}', '{}', {}, '{}', {}, {}, '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                        i++, arr[1], PinYinKit.getPinYinHeadChar(arr[1]).toUpperCase(), Integer.valueOf(arr[0].trim()), PinYinKit.getPinYinHeadChar(tempArr[0]).toUpperCase(), 0, 0, "", System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));
            }
        }
    }

    private static String resources = "用户管理(icon-lessee)=" +
        "{人员管理(icon-nav&1.html)[查询-添加-修改-删除]};" +
        "{角色管理(icon-nav&2.html)[查询-添加-修改-删除]};" +
        "{组织机构(icon-nav&3.html)[查询-添加-修改-删除]};" +
        "{菜单权限(icon-nav&4.html)[查询-添加-修改-删除]}|" +
        "基础数据(icon-car)=" +
        "{公共码表(icon-nav&5.html)[查询-添加-修改-删除]}|" +
        "日志监控(icon-entry)=" +
        "{操作日志(icon-nav&6.html)[查询-添加-修改-删除]};" +
        "{数据监控(icon-nav&8.html)[查询-添加-修改-删除]}";

    private static void initResourcesAndRole() {
        String[] array2 = StrKit.split(resources, "|");

        int j = 1;
        int m = 1;
        for (String str : array2) {
            String[] arr = str.split("=");
            String menu1 = arr[0];
            String icon = menu1.substring(menu1.lastIndexOf("(") + 1, menu1.lastIndexOf(")"));
            menu1 = menu1.substring(0, menu1.lastIndexOf("("));

            int pid = j;

            System.out.println(
                StrKit.format("INSERT INTO `su_resource` " +
                        "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_leaf`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                        "VALUES " +
                        "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                    j, menu1, PinYinKit.getPinYinHeadChar(menu1).toUpperCase(), "", icon, "", 0, 0, 1, 0, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

            System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                    "VALUES " +
                    "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                m++, 1, "超级管理员", "CJGLY", j++, menu1, PinYinKit.getPinYinHeadChar(menu1).toUpperCase(), System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

            String[] arr2 = arr[1].split(";");
            for (String str1 : arr2) {

                String[] arr3 = str1.split("\\[");
                String subMenu = arr3[0].substring(1);
                icon = subMenu.substring(subMenu.lastIndexOf("(") + 1, subMenu.lastIndexOf("&"));
                String url = subMenu.substring(subMenu.lastIndexOf("&") + 1, subMenu.lastIndexOf(")"));
                subMenu = subMenu.substring(0, subMenu.lastIndexOf("("));

                int pid1 = j;

                System.out.println(
                    StrKit.format("INSERT INTO `su_resource` " +
                            "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_leaf`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                            "VALUES " +
                            "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                        j++, subMenu, PinYinKit.getPinYinHeadChar(menu1).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenu).toUpperCase(), url, icon, "", pid, 0, 1, 1, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                        "VALUES " +
                        "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                    m++, 1, "超级管理员", "CJGLY", pid1, subMenu, PinYinKit.getPinYinHeadChar(menu1).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenu).toUpperCase(), System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                String[] arr4 = arr3[1].substring(0, arr3[1].lastIndexOf("]")).split(",");
                for (String str2 : arr4) {

                    String[] arr6;
                    if (str2.contains(":")) {
                        String[] arr5 = str2.split(":");
                        String op1 = arr5[0];
                        System.out.println(
                            StrKit.format("INSERT INTO `su_resource` " +
                                    "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_leaf`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                                    "VALUES " +
                                    "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                                j, op1, PinYinKit.getPinYinHeadChar(menu1).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenu).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(op1).toUpperCase(), "", "", "", pid1, 0, 0, 0, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                        System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                                "VALUES " +
                                "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                            m++, 1, "超级管理员", "CJGLY", j, op1, PinYinKit.getPinYinHeadChar(menu1).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenu).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(op1).toUpperCase(), System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                        int pid2 = j++;
                        arr6 = StrKit.split(arr5[1], "-");

                        for (String str3 : arr6) {
                            int tempId = j;
                            System.out.println(
                                StrKit.format("INSERT INTO `su_resource` " +
                                        "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_leaf`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                                        "VALUES " +
                                        "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                                    j++, str3, PinYinKit.getPinYinHeadChar(menu1).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenu).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(op1).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(str3).toUpperCase(), "", "", "", pid2, 0, 0, 1, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                            System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                                    "VALUES " +
                                    "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                                m++, 1, "超级管理员", "CJGLY", tempId, str3, PinYinKit.getPinYinHeadChar(menu1).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenu).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(op1).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(str3).toUpperCase(), System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));
                        }
                    } else {
                        arr6 = StrKit.split(str2, "-");

                        for (String str3 : arr6) {
                            int tempId = j;
                            System.out.println(
                                StrKit.format("INSERT INTO `su_resource` " +
                                        "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_leaf`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                                        "VALUES " +
                                        "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                                    j++, str3, PinYinKit.getPinYinHeadChar(menu1).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenu).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(str3).toUpperCase(), "", "", "", pid1, 0, 0, 1, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                            System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                                    "VALUES " +
                                    "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                                m++, 1, "超级管理员", "CJGLY", tempId, str3, PinYinKit.getPinYinHeadChar(menu1).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(subMenu).toUpperCase() + ":" + PinYinKit.getPinYinHeadChar(str3).toUpperCase(), System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));
                        }
                    }
                }
            }
        }
    }
}
