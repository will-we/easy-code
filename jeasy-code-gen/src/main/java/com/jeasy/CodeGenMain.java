package com.jeasy;

import com.jeasy.common.charset.CharsetKit;
import com.jeasy.common.file.FileKit;
import com.jeasy.common.template.TemplateKit;
import com.jeasy.conf.Config;
import com.jeasy.db.DBInfo;
import com.jeasy.db.TableInfo;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/20 15:44
 */
@Slf4j
public class CodeGenMain {

    public static void main(final String[] args) throws Exception {
        Config conf = new Config();
        DBInfo dbInfo = DBInfo.getInstance(conf);
        genCodeForMvc(conf, dbInfo);
    }

    private static void genCodeForMvc(final Config conf, final DBInfo dbInfo) throws Exception {

        Map<String, Object> model = Maps.newHashMap();
        model.put("conf", conf);

        log.info("\n--------------------------------------------------------------Generate Dao Module Start--------------------------------------------------------------");
        genDaoModule(conf.getDaoModuleName(), model, conf.getBackendTemplatePath() + "/dao", conf.getBackendTargetPath() + "/" + conf.getDaoModuleName(), dbInfo, conf.getBasePackage().replace(".", "/"));
        log.info("\n--------------------------------------------------------------Generate Dao Module End  --------------------------------------------------------------");

        log.info("\n--------------------------------------------------------------Generate Service Module Start--------------------------------------------------------------");
        genServiceModule(conf.getServiceModuleName(), model, conf.getBackendTemplatePath() + "/service", conf.getBackendTargetPath() + "/" + conf.getServiceModuleName(), dbInfo, conf.getBasePackage().replace(".", "/"));
        log.info("\n--------------------------------------------------------------Generate Service Module End  --------------------------------------------------------------");

        log.info("\n--------------------------------------------------------------Generate Web Module Start--------------------------------------------------------------");
        genWebModule(conf.getWebModuleName(), model, conf.getBackendTemplatePath() + "/web", conf.getBackendTargetPath() + "/" + conf.getWebModuleName(), dbInfo, conf.getBasePackage().replace(".", "/"));
        log.info("\n--------------------------------------------------------------Generate Web Module End  --------------------------------------------------------------");

        log.info("\n--------------------------------------------------------------Generate Client Module Start--------------------------------------------------------------");
        genClientModule(conf.getClientModuleName(), model, conf.getBackendTemplatePath() + "/client", conf.getBackendTargetPath() + "/" + conf.getClientModuleName(), dbInfo, conf.getBasePackage().replace(".", "/"));
        log.info("\n--------------------------------------------------------------Generate Client Module End  --------------------------------------------------------------");
    }

    private static void genClientModule(final String clientModuleName, final Map<String, Object> model, final String templatePath, final String targetPath, final DBInfo dbInfo, final String basePackage) throws IOException {
        log.info("1. Generate " + clientModuleName + " Module : " + targetPath + "/pom.xml");
        TemplateKit.executeFreemarker(templatePath, "pom.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath, "pom.xml");
        log.info("==> success");

        log.info("2. Generate " + clientModuleName + " Module : " + targetPath + "/src/test/java");
        FileKit.createDir(targetPath + "/src/test/java", false);
        log.info("==> success");

        log.info("3. Generate " + clientModuleName + " Module : " + targetPath + "/src/main/java");
        FileKit.createDir(targetPath + "/src/main/java", false);
        log.info("==> success");

        log.info("4. Generate " + clientModuleName + " Module : " + targetPath + "/src/main/resources");
        FileKit.createDir(targetPath + "/src/main/resources", false);
        log.info("==> success");

        log.info("5. Generate " + clientModuleName + " Module : " + targetPath + "/src/main/resources/applicationContext-client.xml");
        TemplateKit.executeFreemarker(templatePath + "/resources", "applicationContext-client.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath + "/src/main/resources", "applicationContext-client.xml");
        log.info("==> success");

        log.info("6. Generate " + clientModuleName + " Module : " + targetPath + "/src/test/java" + File.separator + basePackage + File.separator + "BaseJUnitTester4SpringContext.java");
        TemplateKit.executeFreemarker(templatePath, "BaseJUnitTester4SpringContext.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath + "/src/test/java" + File.separator + basePackage, "BaseJUnitTester4SpringContext.java");
        log.info("==> success");

        for (TableInfo table : dbInfo.getTables()) {
            model.put("table", table);

            // DTO
            String dtoTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "dto";
            String dtoTargetName = table.getClassName() + "AddReqDTO.java";

            log.info("7. Generate " + clientModuleName + " Module : " + dtoTargetPath + File.separator + dtoTargetName);
            TemplateKit.executeFreemarker(templatePath, "AddReqDto.ftl", CharsetKit.DEFAULT_ENCODE, model, dtoTargetPath, dtoTargetName);
            log.info("==> success");

            dtoTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "dto";
            dtoTargetName = table.getClassName() + "ListReqDTO.java";

            log.info("8. Generate " + clientModuleName + " Module : " + dtoTargetPath + File.separator + dtoTargetName);
            TemplateKit.executeFreemarker(templatePath, "ListReqDto.ftl", CharsetKit.DEFAULT_ENCODE, model, dtoTargetPath, dtoTargetName);
            log.info("==> success");

            dtoTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "dto";
            dtoTargetName = table.getClassName() + "ListResDTO.java";

            log.info("9. Generate " + clientModuleName + " Module : " + dtoTargetPath + File.separator + dtoTargetName);
            TemplateKit.executeFreemarker(templatePath, "ListResDto.ftl", CharsetKit.DEFAULT_ENCODE, model, dtoTargetPath, dtoTargetName);
            log.info("==> success");

            dtoTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "dto";
            dtoTargetName = table.getClassName() + "ModifyReqDTO.java";

            log.info("10. Generate " + clientModuleName + " Module : " + dtoTargetPath + File.separator + dtoTargetName);
            TemplateKit.executeFreemarker(templatePath, "ModifyReqDto.ftl", CharsetKit.DEFAULT_ENCODE, model, dtoTargetPath, dtoTargetName);
            log.info("==> success");

            dtoTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "dto";
            dtoTargetName = table.getClassName() + "PageReqDTO.java";

            log.info("11. Generate " + clientModuleName + " Module : " + dtoTargetPath + File.separator + dtoTargetName);
            TemplateKit.executeFreemarker(templatePath, "PageReqDto.ftl", CharsetKit.DEFAULT_ENCODE, model, dtoTargetPath, dtoTargetName);
            log.info("==> success");

            dtoTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "dto";
            dtoTargetName = table.getClassName() + "PageResDTO.java";

            log.info("12. Generate " + clientModuleName + " Module : " + dtoTargetPath + File.separator + dtoTargetName);
            TemplateKit.executeFreemarker(templatePath, "PageResDto.ftl", CharsetKit.DEFAULT_ENCODE, model, dtoTargetPath, dtoTargetName);
            log.info("==> success");

            dtoTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "dto";
            dtoTargetName = table.getClassName() + "RemoveReqDTO.java";

            log.info("13. Generate " + clientModuleName + " Module : " + dtoTargetPath + File.separator + dtoTargetName);
            TemplateKit.executeFreemarker(templatePath, "RemoveReqDto.ftl", CharsetKit.DEFAULT_ENCODE, model, dtoTargetPath, dtoTargetName);
            log.info("==> success");

            dtoTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "dto";
            dtoTargetName = table.getClassName() + "ShowResDTO.java";

            log.info("14. Generate " + clientModuleName + " Module : " + dtoTargetPath + File.separator + dtoTargetName);
            TemplateKit.executeFreemarker(templatePath, "ShowResDto.ftl", CharsetKit.DEFAULT_ENCODE, model, dtoTargetPath, dtoTargetName);
            log.info("==> success");

            dtoTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "dto";
            dtoTargetName = table.getClassName() + "DTO.java";

            log.info("15. Generate " + clientModuleName + " Module : " + dtoTargetPath + File.separator + dtoTargetName);
            TemplateKit.executeFreemarker(templatePath, "Dto.ftl", CharsetKit.DEFAULT_ENCODE, model, dtoTargetPath, dtoTargetName);
            log.info("==> success");

            // API
            String apiTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "api";
            String apiTargetName = table.getClassName() + "Api.java";

            log.info("15. Generate " + clientModuleName + " Module : " + apiTargetPath + File.separator + apiTargetName);
            TemplateKit.executeFreemarker(templatePath, "Api.ftl", CharsetKit.DEFAULT_ENCODE, model, apiTargetPath, apiTargetName);
            log.info("==> success");

            // JUnit
            String junitTargetPath = targetPath + "/src/test/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "api";
            String junitTargetName = table.getClassName() + "ApiJUnitTest.java";

            log.info("16. Generate " + clientModuleName + " Module : " + junitTargetPath + File.separator + junitTargetName);
            TemplateKit.executeFreemarker(templatePath, "ApiJUnitTest.ftl", CharsetKit.DEFAULT_ENCODE, model, junitTargetPath, junitTargetName);
            log.info("==> success");
        }
    }

    private static void genWebModule(final String webModuleName, final Map<String, Object> model, final String templatePath, final String targetPath, final DBInfo dbInfo, final String basePackage) throws IOException {
        log.info("1. Generate " + webModuleName + " Module : " + targetPath + "/pom.xml");
        TemplateKit.executeFreemarker(templatePath, "pom.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath, "pom.xml");
        log.info("==> success");

        log.info("2. Generate " + webModuleName + " Module : " + targetPath + "/src/test/java");
        FileKit.createDir(targetPath + "/src/test/java", false);
        log.info("==> success");

        log.info("3. Generate " + webModuleName + " Module : " + targetPath + "/src/main/java");
        FileKit.createDir(targetPath + "/src/main/java", false);
        log.info("==> success");

        log.info("4. Generate " + webModuleName + " Module : " + targetPath + "/src/main/resources");
        FileKit.createDir(targetPath + "/src/main/resources", false);
        log.info("==> success");

        log.info("5. Generate " + webModuleName + " Module : " + targetPath + "/src/main/webapp");
        FileKit.createDir(targetPath + "/src/main/webapp", false);
        log.info("==> success");

        log.info("6. Generate " + webModuleName + " Module : " + targetPath + "/src/main/webapp");
        FileKit.copyDirectiory(new File(templatePath + "/webapp"), targetPath + "/src/main/webapp");
        log.info("==> success");

        log.info("7.1 Generate " + webModuleName + " Module : " + targetPath + "/src/main/resources/dev");
        FileKit.copyDirectiory(new File(templatePath + "/resources/dev"), targetPath + "/src/main/resources/dev");
        log.info("==> success");

        log.info("7.2 Generate " + webModuleName + " Module : " + targetPath + "/src/main/resources/qa");
        FileKit.copyDirectiory(new File(templatePath + "/resources/qa"), targetPath + "/src/main/resources/qa");
        log.info("==> success");

        log.info("7.3 Generate " + webModuleName + " Module : " + targetPath + "/src/main/resources/prod");
        FileKit.copyDirectiory(new File(templatePath + "/resources/prod"), targetPath + "/src/main/resources/prod");
        log.info("==> success");

        log.info("8. Generate " + webModuleName + " Module : " + targetPath + "/src/main/webapp/WEB-INF/web.xml");
        TemplateKit.executeFreemarker(templatePath, "web.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath + "/src/main/webapp/WEB-INF", "web.xml");
        log.info("==> success");

        log.info("9. Generate " + webModuleName + " Module : " + targetPath + "/src/main/resources/dispatcher-servlet.xml");
        TemplateKit.executeFreemarker(templatePath + "/resources", "dispatcher-servlet.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath + "/src/main/resources", "dispatcher-servlet.xml");
        log.info("==> success");

        log.info("10. Generate " + webModuleName + " Module : " + targetPath + "/src/test/java" + File.separator + basePackage + File.separator + "BaseJUnitTester4SpringContext.java");
        TemplateKit.executeFreemarker(templatePath, "BaseJUnitTester4SpringContext.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath + "/src/test/java" + File.separator + basePackage, "BaseJUnitTester4SpringContext.java");
        log.info("==> success");

        log.info("11. Generate " + webModuleName + " Module : " + targetPath + "/src/test/java" + File.separator + basePackage + File.separator + "IndexTest.java");
        TemplateKit.executeFreemarker(templatePath, "IndexTest.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath + "/src/test/java" + File.separator + basePackage, "IndexTest.java");
        log.info("==> success");

        for (TableInfo table : dbInfo.getTables()) {
            model.put("table", table);

            // Controller
            String controllerTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "controller";
            String controllerTargetName = table.getClassName() + "Controller.java";

            log.info("12. Generate " + webModuleName + " Module : " + controllerTargetPath + File.separator + controllerTargetName);
            TemplateKit.executeFreemarker(templatePath, "Controller.ftl", CharsetKit.DEFAULT_ENCODE, model, controllerTargetPath, controllerTargetName);
            log.info("==> success");
        }
    }

    private static void genDaoModule(final String daoModuleName, final Map<String, Object> model, final String templatePath, final String targetPath, final DBInfo dbInfo, final String basePackage) throws IOException {
        log.info("1. Generate " + daoModuleName + " Module : " + targetPath + "/pom.xml");
        TemplateKit.executeFreemarker(templatePath, "pom.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath, "pom.xml");
        log.info("==> success");

        log.info("2. Generate " + daoModuleName + " Module : " + targetPath + "/src/test/java");
        FileKit.createDir(targetPath + "/src/test/java", false);
        log.info("==> success");

        log.info("3. Generate " + daoModuleName + " Module : " + targetPath + "/src/main/java");
        FileKit.createDir(targetPath + "/src/main/java", false);
        log.info("==> success");

        log.info("4. Generate " + daoModuleName + " Module : " + targetPath + "/src/main/resources");
        FileKit.createDir(targetPath + "/src/main/resources", false);
        log.info("==> success");

        log.info("5. Generate " + daoModuleName + " Module : " + targetPath + "/src/main/resources/sqlMapConfig.xml");
        TemplateKit.executeFreemarker(templatePath + "/resources", "sqlMapConfig.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath + "/src/main/resources", "sqlMapConfig.xml");
        log.info("==> success");

        log.info("6. Generate " + daoModuleName + " Module : " + targetPath + "/src/main/resources/applicationContext-dao.xml");
        TemplateKit.executeFreemarker(templatePath + "/resources", "applicationContext-dao.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath + "/src/main/resources", "applicationContext-dao.xml");
        log.info("==> success");

        for (TableInfo table : dbInfo.getTables()) {
            model.put("table", table);

            // Model
            String modelTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "entity";
            String modelTargetName = table.getClassName() + "Entity.java";

            log.info("7. Generate " + daoModuleName + " Module : " + modelTargetPath + File.separator + modelTargetName);
            TemplateKit.executeFreemarker(templatePath, "Entity.ftl", CharsetKit.DEFAULT_ENCODE, model, modelTargetPath, modelTargetName);
            log.info("==> success");

            // DAO
            String daoTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "dao";
            String daoTargetName = table.getClassName() + "DAO.java";

            log.info("8. Generate " + daoModuleName + " Module : " + modelTargetPath + File.separator + daoTargetName);
            TemplateKit.executeFreemarker(templatePath, "DAO.ftl", CharsetKit.DEFAULT_ENCODE, model, daoTargetPath, daoTargetName);
            log.info("==> success");

            // Mapper
            String mapperTargetPath = targetPath + "/src/main/resources/sqlMapper";
            String mapperTargetName = table.getClassName() + "Mapper.xml";

            log.info("9. Generate " + daoModuleName + " Module : " + mapperTargetPath + File.separator + mapperTargetName);
            TemplateKit.executeFreemarker(templatePath, "Mapper.ftl", CharsetKit.DEFAULT_ENCODE, model, mapperTargetPath, mapperTargetName);
            log.info("==> success");
        }
    }

    private static void genServiceModule(final String serviceModuleName, final Map<String, Object> model, final String templatePath, final String targetPath, final DBInfo dbInfo, final String basePackage) throws IOException {
        log.info("1. Generate " + serviceModuleName + " Module : " + targetPath + "/pom.xml");
        TemplateKit.executeFreemarker(templatePath, "pom.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath, "pom.xml");
        log.info("==> success");

        log.info("2. Generate " + serviceModuleName + " Module : " + targetPath + "/src/test/java");
        FileKit.createDir(targetPath + "/src/test/java", false);
        log.info("==> success");

        log.info("3. Generate " + serviceModuleName + " Module : " + targetPath + "/src/main/java");
        FileKit.createDir(targetPath + "/src/main/java", false);
        log.info("==> success");

        log.info("4. Generate " + serviceModuleName + " Module : " + targetPath + "/src/main/resources");
        FileKit.createDir(targetPath + "/src/main/resources", false);
        log.info("==> success");

        log.info("5. Generate " + serviceModuleName + " Module : " + targetPath + "/src/main/resources/applicationContext-server.xml");
        TemplateKit.executeFreemarker(templatePath + "/resources", "applicationContext-server.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath + "/src/main/resources", "applicationContext-server.xml");
        log.info("==> success");

        log.info("6. Generate " + serviceModuleName + " Module : " + targetPath + "/src/test/java" + File.separator + basePackage + File.separator + "BaseJUnitTester4SpringContext.java");
        TemplateKit.executeFreemarker(templatePath, "BaseJUnitTester4SpringContext.ftl", CharsetKit.DEFAULT_ENCODE, model, targetPath + "/src/test/java" + File.separator + basePackage, "BaseJUnitTester4SpringContext.java");
        log.info("==> success");

        for (TableInfo table : dbInfo.getTables()) {
            model.put("table", table);

            // Model
            String modelTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "biz/model";
            String modelTargetName = table.getClassName() + "Model.java";

            log.info("7. Generate " + serviceModuleName + " Module : " + modelTargetPath + File.separator + modelTargetName);
            TemplateKit.executeFreemarker(templatePath, "Model.ftl", CharsetKit.DEFAULT_ENCODE, model, modelTargetPath, modelTargetName);
            log.info("==> success");

            // Biz
            String bizTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "biz";
            String bizTargetName = table.getClassName() + "Biz.java";

            log.info("8. Generate " + serviceModuleName + " Module : " + bizTargetPath + File.separator + bizTargetName);
            TemplateKit.executeFreemarker(templatePath, "Biz.ftl", CharsetKit.DEFAULT_ENCODE, model, bizTargetPath, bizTargetName);
            log.info("==> success");

            // JUnit
            String junitTargetPath = targetPath + "/src/test/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "biz";
            String junitTargetName = table.getClassName() + "BizJUnitTest.java";

            log.info("10. Generate " + serviceModuleName + " Module : " + junitTargetPath + File.separator + junitTargetName);
            TemplateKit.executeFreemarker(templatePath, "BizJUnitTest.ftl", CharsetKit.DEFAULT_ENCODE, model, junitTargetPath, junitTargetName);
            log.info("==> success");

            // Service
            String serviceTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "service";
            String serviceTargetName = table.getClassName() + "Service.java";

            log.info("11. Generate " + serviceModuleName + " Module : " + serviceTargetPath + File.separator + serviceTargetName);
            TemplateKit.executeFreemarker(templatePath, "Service.ftl", CharsetKit.DEFAULT_ENCODE, model, serviceTargetPath, serviceTargetName);
            log.info("==> success");

            // Service Impl
            String serviceImplTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "service/impl";
            String serviceImplTargetName = table.getClassName() + "ServiceImpl.java";

            log.info("12. Generate " + serviceModuleName + " Module : " + serviceImplTargetPath + File.separator + serviceImplTargetName);
            TemplateKit.executeFreemarker(templatePath, "ServiceImpl.ftl", CharsetKit.DEFAULT_ENCODE, model, serviceImplTargetPath, serviceImplTargetName);
            log.info("==> success");

            // Manager
            String managerTargetPath = targetPath + "/src/main/java" + File.separator + basePackage + File.separator + table.getLowerCamelName() + File.separator + "manager";
            String managerTargetName = table.getClassName() + "Manager.java";

            log.info("13. Generate " + serviceModuleName + " Module : " + managerTargetPath + File.separator + managerTargetName);
            TemplateKit.executeFreemarker(templatePath, "Manager.ftl", CharsetKit.DEFAULT_ENCODE, model, managerTargetPath, managerTargetName);
            log.info("==> success");
        }
    }
}
