package com.jeasy.conf;

import com.jeasy.common.date.DateKit;
import com.google.common.collect.Sets;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.util.Set;

/**
 * @author taomk
 * @version 1.0
 * @since 2014/10/18 13:52
 */
@Data
@Slf4j
public class Config {

    private static final String CONF_PATH = "jeasy-code-gen.properties";

    private String backendTemplatePath;

    private String backendTargetPath;

    private String driverClass;

    private String jdbcUrl;

    private String userName;

    private String userPwd;

    private String dbName;

    private String tables;

    private String basePackage;

    private String author;

    private String version;

    private String webModuleName;

    private String webAppName;

    private String apiModuleName;

    private String apiAppName;

    private String serverModuleName;

    private String clientModuleName;

    private String bizModuleName;

    private String serviceModuleName;

    private String daoModuleName;

    private String shiroModuleName;

    private String parentArtifactId;

    private String parentGroupId;

    private String coreArtifactId;

    private String includeMsa;

    private String includeCxf;

    private String includeRabbitMQ;

    private String includeHessian;

    private Set<String> tableSet = Sets.newHashSet();

    public Config() {
        initConf(CONF_PATH);
    }

    private void initConf(String confPath) {
        try {
            Configuration conf = new PropertiesConfiguration(confPath);
            this.backendTemplatePath = conf.getString("backend.template.path") == null ? StringUtils.EMPTY : conf.getString("backend.template.path").trim();
            this.backendTargetPath = conf.getString("backend.target.path") == null ? StringUtils.EMPTY : conf.getString("backend.target.path").trim();
            this.driverClass = conf.getString("driver.class") == null ? StringUtils.EMPTY : conf.getString("driver.class").trim();
            this.userName = conf.getString("userName") == null ? StringUtils.EMPTY : conf.getString("userName").trim();
            this.userPwd = conf.getString("userPwd") == null ? StringUtils.EMPTY : conf.getString("userPwd").trim();
            this.dbName = conf.getString("dbName") == null ? StringUtils.EMPTY : conf.getString("dbName").trim();
            this.jdbcUrl = conf.getString("jdbc.url") == null ? StringUtils.EMPTY : conf.getString("jdbc.url").trim().replaceAll("\\{dbName}", this.dbName);
            this.tables = conf.getString("tables") == null ? StringUtils.EMPTY : conf.getString("tables").trim();

            if (!tables.contains("*")) {
                for (String table : tables.split(";")) {
                    tableSet.add(table.trim());
                }
            }

            this.basePackage = conf.getString("base.package") == null ? StringUtils.EMPTY : conf.getString("base.package").trim();
            this.author = conf.getString("author") == null ? StringUtils.EMPTY : conf.getString("author").trim();
            this.version = conf.getString("version") == null ? StringUtils.EMPTY : conf.getString("version").trim();

            this.webModuleName = conf.getString("webModuleName") == null ? StringUtils.EMPTY : conf.getString("webModuleName").trim();
            this.webAppName = conf.getString("webAppName") == null ? StringUtils.EMPTY : conf.getString("webAppName").trim();
            this.apiModuleName = conf.getString("apiModuleName") == null ? StringUtils.EMPTY : conf.getString("apiModuleName").trim();
            this.apiAppName = conf.getString("apiAppName") == null ? StringUtils.EMPTY : conf.getString("apiAppName").trim();
            this.serverModuleName = conf.getString("serverModuleName") == null ? StringUtils.EMPTY : conf.getString("serverModuleName").trim();
            this.clientModuleName = conf.getString("clientModuleName") == null ? StringUtils.EMPTY : conf.getString("clientModuleName").trim();
            this.bizModuleName = conf.getString("bizModuleName") == null ? StringUtils.EMPTY : conf.getString("bizModuleName").trim();
            this.serviceModuleName = conf.getString("serviceModuleName") == null ? StringUtils.EMPTY : conf.getString("serviceModuleName").trim();
            this.daoModuleName = conf.getString("daoModuleName") == null ? StringUtils.EMPTY : conf.getString("daoModuleName").trim();
            this.shiroModuleName = conf.getString("shiroModuleName") == null ? StringUtils.EMPTY : conf.getString("shiroModuleName").trim();
            this.parentArtifactId = conf.getString("parentArtifactId") == null ? StringUtils.EMPTY : conf.getString("parentArtifactId").trim();
            this.parentGroupId = conf.getString("parentGroupId") == null ? StringUtils.EMPTY : conf.getString("parentGroupId").trim();
            this.coreArtifactId = conf.getString("coreArtifactId") == null ? StringUtils.EMPTY : conf.getString("coreArtifactId").trim();
            this.includeMsa = conf.getString("includeMsa") == null ? StringUtils.EMPTY : conf.getString("includeMsa").trim();
            this.includeCxf = conf.getString("includeCxf") == null ? StringUtils.EMPTY : conf.getString("includeCxf").trim();
            this.includeRabbitMQ = conf.getString("includeRabbitMQ") == null ? StringUtils.EMPTY : conf.getString("includeRabbitMQ").trim();
            this.includeHessian = conf.getString("includeHessian") == null ? StringUtils.EMPTY : conf.getString("includeHessian").trim();
        } catch (ConfigurationException e) {
            log.error("error exception : ", e);
        }
    }

    public String getCreateDate() {
        try {
            return DateKit.currDate("yyyy/MM/dd HH:mm");
        } catch (ParseException e) {
            log.error("error exception : ", e);
        }
        return StringUtils.EMPTY;
    }
}
