package com.jeasy;

import lombok.Data;

import java.util.List;

@Data
public class DicType {

    String name;

    String code;

    List<Dic> dics;
}
